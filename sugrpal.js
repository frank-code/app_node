const port                                         = process.env.PORT || 82;
const express                                      = require('express')
const bodyParser                                   = require('body-parser')
const app                                          = express()
const axios                                        = require('axios');
const FormData                                     = require('form-data');
const router                                       = express.Router();
const cors                                         = require('cors')
const url                                          = require('url');
const qs                                           = require('qs');
const request                                      = require('request');
const LinePay                                      = require('line-pay-v3')
const admin                                        = require("firebase-admin");
const serviceAccount                               = require("./key/sugrpal-5d147-firebase-adminsdk-c3vjt-269a717690.json");
const {initializeApp, applicationDefault, cert}    = require('firebase-admin/app');
const fs                                           = require('fs');
const path                                         = require('path');
const {getFirestore, Timestamp, FieldValue}        = require('firebase-admin/firestore');
const line                                         = require('@line/bot-sdk');
const cron                                         = require('node-cron');
const nodemailer                                   = require('nodemailer');
const utf8                                         = require('nodejs-utf8');
var crypto                                         = require('crypto');
const multer                                       = require('multer');
const TelegramBot                                  = require('node-telegram-bot-api');
const {InlineKeyboardButton, InlineKeyboardMarkup} = require('node-telegram-bot-api');


// app.use(cors())
app.use(cors())
// app.use(cors({
//     origin: 'http://localhost:3000', // 你的前端应用地址
//     methods: ['GET', 'POST'],
//     credentials: true,
//     allowedHeaders: ['Content-Type'],
// }));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

admin.initializeApp({
                        credential : admin.credential.cert(serviceAccount),
                        databaseURL: 'https://sugrpal-5d147.firebaseio.com'

                    });
// const fireData = admin.database();
const db = getFirestore();


Date.prototype.format  = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        // "h+": this.getHours() - (this.getHours() >= 12 ? 12 : 0) + '',                   //小时
        "h+": this.getHours(),
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S" : this.getMilliseconds(),             //毫秒
        "t" : this.getHours() >= 12 ? 'PM' : 'AM'
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return this;
}

// app.get("/crontest", async (req, res, next) => {
//     try {
//         // 獲取今天的日期
//         const today                  = new Date();
//         // 計算前兩天的日期
//         const twoDaysBefore          = new Date(today.getTime() - (2 * 24 * 60 * 60 * 1000));
//         // 將前兩天的日期轉換為時間戳
//         const twoDaysBeforeTimestamp = twoDaysBefore.getTime();
//
//         const citiesRef = db.collection('candidate');
//         const snapshot  = await citiesRef.where('status', '==', 0).get();
//         await snapshot.forEach(async (doc) => {
//
//             const date = new Date(doc.data().created_at)
//             if (date.getTime() > twoDaysBeforeTimestamp) {
//                 console.log(`ID : ${doc.data().candidateId} 是在今天前兩天之後`);
//             } else {
//                 console.log(`ID : ${doc.data().candidateId} 是在今天前兩天之前`);
//                 doc.ref.update({status: 2});
//
//                 var config = {
//                     method : 'post',
//                     url    : 'https://node.loverun.app/tgBotMemberSend',
//                     headers: {
//                         'Content-Type': 'application/json'
//                     },
//                     data   : {
//                         botTokeb: "7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8", // 機器人
//                         username: doc.data().memberDetail.fromId,
//                         message : `等待太久無回應，系統判定未入選，退還扣除的積分。`
//                     }
//                 };
//
//                 axios(config).then(function (response) {
//                     res.status(200).send({
//                                              result: response.data,
//                                          });
//                 }).catch(function (error) {
//                     console.log(error);
//                 });
//
//
//                 // 取得目前會員積分更新返還
//                 const citiesRef2 = db.collection('points');
//                 const snapshot2  = await citiesRef2.where('memberId', '==', doc.data().memberId).get();
//                 await snapshot2.forEach(doc2 => {
//                     doc2.ref.update({points: doc2.data().points + 20});
//                 })
//
//                 const docRef3 = await db.collection('notify').add(
//                     {
//                         "check"     : false,
//                         "content"   : "未入選返還積分20點",
//                         "created_at": new Date().format("yyyy-MM-dd hh:mm:ss"),
//                         "date"      : new Date().format("yyyy-MM-dd"),
//                         "memberId"  : doc.data().memberId,
//                         "notifyId"  : "",
//                         "status"    : 0,
//                     }
//                 );
//                 await docRef3.update({
//                                          notifyId: docRef3.id
//                                      });
//
//                 const docRef4 = await db.collection('pointsLog').add(
//                     {
//                         "created_at" : new Date().format("yyyy-MM-dd hh:mm:ss"),
//                         "memberId"   : doc.data().memberId,
//                         "pointsData" : {
//                             "point": 30,
//                             "title": "未入選返還積分+30",
//                             "value": "未入選返還積分30"
//                         },
//                         "pointsLogId": "",
//                         "projectType": doc.data().memberDetail.projectType
//                     }
//                 );
//                 await docRef4.update({
//                                          pointsLogId: docRef4.id
//                                      });
//
//             }
//
//         });
//     } catch (err) {
//         console.log(err)
//     }
// })


//每分鐘發送測試
// cron.schedule('0 0 1 * * *', async () => {
//     try {
//         // 獲取今天的日期
//         const today                  = new Date();
//         // 計算前兩天的日期
//         const twoDaysBefore          = new Date(today.getTime() - (2 * 24 * 60 * 60 * 1000));
//         // 將前兩天的日期轉換為時間戳
//         const twoDaysBeforeTimestamp = twoDaysBefore.getTime();
//
//         const citiesRef = db.collection('candidate');
//         const snapshot  = await citiesRef.where('status', '==', 0).get();
//         await snapshot.forEach(async (doc) => {
//
//             const date = new Date(doc.data().created_at)
//             if (date.getTime() > twoDaysBeforeTimestamp) {
//                 console.log(`ID : ${doc.data().candidateId} 是在今天前兩天之後`);
//             } else {
//                 console.log(`ID : ${doc.data().candidateId} 是在今天前兩天之前`);
//                 doc.ref.update({status: 2});
//
//                 var config = {
//                     method : 'post',
//                     url    : 'https://node.loverun.app/tgBotMemberSend',
//                     headers: {
//                         'Content-Type': 'application/json'
//                     },
//                     data   : {
//                         botTokeb: "7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8", // 機器人
//                         username: doc.data().memberDetail.fromId,
//                         message : `等待太久無回應，系統判定未入選，退還扣除的積分。`
//                     }
//                 };
//
//                 axios(config).then(function (response) {
//                     res.status(200).send({
//                                              result: response.data,
//                                          });
//                 }).catch(function (error) {
//                     console.log(error);
//                 });
//
//
//                 // 取得目前會員積分更新返還
//                 const citiesRef2 = db.collection('points');
//                 const snapshot2  = await citiesRef2.where('memberId', '==', doc.data().memberId).get();
//                 await snapshot2.forEach(doc2 => {
//                     doc2.ref.update({points: doc2.data().points + 30});
//                 })
//
//                 const docRef3 = await db.collection('notify').add(
//                     {
//                         "check"     : false,
//                         "content"   : "未入選返還積分30點",
//                         "created_at": new Date().format("yyyy-MM-dd hh:mm:ss"),
//                         "date"      : new Date().format("yyyy-MM-dd"),
//                         "memberId"  : doc.data().memberId,
//                         "notifyId"  : "",
//                         "status"    : 0,
//                     }
//                 );
//                 await docRef3.update({
//                                          notifyId: docRef3.id
//                                      });
//
//                 const docRef4 = await db.collection('notify').add(
//                     {
//                         "created_at" : new Date().format("yyyy-MM-dd hh:mm:ss"),
//                         "memberId"   : doc.data().memberId,
//                         "pointsData" : {
//                             "point": 30,
//                             "title": "未入選返還積分+30",
//                             "value": "未入選返還積分30"
//                         },
//                         "pointsLogId": doc.data().memberDetail.projectType,
//                         "projectType": "couplezone"
//                     }
//                 );
//                 await docRef4.update({
//                                          pointsLogId: docRef4.id
//                                      });
//
//             }
//
//         });
//     } catch (err) {
//         console.log(err)
//     }
// })
// // 工作排程
// cron.schedule('0 0 1 * * *', async () => {
//     try {
//         let content     = [];
//         const citiesRef = db.collection('rate');
//         const snapshot  = await citiesRef.where('status', '==', 0).get();
//         await snapshot.forEach(doc => {
//             content.push(doc.data());
//         });
//
//         await content.forEach((val,index)=>{
//
//             var config = {
//                 method : 'post',
//                 url    : 'https://node.loverun.app/tgBotMemberSend',
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 data   : {
//                     botTokeb: "7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8", // 機器人
//                     username: val.fromId,
//                     message : `您一則待評價，尚未填填寫。 [去填寫賺取積分](https://loverun.app/rate-list)`
//                 }
//             };
//
//             axios(config).then(function (response) {
//                 res.status(200).send({
//                     result: response.data,
//                 });
//             }).catch(function (error) {
//                 console.log(error);
//             });
//
//         })
//     }
//     catch (err) {
//         console.log(err)
//     }
// })


function findFollowDataByDealerId(data, targetDealerId) {
    if (!data || !Array.isArray(data)) {
        return [];
    }

    const results = [];
    for (const item of data) {
        if (item.dealerId === targetDealerId) {
            results.push(item);
        }

        const deeperResults = findFollowDataByDealerId(item.children, targetDealerId);
        if (deeperResults.length > 0) {
            results.push(...deeperResults);
        }
    }

    return results;
}


app.post('/link', async (req, res) => {
    let formData = req.body;
    console.log('form data888', req.body);
    // console.log('form data777', formData.code)

    //取得member Data資料
    let content     = [];
    const citiesRef = db.collection('member');
    const snapshot  = await citiesRef.where('memberId', '==', formData.state).get();
    snapshot.forEach(doc => {
        content = doc.data();
    });

    try {
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-bot.line.me/oauth/token',
            'headers': {
                'Cookie': 'XSRF-TOKEN=9ab3e848-ba0e-4f3a-9c54-8079be7aa2e6'
            },
            formData : {
                'grant_type'   : 'authorization_code',
                'redirect_uri' : 'https://node.loverun.app/link',
                'code'         : formData.code,
                'client_id'    : 'jQNsWo2fXfizevs6OVtvmq',
                'client_secret': '0fZIruRNvcwS0I3EX8UwF65YldPHfMoUuYSqmEnNUIY'
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            var getData = JSON.parse(response.body)

            // fireData.ref('adminLine').set({token:'Bearer '+getData.access_token })
            console.log('999 ' + getData);
            console.log('Bearer ' + getData.access_token);
            content.fromLineToken = 'Bearer ' + getData.access_token;
            content.LINEbot       = true;

            db.collection('member').doc(content.memberId).set(content);

            // res.status(200).send('設定成功！')
            // res.location(content.siteUrl);
            res.redirect('https://loverun.app/notify-list');
        });
    } catch (err) {
        console.log(err)
    }
})

app.post("/getMsg", async (req, res, next) => {
    console.log(req.body);
    try {

        //取得lineData資料
        // let content     = [];
        // const citiesRef = db.collection('lineData');
        // const snapshot  = await citiesRef.where('shopLinkId', '==', req.body.shopLinkId).get();
        // snapshot.forEach(doc => {
        //     content = doc.data();
        // });
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-api.line.me/api/notify',
            'headers': {
                'Authorization': req.body.token,
            },
            formData : {
                'message': req.body.message
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
            res.status(200).send({
                                     result: true,
                                 });
        });
    } catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 token
app.post("/smsToken", async (req, res, next) => {
    // console.log(req.body);
    try {
        //取得lineData資料
        // let tdata    = {
        //   "HandlerType": 3,
        //   "VerifyType" : 1,
        //   "UID"        : "ye0205414225",
        //   "PWD"        : "Aa859230"
        // };
        // var options = {
        //   'method' : 'POST',
        //   'url'    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
        //   'headers': {
        //     'Content-Type': 'application/json'
        //   },
        //   formData : tdata
        // };
        //
        // axios.post('http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',tdata).then(res=>{
        //   res.status(200).send({
        //     result:res.data,
        //   });
        // });

        var data = JSON.stringify({"HandlerType": 3, "VerifyType": 1, "UID": "ye0205414225", "PWD": "Aa859230"});

        var config = {
            method : 'post',
            url    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
            headers: {
                'Content-Type': 'application/json'
            },
            data   : data
        };

        axios(config).then(function (response) {
            res.status(200).send({
                                     result: response.data,
                                 });
        }).catch(function (error) {
            console.log(error);
        });


        // request(options, function (error, response) {
        //   if (error) throw new Error(error);
        //   res.status(200).send({
        //     result:response,
        //   });
        // });
    } catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 送出內容
app.post("/smsSend", async (req, res, next) => {
    try {
        var options = {
            'method' : 'POST',
            'url'    : 'http://api.every8d.com/API21/HTTP/SendSMS.ashx',
            'headers': {
                'Authorization': req.body.token
            },
            formData : req.body.content
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            res.status(200).send({
                                     result: true,
                                 });
        });
    } catch (err) {
        console.log(err)
    }
})


app.post("/", async (req, res, next) => {

    console.log(req.body)

    res.status(200).send('newdata');
})

app.get("/test", async (req, res, next) => {
    console.log(req.body)
    res.status(200).send('Is sugrpal');
})

app.post("/sendEmail", async (req, res, next) => {
    console.log(req)
    const transporter = nodemailer.createTransport({
                                                       host: 'smtp.gmail.com',
                                                       port: 465,
                                                       auth: {
                                                           user: 'ye0205414225@gmail.com',
                                                           pass: 'vmuzzpmnpjjbreka',
                                                       },
                                                   });
    transporter.sendMail({
                             from   : 'service@newdate.app',
                             to     : req.body.sendEmail,
                             subject: req.body.sendTitle,
                             html   : req.body.sendContent,
                         }).then(info => {
        console.log({info});
    }).catch(console.error);
})



//金流－回傳結果 存入紀錄
app.post("/payment/result", async (req, res, next) => {
    // mchOrderNos 訂單編號

    //status = 3 支付成功狀態
    console.log(req.body)
    res.status(200).send('good');
    // res.redirect(`https://newdate.app/taff/payresult?status=success&paymentId=${docRef.id}`)
})



app.post("/payment", async (req, res, next) => {


    const generateOrderNumber = () => {
        const timestamp = Math.floor(Date.now() / 1000); // 取得當前的Unix時間戳（秒）
        return timestamp.toString(); // 將時間戳轉換為字符串
    };
    const orderId = generateOrderNumber()

    const merchantKey = '020DGXQT3LT5XDZK41ERDJFWBIMY47PXTJLT54SKLRMAXPFARQFRK5QHTD85WKZLYKOGORWGOEFSK5JT89AKTGO7MODKKSTTEFIOTZTPHZZVBSCNMMPNE5HSHPCRVLFQ'; // 商戶後台取得的金鑰
    const verifyKey   = 'oWEy%de3wr48g*lh'; // 客服提供的驗證金鑰
    const key         = merchantKey + verifyKey; // 組合密鑰

    let day          = req.body.day;
    // 商戶參數
    const mchId      = '202';
    const appId      = '531bbf140c7440bda75eefd5e070da05';
    const productId  = 8005;
    const mchOrderNo = orderId;
    const currency   = 'jpy';
    const amount     = req.body.amount; //日幣以分計算
    const clientIp   = '199.36.158.100';
    const notifyUrl  = 'https://node.sugrpal.app/payment/result';
    const returnUrl  = `https://sugrpal.app/payment_result?id=${req.body.id}&day=${day}&orderId=${orderId}`;
    const subject    = req.body.subject; //商品名稱
    const body       = req.body.body; //商品描述
    const param1     = req.body.subject;

    const params = {
        amount,
        appId,
        body,
        clientIp,
        currency,
        mchId,
        mchOrderNo,
        notifyUrl,
        returnUrl,
        productId,
        subject,
        param1
    };

    // 將參數按ASCII碼排序後拼接成字符串
    const stringA = Object.keys(params).sort().map(key => `${key}=${params[key]}`).join('&');

    // 拼接密鑰並進行MD5加密
    const stringSignTemp = `${stringA}&key=${key}`;

    console.log(stringSignTemp);
    const sign = crypto.createHash('md5').update(stringSignTemp).digest('hex').toUpperCase();

    // 添加簽名至參數
    params.sign = sign;

    const data = {
        // params: '{"amount": 1000, "appId": "531bbf140c7440bda75eefd5e070da05", "body": "測試商品描述", "clientIp": "199.36.158.100", "currency": "JPY", "mchId": "202", "mchOrderNo": "20160427210604000490", "notifyUrl": "https://sugrpal.loverun.app/payment/result", "productId": 8005, "returnUrl": "https://sugrpal-5d147.web.app/cn/return", "subject": "測試商品名稱", "sign": "938DD1E4BC172B1CDF0B22C729C0DE2F"}'
        params: JSON.stringify(params)
    };

    await axios.post('https://pay-jp.heropays.com/api/pay/create_order', qs.stringify(data), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
         .then(async (response) => {
             console.log('成功:', response.data);
             if (response.data.retCode == 'SUCCESS') {
                 // res.redirect(response.data.payParams.payUrl)


             const docRef = await db.collection('payment').add(
                    {
                        "created_at" : new Date().format("yyyy-MM-dd hh:mm:ss"),
                        "memberId"   : req.body.id,
                        "paymentId"  : "",
                        "orderId"    : orderId,
                        "status"     : 0,
                        "checkout"   : orderId,
                        "day"        : day
                    }
                );
                await docRef.update({
                                         paymentId: docRef.id
                                     });

                 response.data.day = day;
                 res.json(response.data);
             }
             // payParams: {
             //     amount: 600000,
             //     payUrl: 'https://www.crownpointnow.com/Shop/TransferInformation.html?PaymentID=1828&Realname=202-1723771984&ChannelCode=CreditCard'
             // },
             // retCode: 'SUCCESS',
             // payOrderId: 'JP01520240816103305',
             //  sign: '6CB9540A58BD9B269BDEA4B9F37A996B'

         })
         .catch(error => {
             console.error('失敗:', error.response ? error.response.data : error.message);
             res.status(500).send('error');
         });
});


// getRichmenu
app.post("/getRichmenu", async (req, res, next) => {
    const config = {
        method : 'get',
        url    : 'https://api.line.me/v2/bot/richmenu/list',
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        // data   : {
        //     message: lneMsg,
        //     userId : item.orderData.lineUserId,
        //     token  : item.linaAccessToken,
        // }
    };
    let result   = await axios(config)
    res.json(result.data);
})

function createFileFromBuffer(buffer, filename, mimeType) {
    const {Readable} = require('stream');

    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);

    const file = {
        buffer  : buffer,
        size    : buffer.length,
        mimeType: mimeType,
        name    : filename,
        stream  : () => stream,
    };

    return file;
}

// addRichmenu
app.post("/addRichmenu", async (req, res, next) => {


    // 讀取本地端的 JPG 圖片
    const filePath  = '../app_node/uploads/line-menu-action.jpg';
    const imageData = fs.readFileSync(filePath);

    console.log(imageData);

    const config = {
        method : 'post',
        url    : `https://api.line.me/v2/bot/richmenu`,
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        data   : req.body.data
    };
    await axios(config).then(async (result) => {


        // 上傳圖片
        const configImg = {
            method : 'post',
            url    : `https://api-data.line.me/v2/bot/richmenu/${result.data.richMenuId}/content`,
            headers: {
                'Content-Type' : 'image/jpeg',
                'Authorization': `Bearer ${req.body.token}`,
            },
            data   : imageData
        };
        axios(configImg).then(r => {
            // 設定顯示
            const configShow = {
                method : 'post',
                url    : `https://api.line.me/v2/bot/user/all/richmenu/${result.data.richMenuId}`,
                headers: {
                    'Authorization': `Bearer ${req.body.token}`,
                },
            };
            axios(configShow)
        })
    })


})
// delRichmenu
app.post("/delRichmenu", async (req, res, next) => {

    const config = {
        method : 'delete',
        url    : `https://api.line.me/v2/bot/richmenu/${req.body.richmenuId}`,
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        // data   : {
        //     message: lneMsg,
        //     userId : item.orderData.lineUserId,
        //     token  : item.linaAccessToken,
        // }
    };
    let result   = await axios(config)
    res.json(result.data);
})


const {Api, TelegramClient} = require('telegram')
const {StringSession}       = require('telegram/sessions')
// const {
//     CreateChatRequest,
//     DeleteChatUserRequest,
//     AddChatUserRequest
// } = require('telegram/functions');


const input = require('input') // npm i input

const apiId   = 27351716;
const apiHash = "8f32612dc166213d4ead972f85e2fd13";

// const stringSession = new StringSession("");
const stringSession = new StringSession("1BQANOTEuMTA4LjU2LjE3MQG7FgcPuEIoc26kkbcK6hn7pEmLF7QhJfh2cknsU2B4/9DmE9rZrEi9xo9slk4hqN29IyY8TDxYZjAHg5kMl3HWbCtpxBqpe2wpxI8RGIZEBWXw6Xy06RxcKe+1yhStUUJdnpSlnvvCGMCn+eaGoYl6MCDSrFnyt/0rdticdCTgREJq2Pe8vRyxhfIHN36Weep/J0RIn+XV2mqB3msx3jYpWD8oWUHQHhDgSP9UZgZalvd73A0YU8mwJcbwNp/TUgaKkiED4uq/UpC3vLE14COJV9ffsxxv6/LCwEq6rNAqBA3+IE9exwhGMKVno8ZryYXrKXjxH4J3zwEvKwbuQHOKaQ=="); // fill this later with the value from session.save()

const bot = new TelegramBot('7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8');
bot.setWebHook(`https://node.sugrpal.app/tgWebhook`);

// 驗證使用api權限
app.get("/tg", async (req, res, next) => {

    (async () => {
        console.log("Loading interactive example...");
        const client = new TelegramClient(stringSession, apiId, apiHash, {
            connectionRetries: 5,
        });
        await client.start({
                               phoneNumber: async () => await input.text("Please enter your number: "),
                               password   : async () => await input.text("Please enter your password: "),
                               phoneCode  : async () =>
                                   await input.text("Please enter the code you received: "),
                               onError    : (err) => console.log(err),
                           });
        console.log("You should now be connected.");
        console.log(client.session.save()); // Save this string to avoid logging in again
        await client.sendMessage("me", {message: "Hello!"});
    })();

    res.status(200).send('calendartest');
})

app.post("/tgsend", async (req, res, next) => {

    // (async () => {
    //     // 第一步: 創建群組並邀請一名用戶
    //     const users = [1298766725];  // 替換 'user1' 為第一個用戶的用戶名或ID
    //     const chat = await client.invoke(new CreateChatRequest(users, 'New Group Name'));
    //     const chatId = chat.chats[0].id;
    //     console.log(chatId)
    //     第二步: 將自己從群組中移除
    //     await client.invoke(new DeleteChatUserRequest(chatId, 'your_username'));  // 替換 'your_username' 為您的用戶名或ID
    //
    //     第三步: 邀請其他成員加入群組
    //     const otherUsers = ['user2', 'user3', 'user4'];  // 替換這些用戶名稱為您想邀請的其他用戶
    //     for (let user of otherUsers) {
    //         await client.invoke(new AddChatUserRequest(chatId, user, fwdLimit=10));
    //     }
    // })();
    try {
        (async function run() {
            const client = new TelegramClient(stringSession, apiId, apiHash, {
                connectionRetries: 5,
            });
            await client.connect(); // This assumes you have already authenticated with .start()

            // 傳送訊息
            // const result = await client.invoke(
            //     new Api.messages.SendMessage({
            //         peer      : "@testisooop",
            //         message   : "Hello there!",
            //         randomId  : BigInt("-4156887774564"),
            //         noWebpage : true,
            //         noforwards: true,
            //         // scheduleDate: 43,
            //         // sendAs: "@mimibabytime",
            //     })
            // );

            // 建立聊天室
            // const result = await client.invoke(
            //     new Api.messages.CreateChat({
            //         users: ["@kaden0216"],
            //         title: "My very normal title",
            //     })
            // );

            // webview
            // const result = await client.invoke(
            //     new Api.messages.RequestWebView({
            //         peer: "@mimibabytime",
            //         bot: "@Loveisrun_bot",
            //         fromBotMenu: true,
            //         url: "https://newdate.app/",
            //         startParam: "some string here",
            //         themeParams: new Api.DataJSON({
            //             data: "some string here",
            //         }),
            //         sendAs: "@mimibabytime",
            //     })
            // );

            //詳細用戶資訊
            const result = await client.invoke(
                new Api.users.GetFullUser({
                                              id: req.body.fromId,
                                          })
            );


            //邀請用戶加入頻道
            // const result = await client.invoke(
            //     new Api.channels.InviteToChannel({
            //         channel: "@ryuhiijv",
            //         users: ["@Syuh666"],
            //     })
            // );

            // 啟動機器人
            console.log(result); // prints the result
            res.status(200).send(result);

        })();
    } catch (error) {
        console.error("An error occurred:", error);
    }
})


// tg 機器人對用戶發送訊息
app.post("/tgBotMemberSend", async (req, res, next) => {
    try {
        // const BOT_TOKEN    = req.body.botTokeb
        const USER_CHAT_ID = req.body.username
        const MESSAGE      = req.body.message
        // const bot          = new TelegramBot(BOT_TOKEN, {polling: true});
        bot.sendMessage(USER_CHAT_ID, MESSAGE, {parse_mode: 'Markdown'}).then((message) => {
            res.status(200).send(message);
        });

    } catch (error) {
        console.error("An error occurred:", error);
    }
})

// tg 機器人在頻道發送訊息
app.post("/tgBotSend", async (req, res, next) => {

    try {
        const eventData  = req.body.eventData;
        const CHANNEL_ID = req.body.channelUsername;

        // 發送媒體群組
        bot.sendMediaGroup(CHANNEL_ID, eventData.media)
           .then(messages => {
               const messageId = messages[messages.length - 1].message_id;

               // 編輯消息的標題
               return bot.editMessageCaption(eventData.message, {
                   chat_id   : CHANNEL_ID,
                   message_id: messageId,
                   parse_mode: 'Markdown'
               });
           })
           .then(editedMessage => {
               res.status(200).send(editedMessage);
           })
           .catch(error => {
               console.log(error);
               res.status(500).send(error);
           });
    } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).send(error);
    }

    // try {
    //
    //     const eventData = req.body.eventData
    //     const BOT_TOKEN  = req.body.botTokeb
    //     const CHANNEL_ID = req.body.channelUsername
    //     const MESSAGE    = req.body.message
    //
    //     const urlmedia = `https://api.telegram.org/bot${BOT_TOKEN}/sendMediaGroup`;
    //
    //     const configmedia = {
    //         method : 'post',
    //         url    : urlmedia,
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //         data   : {
    //             chat_id: CHANNEL_ID,
    //             media: eventData.media
    //         }
    //     };
    //
    //     axios(configmedia).then(function (response) {
    //         console.log(response.data);
    //         let messageId = response.data.result[[response.data.result.length - 1]].message_id
    //
    //         const url = `https://api.telegram.org/bot${BOT_TOKEN}/editMessageCaption`;
    //         const config = {
    //             method : 'post',
    //             url    : url,
    //             headers: {
    //                 'Content-Type': 'application/json',
    //             },
    //             data   : {
    //                 message_id: messageId,
    //                 chat_id: CHANNEL_ID,
    //                 caption :  eventData.message,
    //                 parse_mode: 'Markdown'
    //             }
    //         };
    //
    //         axios(config).then(function (response) {
    //             res.status(200).send(response.data);
    //         }).catch(function (error) {
    //             console.log(error);
    //         });
    //
    //     }).catch(function (error) {
    //         console.log(error);
    //     });
    //
    //     // const url = `https://api.telegram.org/bot${BOT_TOKEN}/editMessageText`;
    //     // const config = {
    //     //     method : 'post',
    //     //     url    : url,
    //     //     headers: {
    //     //         'Content-Type': 'application/json',
    //     //     },
    //     //     data   : {
    //     //         message_id: messageId,
    //     //         chat_id: CHANNEL_ID,
    //     //         text      :  eventData.message,
    //     //         parse_mode: 'MarkdownV2' // 使用Markdown格式
    //     //     }
    //     // };
    //     //
    //     //
    //     // axios(config).then(function (response) {
    //     //     console.log(response.data);
    //     // }).catch(function (error) {
    //     //     console.log(error);
    //     // });
    //
    // }
    // catch (error) {
    //     console.error("An error occurred:", error);
    // }
})
// tg 機器人在頻道設定按鈕
app.get("/tgBotBtn", async (req, res, next) => {

    try {
        const BOT_TOKEN  = '7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8';  // Replace with your bot's API token
        const CHANNEL_ID = '-1002012164116';  // Replace with your channel's username or ID
        const url        = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`;

        const config = {
            method : 'post',
            url    : url,
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                chat_id: CHANNEL_ID,
                text   : '为了尽可能给大家创造一个真实的线下NTR、换妻渠道，我们设立了一个真实验证交流平台。\n' +
                         '\n' +
                         '只有通过随机手势验证的真实夫妻或优质单男才能通过审核。\n' +
                         '大陆 验证员：@Betasman    台湾 验证员@Loveisrun_bot' +
                         '\n' +
                         '操作方法例如：\n' +
                         '点击大陆>进入频道>点击登陆>跳转至验证中心>使用TG登陆>投稿>联系验证员>通过验证✅\n' +
                         '\n' +
                         '完成后寻找心仪对象，双方同意后程序会自动发送TG让双方联系。\n' +
                         '\n' +
                         '需要大家的共同参与，各个省市的用户才能尽可能的齐全，且方便大家各自寻找匹配。\n' +
                         '\n' +
                         '\n' +
                         '大陆 验证员：@Betasman    台湾 验证员@Loveisrun_bot'
            }
        };

        axios(config).then(function (response) {
            console.log(JSON.stringify(response.data));

            const botToken  = BOT_TOKEN
            // const chatId    = response.data.result.sender_chat.id
            const chatId    = response.data.result.chat.id; // 正确获取 chatId
            const messageId = response.data.result.message_id

            const url = `https://api.telegram.org/bot${botToken}/editMessageReplyMarkup`;

            const replyMarkup = {
                inline_keyboard: [
                    [
                        {
                            text: "台灣",
                            url : "https://loverun.app/couplezone"
                        },
                        {
                            text: "大陆",
                            url : "https://loverun.app/couplezone_cn"
                        },
                        {
                            text: "Twitter",
                            url : "https://twitter.com/i/communities/1688238809807523961"
                        }
                    ]
                ]
            };


            const config = {
                method : 'post',
                url    : url,
                headers: {
                    'Content-Type': 'application/json',
                },
                data   : {
                    chat_id     : chatId,
                    message_id  : messageId,
                    reply_markup: replyMarkup
                }
            };


            axios(config).then(function (response) {
                res.status(200).send(response.data);
            }).catch(function (error) {
                console.log(error);
            });


        }).catch(function (error) {
            console.log(error);
        });

    } catch (error) {
        console.error("An error occurred:", error);
    }
})
// tg 設定機器人選單hook
app.post("/tgWebhook", async (req, res, next) => {

    bot.processUpdate(req.body);
    res.sendStatus(200);

    /*    try {

            // const chatId = req.body.message.chat.id;

            //連動
             bot.onText(/\/start\s+(.+)/, async (msg, match) => {
                const chatId =  msg.chat.id;
                const username =  msg.from.username;
                const fromId =  msg.from.id;
                console.log(username)
                console.log(fromId)
                if (match[1],username) {
                    const memberId  = await match[1];
                    const citiesRef = await db.collection('member').doc(memberId);
                    await citiesRef.update({
                        TGbot: true,
                        TG:username,
                        fromId:fromId,
                    })
                    await bot.sendMessage(chatId, '通知連動成功，有疑問也可在此詢問管理員');
                }
                // const keyboard = [
                //     [
                //         {text: '我要真人驗證'},
                //         {text: '與管理員對話'},
                //         // {text: '登入教學影片'},
                //         // {text: '關於贊助獎勵'},
                //     ]
                // ];
                // const opts     = {
                //     reply_markup: {
                //         keyboard         : keyboard,
                //         resize_keyboard  : true,
                //         one_time_keyboard: false
                //     },
                //     parse_mode: 'Markdown'
                // };
                // bot.sendMessage(chatId, '已連動成功', opts);


            });

            //主動對機器人發的訊息
            bot.on('message', (msg) => {

                //檢查影片取得ID
                if (msg.video) {
                    const videoId = msg.video.file_id;
                    console.log("收到的影片文件ID: ", videoId);
                    // 您可以在這裡做更多的處理，比如存儲這個ID或回應用戶
                }
                if (msg.photo && msg.photo.length > 0) {
                    // Telegram的photo是一個陣列，包含不同大小的相同圖片
                    // 通常最後一個元素是分辨率最高的
                    const photoId = msg.photo[msg.photo.length - 1].file_id;
                    console.log("收到的圖片文件ID: ", photoId);
                    // 您可以在這裡做更多的處理，比如存儲這個ID或回應用戶
                }

                const chatId = msg.chat.id;
                if (msg.text === '與管理員對話') {
                    bot.sendMessage(chatId, `
                        在機器人中發送訊息可直接與管理員對話
                    `);
                }

                if (msg.text === '關於贊助獎勵') {
                    bot.sendMessage(chatId, `
    贊助 400$NT 送 500積分
    贊助 700$NT 送 1000積分
    贊助 1800$NT 送 3000積分

    與管理員對話可詢問如何贊助
                    `);
                }

                // if (msg.text === '登入教學影片') {
                //
                //     const video = 'BAACAgUAAxkBAAIFoGVRx7s8vY0d20ZXaR1ANVgWP3wcAAL3CgACTZqRVu6EhkAHgG_4MwQ';
                //     bot.sendVideo(chatId, video, {caption: '輸入電話後，需返回TG確認'});
                // }

                if (msg.text === '我要真人驗證') {
                    const inlineKeyboard = [
                        [
                            { text: '真人驗證規則', callback_data: '真人驗證規則' },
                            { text: '提交驗證', callback_data: '提交驗證' }
                        ]
                    ];
                    bot.sendMessage(chatId, `驗證說明：

    1.上傳相關照片影片
    2.完成後點選「提交驗證」
    3.等待管理員審核
    4.審核通過，平台上會新增「真人驗證」標籤，並且獲得「100積分」獎勵

    驗證和活動上任何疑問可直接在機器人聯絡管理員詢問。`, {  reply_markup: {
                            inline_keyboard: inlineKeyboard
                        }});
                }else{
                    //接受到的所有皆轉發 到我帳號
                    if(chatId !=1298766725 ){
                        bot.sendMessage(1298766725,`回覆留言:${chatId}:`);
                        bot.forwardMessage(1298766725, chatId, msg.message_id)
                    }

                    if (msg.text.includes('回覆留言')){
                        // 回復用戶:testisooop:你好你好
                        let message = msg.text.split(':');
                        console.log(message)
                        let userName     =  message[1]
                        let msgContent   =  message[2]
                        bot.sendMessage(userName,msgContent);
                    }
                }




            });

            // 按鈕文字回復
            bot.on('callback_query', (callbackQuery) => {
                const data = callbackQuery.data;
                const chatId = callbackQuery.message.chat.id;

                if (data === '真人驗證規則')  {

                    const inlineKeyboard2 = [
                        [
                            { text: '環境驗證範例', callback_data: '環境驗證範例' },
                            { text: '手勢驗證範例', callback_data: '手勢驗證範例' }
                        ]
                    ];

                    bot.sendMessage(chatId, `驗證規則：
    圖片和影片都可，驗證需符合以下驗證方式之一。

    1.[舉牌驗證]：
    女方裸體（照片影片都可）需有紙條寫上「LoveRun」的字。

    2.[環境驗證]：
    女方裸體（照片影片都可）+ 女方裸體相同環境的相同手勢影片（最好是相同角度）。

    3.[手勢驗證]：
    女方露點影片中做相同手勢

    驗證和活動上任何疑問可直接在機器人中打字詢問。`,{  reply_markup: {inline_keyboard: inlineKeyboard2}});
                } else if (data === '提交驗證') {
                    // 處理提交驗證的行為，例如引導用戶如何提交資料
                    //接受到的所有皆轉發 到我帳號

                    bot.sendMessage(1298766725,`${chatId}　提交驗證`);
                    bot.sendMessage(chatId, `提交需審核，請耐心等候！`);
                }

                if(data === '環境驗證範例'){
                    const img = 'AgACAgUAAxkBAAIDoWVOXSSC0EohS5fU1zJlpSQcBnt7AALuvzEbqu1xVk3Qf8OhxDbSAQADAgADeAADMwQ';
                    bot.sendPhoto(chatId, img, {caption: '環境驗證範例-相同的床單顏色'});

                    const video = 'BAACAgUAAxkBAAIDomVOXUPGGnJ2zacGyrkOB-rPSOWxAAJpDQACqu1xVj2H2YiAjFRPMwQ';
                    bot.sendVideo(chatId, video, {caption: '環境驗證範例-相同手勢環境確認屬於原創'});

                }else if (data === '手勢驗證範例'){
                    const video = 'BAACAgUAAxkBAAIDo2VOXU8Oy_AvxMXFKGbrDZY92LxsAAJqDQACqu1xVtmGowXhetIvMwQ';
                    bot.sendVideo(chatId, video, {caption: '手勢驗證範例-相同手勢確認屬於原創'});
                }

                // 確保呼叫 `answerCallbackQuery` 來通知 Telegram 你已處理回撥
                bot.answerCallbackQuery(callbackQuery.id);
            });


        }
        catch (error) {
            console.error("An error occurred:", error);
        }*/
})
bot.onText(/\/start\s+(.+)/, async (msg, match) => {
    const chatId   = msg.chat.id;
    const username = msg.from.username;
    const fromId   = msg.from.id;


    const keyboard = [
        [
            {text: '審核驗證'},
        ]
    ];

    const opts = {
        reply_markup: {
            keyboard         : keyboard,
            resize_keyboard  : true,
            one_time_keyboard: false
        },
        parse_mode  : 'Markdown'
    };
    if (match[1]) {
        const memberId  = match[1];
        const citiesRef = db.collection('member').doc(memberId);
        console.log(msg, match)
        // try {
        await citiesRef.update({
                                   TGbot : true,
                                   TG    : (username) ? username : 'none',
                                   fromId: fromId,
                               });


        await bot.sendMessage(chatId, '通知連動成功，如有問題請詢問管理員 ', opts);
        // } catch (error) {
        //     await bot.sendMessage(chatId, '如有問題請詢問管理員。',opts);
        // }
    } else {
        await bot.sendMessage(chatId, '如有問題請詢問管理員@LoveRunaAdmin', opts);
        // const img1 = 'AgACAgUAAxkBAAJUymXQS8YnAXIEaYwGAAH_mSHF7PTUdAACF70xG0YkgVbCzLP-g8nvlwEAAwIAA3kAAzQE';
        // bot.sendPhoto(chatId, img1, {caption: '設定帳號步驟1'});
        // const img2 = 'AgACAgUAAxkBAAJUy2XQTCHOAAG8VFClJEZvDChi4gFGGQACGb0xG0YkgVZ39EK6j84STAEAAwIAA3kAAzQE';
        // bot.sendPhoto(chatId, img2, {caption: '設定帳號步驟2'});
    }
});
//主動對機器人發的訊息
bot.on('message', (msg) => {

    const chatId = msg.chat.id;
    //接受到的所有皆轉發 到我帳號
    if (chatId != 1298766725) {
        // bot.sendMessage(1298766725,`回覆留言:${chatId}:`);
        bot.forwardMessage(1298766725, chatId, msg.message_id)
    }

    if (msg.text === '與管理員對話') {
        bot.sendMessage(chatId, `  
                    管理員帳號:@LoveRunaAdmin  
                `);
    } else if (msg.text === '審核驗證') {
        bot.sendMessage(chatId, `  
1.審核時間不固定，皆為人工審核，請耐心等候。  
2.真人驗證贈送積分100 3天VIP試用(無限暢聊)。  
                `);
    } else if (msg.text === '升級VIP') {

        bot.sendMessage(chatId, `  
        VIP權益說明  
        - 無限制私訊  
        - 無限制解鎖照片  
        - 其他待開放  
          
        贊助費用   
        - VIP贊助詳情 https://loverun.app/#payment
        1.請截圖帳號名稱給管理員  
        2.使用轉帳匯款  
        3.直接告管理員 @LoveRunaAdmin 
                `);
    } else if (msg.text === '投稿說明') {

        bot.sendMessage(chatId, `  
        發佈投稿   
        - 官方自動連同TG、X發佈  
        - 會增加積分  
        1.接收報名投稿活動  
        2.到個人頁面->我的投稿->你投稿名稱->審核報名->開啟聊天室對話  
                `);
    } else if (msg.text === '頻道列表') {

        const inlineKeyboard = [
            [
                {
                    text: "台灣NTR交流",
                    url : "https://t.me/ryuhiijv"
                },
                {
                    text: "大陆NTR交流",
                    url : "https://t.me/couplezone_cn"
                },
            ]
        ];
        bot.sendMessage(chatId, `選擇頻道`, {
            reply_markup: {
                inline_keyboard: inlineKeyboard
            }
        });

    } else {

        bot.sendMessage(chatId, `此為機器人回覆，其他問題請聯繫管理員　@LoveRunaAdmin`);
    }


});
// 按鈕文字回復
bot.on('callback_query', (callbackQuery) => {
    const data   = callbackQuery.data;
    const chatId = callbackQuery.message.chat.id;


    //夫妻情侶交流
    if (data === 'NTR交流') {
        const inlineKeyboard = [
            [
                {text: '真人驗證規則', callback_data: '真人驗證規則'},
                {text: '提交驗證', callback_data: '提交驗證'}
            ]
        ];
        bot.sendMessage(chatId, `驗證說明：  
  
1.上傳相關照片影片(請勿轉傳)  
2.完成後點選「提交驗證」  
3.等待管理員審核  
4.審核通過，平台上會新增「真人驗證」標籤，並且獲得「100積分」獎勵  
5.驗證主要給夫妻情侶，單男無需驗證，單男以評價為參考  
  
驗證和活動上任何疑問可直接在機器人聯絡管理員詢問。`, {
            reply_markup: {
                inline_keyboard: inlineKeyboard
            }
        });
    }
    if (data === '真人驗證規則') {

        const inlineKeyboard2 = [
            [
                {text: '環境驗證範例', callback_data: '環境驗證範例'},
                {text: '手勢驗證範例', callback_data: '手勢驗證範例'}
            ]
        ];

        bot.sendMessage(chatId, `驗證規則：  
圖片和影片都可，驗證需符合以下驗證方式之一。  
  
1.[舉牌驗證]：  
女方裸體（照片影片都可）需有紙條寫上「LoveRun」的字。  
  
2.[環境驗證]：  
女方裸體（照片影片都可）+ 女方裸體相同環境的相同手勢影片（最好是相同角度）。  
  
3.[手勢驗證]：  
女方露點影片中做相同手勢  
  
驗證和活動上任何疑問可直接在機器人中打字詢問。`, {reply_markup: {inline_keyboard: inlineKeyboard2}});
    } else if (data === '提交驗證') {
        // 處理提交驗證的行為，例如引導用戶如何提交資料
        //接受到的所有皆轉發 到我帳號

        bot.sendMessage(1298766725, `${chatId}　提交驗證`);
        bot.sendMessage(chatId, `提交需審核，請耐心等候！`);
    }
    if (data === '環境驗證範例') {
        const img = 'AgACAgUAAxkBAAIDoWVOXSSC0EohS5fU1zJlpSQcBnt7AALuvzEbqu1xVk3Qf8OhxDbSAQADAgADeAADMwQ';
        bot.sendPhoto(chatId, img, {caption: '環境驗證範例-相同的床單顏色'});

        const video = 'BAACAgUAAxkBAAIDomVOXUPGGnJ2zacGyrkOB-rPSOWxAAJpDQACqu1xVj2H2YiAjFRPMwQ';
        bot.sendVideo(chatId, video, {caption: '環境驗證範例-相同手勢環境確認屬於原創'});

    } else if (data === '手勢驗證範例') {
        const video = 'BAACAgUAAxkBAAIDo2VOXU8Oy_AvxMXFKGbrDZY92LxsAAJqDQACqu1xVtmGowXhetIvMwQ';
        bot.sendVideo(chatId, video, {caption: '手勢驗證範例-相同手勢確認屬於原創'});
    }


    if (['男同交流', '原味交流', 'SM交流', 'Cosplay交流', '按摩舒壓交流'].includes(data)) {
        bot.sendMessage(chatId, `尚未開放驗證`);
    }

    // 確保呼叫 `answerCallbackQuery` 來通知 Telegram 你已處理回撥
    bot.answerCallbackQuery(callbackQuery.id);
});


const storage = multer.memoryStorage(); // 使用 memory storage，這意味著圖片將只存儲在 RAM 中，不會保存到磁盤。
// 設定
const upload = multer({
                          storage: storage,
                          limits : {
                              fileSize: 20 * 1024 * 1024 // 限制文件大小為 5MB
                          }

                      });

const sharp = require('sharp');

app.post('/uploadblur', upload.single('image'), async (req, res) => {
    const buffer = req.file.buffer;

    try {
        const processedBuffer = await sharp(buffer).resize({
                                                               width             : 800,
                                                               withoutEnlargement: true,
                                                               fit               : sharp.fit.cover,
                                                           }) // 調整圖片寬度為 800 pixels，保持原始的寬高比
                                                   .jpeg({quality: 70}) // 設定 JPEG 質量為 70%
                                                   .blur(26).toBuffer();

        const base64 = processedBuffer.toString('base64');
        res.json({success: true, image: `data:image/png;base64,${base64}`});

    } catch (error) {
        console.error('Processing failed:', error);
        res.status(500).json({success: false, message: 'Processing failed', error: error.message});
    }
});
const Jimp = require('jimp'); // 浮水印
app.post('/uploadimg', upload.single('image'), async (req, res) => {
    const buffer = req.file.buffer;

    try {
        console.log(buffer)
        const processedBuffer = await sharp(buffer).resize({
                                                               width             : 800,
                                                               withoutEnlargement: true,
                                                               fit               : sharp.fit.cover,
                                                           }) // 調整圖片寬度為 800 pixels，保持原始的寬高比
                                                   .jpeg({quality: 70}) // 設定 JPEG 質量為 70%
                                                   .toBuffer();
        console.log(processedBuffer)
        const base64 = processedBuffer.toString('base64');
        res.json({success: true, base64: base64, image: `data:image/png;base64,${base64}`});

    } catch (error) {
        console.error('Processing failed:', error);
        res.status(500).json({success: false, message: 'Processing failed', error: error.message});
    }

    // const buffer = req.file.buffer;
    // const watermarkPath = 'https://firebasestorage.googleapis.com/v0/b/loverun-4a987.appspot.com/o/applogo.png?alt=media&token=3376f78b-f979-48ee-b2d6-dc376e25ab8a'; // 浮水印圖像的路徑
    //
    // try {
    //     // 使用 Jimp 處理浮水印圖像，調整其透明度
    //     const watermark = await Jimp.read(watermarkPath);
    //     watermark.opacity(0.2); // 設定透明度為 20%
    //     const watermarkBuffer = await watermark.getBufferAsync(Jimp.MIME_PNG);
    //
    //     // 將處理過的浮水印合併到主圖像
    //     const processedBuffer = await sharp(buffer)
    //         .resize({
    //             width             : 800,
    //             withoutEnlargement: true,
    //             fit               : sharp.fit.cover,
    //         })
    //         .composite([{ input: watermarkBuffer, gravity: 'southeast' }]) // 添加浮水印
    //         .jpeg({quality: 70})
    //         .toBuffer();
    //
    //     const base64 = processedBuffer.toString('base64');
    //     res.json({success: true, base64: base64, image: `data:image/png;base64,${base64}`});
    // }
    // catch (error) {
    //     console.error('Processing failed:', error);
    //     res.status(500).json({success: false, message: 'Processing failed', error: error.message});
    // }

});


const {TwitterApi}  = require('twitter-api-v2');
// 客戶端初始化
const twitterClient = new TwitterApi({
                                         appKey      : 'DEC1ZYK24QdKZ9zDSJl9Mjcdk',
                                         appSecret   : 'cTUDR5nxy5VaiOjq57jQVFCBskAc6uIJkYn2wjLxDOUUgzcOtO',
                                         accessToken : '1804020774405312512-QeZTNsDgrZGpOOIm386KaDlqoMHzvv',
                                         accessSecret: 'XiJcj9UT1bVfKbRcPQYP9OEFKZ8CmOgrZiKvSwgChcEBp',
                                     });

//推特

// 從 URL 下載圖片
async function downloadImage(url, filepath) {
    const response = await axios({
                                     url,
                                     responseType: 'stream',
                                 });

    return new Promise((resolve, reject) => {
        response.data.pipe(fs.createWriteStream(filepath))
                .on('finish', () => resolve())
                .on('error', e => reject(e));
    });
}

app.post('/pushtwitter', async (req, res) => {
    try {
        const text     = req.body.text;
        const imageURL = req.body.imagePath; // 替换为你的图片路径
        const tags     = req.body.tags;          // 替换为你想使用的标签

        const tempPath = path.join(__dirname, 'temp.jpg');
        // 下载图片
        await downloadImage(imageURL, tempPath);

        // 读取图片
        const imageData = fs.readFileSync(tempPath);

        // 上传图片到 Twitter
        const mediaId = await twitterClient.v1.uploadMedia(imageData, {mimeType: 'image/png'});

        // 创建推文，附加图片和标签
        const tweet = await twitterClient.v2.tweet(text + ' ' + tags.join(' '), {
            media: {media_ids: [mediaId]}
        });
        console.log('Tweet with image posted:', tweet);
    } catch (error) {
        console.error('Error posting tweet with image:', error);
    }
});
const rwClient = twitterClient.readWrite;
app.post('/msgtwitter', async (req, res) => {
    try {
        const recipientId = req.body.twitterId;
        const messageText = req.body.text;
        const result      = await rwClient.v1.sendDm({
                                                         recipient_id: recipientId,
                                                         text        : messageText,
                                                     });
        console.log('Message sent:', result);
    } catch (error) {
        console.error('Error posting tweet with image:', error);
    }
});


// 將原本的程式碼封裝成一個函數
async function sendTelegramMessage() {
    try {
        const BOT_TOKEN  = '7123932428:AAHCTG7PQD8kgatf49be2vxks89Oik_Xag8'; // 替換成你的 bot 的 API token
        const CHANNEL_ID = '-1002012164116'; // 替換成你的頻道的 username 或 ID
        const url        = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`;

        const messageConfig = {
            method : 'post',
            url    : url,
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                chat_id: CHANNEL_ID,
                text   : '为了尽可能给大家创造一个真实的线下NTR、换妻渠道，我们设立了一个真实验证交流平台。\n' +
                         '\n' +
                         '只有通过随机手势验证的真实夫妻或优质单男才能通过审核。\n' +
                         '大陆 验证员：@Betasman    台湾 验证员@Loveisrun_bot' +
                         '\n' +
                         '操作方法例如：\n' +
                         '点击大陆>进入频道>点击登陆>跳转至验证中心>使用TG登陆>投稿>联系验证员>通过验证✅\n' +
                         '\n' +
                         '完成后寻找心仪对象，双方同意后程序会自动发送TG让双方联系。\n' +
                         '\n' +
                         '需要大家的共同参与，各个省市的用户才能尽可能的齐全，且方便大家各自寻找匹配。\n' +
                         '\n' +
                         '\n' +
                         '大陆 验证员：@Betasman    台湾 验证员@Loveisrun_bot'
            }
        };

        const response = await axios(messageConfig);
        console.log(JSON.stringify(response.data));

        const botToken  = BOT_TOKEN;
        const chatId    = response.data.result.chat.id; // 正确获取 chatId
        const messageId = response.data.result.message_id;

        const editConfig = {
            method : 'post',
            url    : `https://api.telegram.org/bot${botToken}/editMessageReplyMarkup`,
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                chat_id     : chatId,
                message_id  : messageId,
                reply_markup: {
                    inline_keyboard: [
                        [
                            {
                                text: "台灣",
                                url : "https://loverun.app/couplezone"
                            },
                            {
                                text: "大陆",
                                url : "https://loverun.app/couplezone_cn"
                            },
                            {
                                text: "Twitter",
                                url : "https://twitter.com/i/communities/1688238809807523961"
                            }
                        ]
                    ]
                }
            }
        };

        await axios(editConfig);
        console.log("Message and buttons sent successfully.");
    } catch (error) {
        console.error("An error occurred:", error);
    }
}

// 设置定时任务，每小时执行一次
cron.schedule('0 0 */3 * * *', async () => {
    console.log('Running sendTelegramMessage every three hours');
    await sendTelegramMessage();
});


const server = app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

