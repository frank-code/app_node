const express = require('express');
const cron = require('node-cron');
const request = require('request');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// 設定一個基本的 GET 路由
app.get('/', (req, res) => {
    res.send('Hello World!');
});

// 設定一個 cron job 每分鐘執行一次
// cron.schedule('* * * * *', () => {
//     var options = {
//         'method' : 'GET',
//         'url'    : 'https://node.newdate.app/firebase',
//     };
//     request(options, function (error, response) {
//         if (error) {
//             console.error('Error:', error);
//         } else {
//             console.log('Response:', response && response.statusCode);
//         }
//     });
// });

cron.schedule('0 0 1 * * *', async () => {

    var options = {
        'method' : 'POST',
        'url'    : 'https://node.newdate.app/cronMessage',
    };
    request(options, function (error, response) {
        if (error) {
            console.error('Error:', error);
        } else {
            console.log('Response:', response && response.statusCode);
        }
    });

    var options1 = {
        'method' : 'POST',
        'url'    : 'https://node.loverun.app/cronNotification',
    };
    request(options1, function (error, response) {
        if (error) {
            console.error('Error:', error);
        } else {
            console.log('Response:', response && response.statusCode);
        }
    });
});

app.post('/cronEvent', async (req, res) => {
    console.log('cronEvent');
    res.send('cronEvent received');
})

app.post('/cronMessage', async (req, res) => {
    console.log('cronMessage');
    res.send('cronMessage received');
})

// 啟動伺服器
app.listen(port, () => {
    console.log(`伺服器正在 http://localhost:${port} 上運行`);
});
