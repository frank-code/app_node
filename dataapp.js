const port                                      = process.env.PORT || 83;
const express                                   = require('express')
const bodyParser                                = require('body-parser')
const app                                       = express()
const axios                                     = require('axios');
const FormData                                  = require('form-data');
const router                                    = express.Router();
const cors                                      = require('cors')
const url                                       = require('url');
// const qs             = require('qs');
const request                                   = require('request');
const LinePay                                   = require('line-pay-v3')
const admin                                     = require("firebase-admin");
const serviceAccount                            = require("./key/freer-ba4ea-firebase-adminsdk-h1osg-252a389277.json");
// import serviceAccount from './key.js';
const {initializeApp, applicationDefault, cert} = require('firebase-admin/app');
const fs                                        = require('fs');
const path                                      = require('path');

const {getFirestore, Timestamp, FieldValue} = require('firebase-admin/firestore');

const line       = require('@line/bot-sdk');
const cron       = require('node-cron');
const nodemailer = require('nodemailer');
const utf8       = require('nodejs-utf8');
var crypto       = require('crypto');
// const multer = require('multer');

app.use(cors());
admin.initializeApp({
    credential : admin.credential.cert(serviceAccount),
    databaseURL: 'https://freer-ba4ea.firebaseio.com'

});
// const fireData = admin.database();
const db = getFirestore();
const storage = admin.storage();
const bucket = storage.bucket('freer-ba4ea.appspot.com');



app.use(cors())
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

Date.prototype.format  = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        // "h+": this.getHours() - (this.getHours() >= 12 ? 12 : 0) + '',                   //小时
        "h+": this.getHours(),
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S" : this.getMilliseconds(),             //毫秒
        "t" : this.getHours() >= 12 ? 'PM' : 'AM'
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return this;
}

const multer = require("multer");
const storage2 = multer.memoryStorage(); // 使用 memory storage，這意味著圖片將只存儲在 RAM 中，不會保存到磁盤。
// 設定
const upload = multer({
    storage: storage2,
    limits : {
        fileSize: 20 * 1024 * 1024 // 限制文件大小為 5MB
    }
});

const sharp = require('sharp');

app.post('/uploadimg', upload.single('image'), async (req, res) => {
    const buffer = req.file.buffer;

    try {
        console.log(buffer)
        const processedBuffer = await sharp(buffer).resize({
            width             : 800,
            withoutEnlargement: true,
            fit               : sharp.fit.cover,
        }) // 調整圖片寬度為 800 pixels，保持原始的寬高比
            .jpeg({quality: 70}) // 設定 JPEG 質量為 70%
            .toBuffer();
        console.log(processedBuffer)
        const base64 = processedBuffer.toString('base64');
        res.json({success: true, base64: base64, image: `data:image/png;base64,${base64}`});

    }
    catch (error) {
        console.error('Processing failed:', error);
        res.status(500).json({success: false, message: 'Processing failed', error: error.message});
    }
});



app.get("/firebase", async (req, res, next) => {
    try {
        const checkDay1 = new Date();
        const checkDay2 = new Date(checkDay1.getTime() + 24 * 60 * 60 * 1000);
        // console.log(checkDay1, checkDay2);

        let content     = [];
        const citiesRef = db.collection('order');
        const snapshot  = await citiesRef.where('date', '==', checkDay2.format('yyyy-MM-dd')).where('status', '==', 1).get();
        snapshot.forEach(doc => {
            content.push(doc.data());
        });


        content.forEach((item) => {
            if (item.shopId === 'L1BhH0FzxJSlPLMVqsmU') {
                // let pay = payType(item.orderPay);
                let lneMsg = [{
                    "type"    : "flex",
                    "altText" : "預約提醒通知",
                    "contents": {
                        "type"  : "bubble",
                        "body"  : {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "contents": [
                                {
                                    "type"  : "text",
                                    "text"  : item.orderDetail.shopName,
                                    "weight": "bold",
                                    "size"  : "xl",
                                    "align" : "center"
                                },
                                {
                                    "type"  : "text",
                                    "text"  : '提醒您～明天有預約',
                                    "margin": "10px",
                                    "size"  : "18px",
                                    "color" : '#b43a3a',
                                    "weight": "bold",
                                    "align" : "center"
                                },
                                {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "margin"    : "20px",
                                    "spacing"   : "sm",
                                    "contents"  : [
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type"  : "text",
                                                    "text"  : "服務項目",
                                                    "color" : "#aaaaaa",
                                                    "size"  : "16px",
                                                    "flex"  : 2,
                                                    "weight": "regular"
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : item.orderDetail.serviceName,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type"  : "text",
                                                    "text"  : "服務人員",
                                                    "color" : "#aaaaaa",
                                                    "size"  : "16px",
                                                    "flex"  : 2,
                                                    "weight": "regular"
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : item.orderDetail.servicerName,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type" : "text",
                                                    "text" : "預約日期",
                                                    "color": "#aaaaaa",
                                                    "size" : "16px",
                                                    "flex" : 2,
                                                    "wrap" : true
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : item.date,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type" : "text",
                                                    "text" : "預約時間",
                                                    "color": "#aaaaaa",
                                                    "size" : "16px",
                                                    "flex" : 2,
                                                    "wrap" : true
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : item.time,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type" : "text",
                                                    "text" : "項目工時",
                                                    "color": "#aaaaaa",
                                                    "size" : "16px",
                                                    "flex" : 2,
                                                    "wrap" : true
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : item.orderDetail.showTime,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type" : "text",
                                                    "text" : "金額",
                                                    "color": "#aaaaaa",
                                                    "size" : "16px",
                                                    "flex" : 2,
                                                    "wrap" : true
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" :  (item.depositAmount) ? item.orderDetail.status+ item.depositAmount : item.orderDetail.status +' '+ item.amount,
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },
                                        {
                                            "type"         : "box",
                                            "layout"       : "baseline",
                                            "spacing"      : "sm",
                                            "contents"     : [
                                                {
                                                    "type" : "text",
                                                    "text" : "預約提醒",
                                                    "color": "#aaaaaa",
                                                    "size" : "16px",
                                                    "flex" : 2,
                                                    "wrap" : true
                                                },
                                                {
                                                    "type" : "text",
                                                    "text" : "明天有預約喔！",
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "16px",
                                                    "flex" : 5
                                                }
                                            ],
                                            "paddingBottom": "10px"
                                        },

                                    ],
                                    "flex"      : 0,
                                    "paddingTop": "10px"
                                }
                            ]
                        },
                        "footer": {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "spacing" : "sm",
                            "contents": [
                                {
                                    "type"  : "button",
                                    "style" : "link",
                                    "height": "sm",
                                    "action": {
                                        "type" : "uri",
                                        "label": "查看詳細",
                                        "uri"  : `https://newdate.app/common/order?orderId=${item.orderId}`
                                    }
                                },
                                {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "contents": [],
                                    "margin"  : "sm"
                                }
                            ],
                            "flex"    : 0
                        }
                    }
                }]

                // console.log(lneMsg);
                // 會員
                const configMember = {
                    method : 'post',
                    url    : 'https://node.newdate.app/lineMsgPush',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data   : {
                        message: lneMsg,
                        userId : item.lineData.linkLineMemberId,
                        token  : item.lineData.token,
                    }
                };

                axios(configMember).then(function (response) {
                    // console.log(JSON.stringify(response.data));
                }).catch(function (error) {
                    // console.log(error);
                });
                // 技術員
                const configTaff = {
                    method : 'post',
                    url    : 'https://node.newdate.app/lineMsgPush',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data   : {
                        message: lneMsg,
                        userId : item.lineData.linkLineTaffId,
                        token  : item.lineData.token,
                    }
                };

                axios(configTaff).then(function (response) {
                    // console.log(JSON.stringify(response.data));
                }).catch(function (error) {
                    // console.log(error);
                });

                //管理員
                if (item.lineData.linkLineAdminId && item.lineData.linkLineAdminId.length > 0) {
                    item.lineData.linkLineAdminId.filter(i => {
                        const configAdmin = {
                            method : 'post',
                            url    : 'https://node.newdate.app/lineMsgPush',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            data   : {
                                message: lneMsg,
                                userId : i.lineId,
                                token  : item.lineData.token,
                            }
                        };

                        axios(configAdmin).then(function (response) {
                            // console.log(JSON.stringify(response.data));
                        }).catch(function (error) {
                            // console.log(error);
                        });
                    })
                }
            }
        })
    }
    catch (err) {
        console.log(err)
    }
})


//每分鐘發送測試
// cron.schedule('* * * * *', async () => {
//
//     const data = {
//         method : 'post',
//         url    : 'https://node.newdate.app/payment/keep',
//         headers: {
//             'Content-Type': 'application/json',
//         },
//         data   : {
//             'Period': '6dbf1b719f183e4c18234ac1a3de136841b8977aeeec88081b326f4ffcac80e8f40f40f59e4d8bdd7733c2ad967744809aed224a33fa1c520e870daa8f61d2efae83c1efe29b2e1716c3b545ac2a447c48ea32fcc1cda4b4068f77b6b4282a644128677359184a6eaff4f7ae7c44bc839040ecaf1235a60bfbb20b44aa95268cb51866386e96c7421b1ee0b00ce9264e47c2bb2cf858aaf81f49adc82c6fc7755c6d47049769f9c2b85053166cdc224571723225d7d970a53df88249ac1afc645086870f86bf2fd24c3dc00a2fac014d067a222631197829160055ea27cf809d30cb8cbe050916b95780debcde4c7dc4b0ae8a7b2791db875fa68e58eb71b4449ee3bb9c3000f454a481069e3972292c6763890cfa717af9ecdfd83c8b4188b8b511f519f4f31f121d2b9a6f6945195c4e12b49123e038ef2df1923c61c88e659864a7dcdcc4b86a2032e8063881f567db13ba0180d85885f72103de5107b40eb9794b24e84fed1d847680990e6a570dc335d6dfd47344a1d40dc077b730ea11473c40eeec47b9f8ea2b8c693242412c9a9fe46b345be8aa7fa9bf8210ba04d1'
//             }
//         };
//
//     axios(data).then(function (response) {
//         // console.log(JSON.stringify(response.data));
//     }).catch(function (error) {
//         // console.log(error);
//     });
//
// })

// cron.schedule('0 * * * * *', async () => {
//     // let content     = [];
//     // const citiesRef = db.collection('order');
//     // const snapshot  = await citiesRef.where('date', '==', checkDay2.format('yyyy-MM-dd')).where('status', '==', 4).get();
//     // snapshot.forEach(doc => {
//     //     content.push(doc.data());
//     // });
// });
//
// // 工作排程
// cron.schedule('0 0 1 * * *', async () => {
//
// })

app.post('/cronMessage', async (req, res) => {
    const checkDay1 = new Date();
    const checkDay2 = new Date(checkDay1.getTime() + 24 * 60 * 60 * 1000);
    console.log(checkDay1, checkDay2);

    let content     = [];
    const citiesRef = db.collection('order');
    const snapshot  = await citiesRef.where('date', '==', checkDay2.format('yyyy-MM-dd')).where('status', '==', 1).get();
    snapshot.forEach(doc => {
        content.push(doc.data());
    });
    console.log(content);

    content.forEach((item) => {
        // if (item.shopId === 'ylk63OfnnIyRdmOtPCrn') {

        let pay = payType(item.orderPay);

        let lneMsg = [{
            "type"    : "flex",
            "altText" : "預約提醒通知",
            "contents": {
                "type"  : "bubble",
                "body"  : {
                    "type"    : "box",
                    "layout"  : "vertical",
                    "contents": [
                        {
                            "type"  : "text",
                            "text"  : item.orderDetail.shopName,
                            "weight": "bold",
                            "size"  : "xl",
                            "align" : "center"
                        },
                        {
                            "type"  : "text",
                            "text"  : '提醒您～明天有預約',
                            "margin": "10px",
                            "size"  : "18px",
                            "color" : '#b43a3a',
                            "weight": "bold",
                            "align" : "center"
                        },
                        {
                            "type"      : "box",
                            "layout"    : "vertical",
                            "margin"    : "20px",
                            "spacing"   : "sm",
                            "contents"  : [
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type"  : "text",
                                            "text"  : "服務項目",
                                            "color" : "#aaaaaa",
                                            "size"  : "16px",
                                            "flex"  : 2,
                                            "weight": "regular"
                                        },
                                        {
                                            "type" : "text",
                                            "text" : item.orderDetail.serviceName,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type"  : "text",
                                            "text"  : "服務人員",
                                            "color" : "#aaaaaa",
                                            "size"  : "16px",
                                            "flex"  : 2,
                                            "weight": "regular"
                                        },
                                        {
                                            "type" : "text",
                                            "text" : item.orderDetail.servicerName,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type" : "text",
                                            "text" : "預約日期",
                                            "color": "#aaaaaa",
                                            "size" : "16px",
                                            "flex" : 2,
                                            "wrap" : true
                                        },
                                        {
                                            "type" : "text",
                                            "text" : item.date,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type" : "text",
                                            "text" : "預約時間",
                                            "color": "#aaaaaa",
                                            "size" : "16px",
                                            "flex" : 2,
                                            "wrap" : true
                                        },
                                        {
                                            "type" : "text",
                                            "text" : item.time,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type" : "text",
                                            "text" : "項目工時",
                                            "color": "#aaaaaa",
                                            "size" : "16px",
                                            "flex" : 2,
                                            "wrap" : true
                                        },
                                        {
                                            "type" : "text",
                                            "text" : item.orderDetail.showTime,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type" : "text",
                                            "text" : "金額",
                                            "color": "#aaaaaa",
                                            "size" : "16px",
                                            "flex" : 2,
                                            "wrap" : true
                                        },
                                        {
                                            "type" : "text",
                                            "text" :  (item.depositAmount) ? item.orderDetail.status+ item.depositAmount: item.orderDetail.status +' '+ item.amount,
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },
                                {
                                    "type"         : "box",
                                    "layout"       : "baseline",
                                    "spacing"      : "sm",
                                    "contents"     : [
                                        {
                                            "type" : "text",
                                            "text" : "預約提醒",
                                            "color": "#aaaaaa",
                                            "size" : "16px",
                                            "flex" : 2,
                                            "wrap" : true
                                        },
                                        {
                                            "type" : "text",
                                            "text" : "明天有預約喔！",
                                            "wrap" : true,
                                            "color": "#666666",
                                            "size" : "16px",
                                            "flex" : 5
                                        }
                                    ],
                                    "paddingBottom": "10px"
                                },

                            ],
                            "flex"      : 0,
                            "paddingTop": "10px"
                        }
                    ]
                },
                "footer": {
                    "type"    : "box",
                    "layout"  : "vertical",
                    "spacing" : "sm",
                    "contents": [
                        {
                            "type"  : "button",
                            "style" : "link",
                            "height": "sm",
                            "action": {
                                "type" : "uri",
                                "label": "查看詳細",
                                "uri"  : `https://newdate.app/common/order?orderId=${item.orderId}`
                            }
                        },
                        {
                            "type"    : "button",
                            "style"   : "secondary",
                            "height"  : "sm",
                            "action"  : {
                                "type" : "message",
                                "label": "好的，我已確定",
                                "text" : `您已確定！`
                            },
                            "position": "relative"
                        }
                    ],
                    "flex"    : 0
                }
            }
        }]

        // 會員
        const configMember = {
            method : 'post',
            url    : 'https://node.newdate.app/lineMsgPush',
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                message: lneMsg,
                userId : item.lineData.linkLineMemberId,
                token  : item.lineData.token,
            }
        };

        axios(configMember).then(function (response) {
            // console.log(JSON.stringify(response.data));
        }).catch(function (error) {
            // console.log(error);
        });
        // 技術員
        const configTaff = {
            method : 'post',
            url    : 'https://node.newdate.app/lineMsgPush',
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                message: lneMsg,
                userId : item.lineData.linkLineTaffId,
                token  : item.lineData.token,
            }
        };

        axios(configTaff).then(function (response) {
            // console.log(JSON.stringify(response.data));
        }).catch(function (error) {
            // console.log(error);
        });

        //管理員
        // if (item.lineData.linkLineAdminId && item.lineData.linkLineAdminId.length > 0) {
        //     item.lineData.linkLineAdminId.filter(i => {
        //         const configAdmin = {
        //             method : 'post',
        //             url    : 'https://node.newdate.app/lineMsgPush',
        //             headers: {
        //                 'Content-Type': 'application/json',
        //             },
        //             data   : {
        //                 message: lneMsg,
        //                 userId : i.lineId,
        //                 token  : item.lineData.token,
        //             }
        //         };
        //
        //         axios(configAdmin).then(function (response) {
        //             // console.log(JSON.stringify(response.data));
        //         }).catch(function (error) {
        //             // console.log(error);
        //         });
        //     })
        // }
        // }
    })
})
app.post('/cronNotification', async (req, res) => {
    try {
        const targetDate = req.query.date || new Date().format('yyyy-MM-dd');
        console.log('Target date for notification:', targetDate);

        const memberRef = db.collection('member');
        const memberSnapshot = await memberRef.where('nextNotificationTime', '==', targetDate).get();

        if (memberSnapshot.empty) {
            console.log('No members found for date:', targetDate);
            return res.status(200).send({
                success: true,
                message: "No notifications needed for the specified date",
                date: targetDate
            });
        }

        const shopIds = new Set();
        memberSnapshot.forEach(doc => {
            shopIds.add(doc.data().shopId);
        });

        const lineDataMap = new Map();
        const lineRef = db.collection('lineData');
        const lineSnapshots = await Promise.all(
            Array.from(shopIds).map(shopId =>
                lineRef.where('shopId', '==', shopId).get()
            )
        );

        lineSnapshots.forEach(snapshot => {
            if (!snapshot.empty) {
                const data = snapshot.docs[0].data();
                lineDataMap.set(data.shopId, data);
            }
        });

        const memberContent = [];
        memberSnapshot.forEach(doc => {
            const memberData = doc.data();
            const lineData = lineDataMap.get(memberData.shopId);

            if (lineData) {
                memberContent.push({
                    memberId: doc.id,
                    lineData,
                    ...memberData
                });
            }
        });

        console.log('Found members to notify:', memberContent.length);

        if (req.query.dryRun === 'true') {
            return res.status(200).send({
                success: true,
                message: "Dry run completed",
                membersToNotify: memberContent.map(member => ({
                    memberId: member.memberId,
                    name: member.name,
                    shopId: member.shopId,
                    notificationTime: member.nextNotificationTime,
                    notificationDays: member.notificationDays
                }))
            });
        }

        const notifications = memberContent.map(member => {
            const buttonText = member.lineData.menuBtn == 4 ? '選擇地區' : '預約';
            const msgContent = member.lineData.notificationContent ? member.lineData.notificationContent : '距離上次造訪我們已經有一段時間了,期待您再次預約';

            const message = [{
                type: "flex",
                altText: "提醒通知",
                contents: {
                    type: "bubble",
                    body: {
                        type: "box",
                        layout: "vertical",
                        contents: [
                            {
                                type: "text",
                                text: `${member.name} 您好`,
                                weight: "bold",
                                size: "xl"
                            },
                            {
                                type: "box",
                                layout: "vertical",
                                margin: "lg",
                                spacing: "sm",
                                contents: [
                                    {
                                        type: "box",
                                        layout: "baseline",
                                        spacing: "sm",
                                        contents: [{
                                            type: "text",
                                            text: msgContent,
                                            wrap: true,
                                            color: "#666666",
                                            size: "sm"
                                        }]
                                    }
                                ]
                            }
                        ]
                    },
                    footer: {
                        type: "box",
                        layout: "vertical",
                        spacing: "sm",
                        contents: [
                            {
                                type: "button",
                                style: "primary",
                                height: "sm",
                                action: {
                                    type: "message",
                                    label: "立即預約",
                                    text: buttonText
                                },
                                color: "#555555"
                            }
                        ],
                        flex: 0
                    }
                }
            }];

            return axios({
                method: 'post',
                url: 'https://node.newdate.app/lineMsgPush',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    message,
                    userId: member.lineMemberId,
                    token: member.lineData.token
                }
            }).catch(error => ({
                error,
                memberId: member.memberId
            }));
        });

        console.log('Sending notifications...',memberContent);



        // 更新通知時間
        let updatedMembers = [];
        for (const member of memberContent) {
            try {
                console.log('Processing member update:', member.memberId);

                const days = parseInt(member.notificationDays || 365);
                const nextDate = new Date(member.nextNotificationTime);
                const newNextDate = new Date(nextDate.getTime() + days * 24 * 60 * 60 * 1000);
                const formattedDate = newNextDate.format('yyyy-MM-dd');



                await memberRef.doc(member.memberId).update({
                    nextNotificationTime: formattedDate
                });

                updatedMembers.push({
                    memberId: member.memberId,
                    newDate: formattedDate
                });
            } catch (error) {
                console.error('Error updating member:', member.memberId, error);
            }
        }

        console.log('Updated members:', updatedMembers);

        res.status(200).send({
            success: true,
        });

    } catch (err) {
        console.error('Error in notification test:', err);
        res.status(500).send({
            success: false,
            error: err.message,
        });
    }
});

app.get('/notificationTest', async (req, res) => {
    try {
        const targetDate = req.query.date || new Date().format('yyyy-MM-dd');
        console.log('Target date for notification:', targetDate);

        const memberRef = db.collection('member');
        const memberSnapshot = await memberRef.where('nextNotificationTime', '==', targetDate).get();

        if (memberSnapshot.empty) {
            console.log('No members found for date:', targetDate);
            return res.status(200).send({
                success: true,
                message: "No notifications needed for the specified date",
                date: targetDate
            });
        }

        const shopIds = new Set();
        memberSnapshot.forEach(doc => {
            shopIds.add(doc.data().shopId);
        });

        const lineDataMap = new Map();
        const lineRef = db.collection('lineData');
        const lineSnapshots = await Promise.all(
            Array.from(shopIds).map(shopId =>
                lineRef.where('shopId', '==', shopId).get()
            )
        );

        lineSnapshots.forEach(snapshot => {
            if (!snapshot.empty) {
                const data = snapshot.docs[0].data();
                lineDataMap.set(data.shopId, data);
            }
        });

        const memberContent = [];
        memberSnapshot.forEach(doc => {
            const memberData = doc.data();
            const lineData = lineDataMap.get(memberData.shopId);

            if (lineData) {
                memberContent.push({
                    memberId: doc.id,
                    lineData,
                    ...memberData
                });
            }
        });

        console.log('Found members to notify:', memberContent.length);

        if (req.query.dryRun === 'true') {
            return res.status(200).send({
                success: true,
                message: "Dry run completed",
                membersToNotify: memberContent.map(member => ({
                    memberId: member.memberId,
                    name: member.name,
                    shopId: member.shopId,
                    notificationTime: member.nextNotificationTime,
                    notificationDays: member.notificationDays
                }))
            });
        }

        const notifications = memberContent.map(member => {
            const buttonText = member.lineData.menuBtn == 4 ? '選擇地區' : '預約';
            const msgContent = member.lineData.notificationContent ? member.lineData.notificationContent : '距離上次造訪我們已經有一段時間了,期待您再次預約';

            const message = [{
                type: "flex",
                altText: "提醒通知",
                contents: {
                    type: "bubble",
                    body: {
                        type: "box",
                        layout: "vertical",
                        contents: [
                            {
                                type: "text",
                                text: `${member.name} 您好`,
                                weight: "bold",
                                size: "xl"
                            },
                            {
                                type: "box",
                                layout: "vertical",
                                margin: "lg",
                                spacing: "sm",
                                contents: [
                                    {
                                        type: "box",
                                        layout: "baseline",
                                        spacing: "sm",
                                        contents: [{
                                            type: "text",
                                            text: msgContent,
                                            wrap: true,
                                            color: "#666666",
                                            size: "sm"
                                        }]
                                    }
                                ]
                            }
                        ]
                    },
                    footer: {
                        type: "box",
                        layout: "vertical",
                        spacing: "sm",
                        contents: [
                            {
                                type: "button",
                                style: "primary",
                                height: "sm",
                                action: {
                                    type: "message",
                                    label: "立即預約",
                                    text: buttonText
                                },
                                color: "#555555"
                            }
                        ],
                        flex: 0
                    }
                }
            }];

            return axios({
                method: 'post',
                url: 'https://node.newdate.app/lineMsgPush',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    message,
                    userId: member.lineMemberId,
                    token: member.lineData.token
                }
            }).catch(error => ({
                error,
                memberId: member.memberId
            }));
        });

        console.log('Sending notifications...',memberContent);



        // 更新通知時間
        let updatedMembers = [];
        for (const member of memberContent) {
            try {
                console.log('Processing member update:', member.memberId);

                const days = parseInt(member.notificationDays || 365);
                const nextDate = new Date(member.nextNotificationTime);
                const newNextDate = new Date(nextDate.getTime() + days * 24 * 60 * 60 * 1000);
                const formattedDate = newNextDate.format('yyyy-MM-dd');



                await memberRef.doc(member.memberId).update({
                    nextNotificationTime: formattedDate
                });

                updatedMembers.push({
                    memberId: member.memberId,
                    newDate: formattedDate
                });
            } catch (error) {
                console.error('Error updating member:', member.memberId, error);
            }
        }

        console.log('Updated members:', updatedMembers);

        res.status(200).send({
            success: true,
        });

    } catch (err) {
        console.error('Error in notification test:', err);
        res.status(500).send({
            success: false,
            error: err.message,
        });
    }
});

const payType = (pay) => {
    if(pay == "bankTransfer"){
        return '銀行匯款'
    }

    if(pay == "creditCard"){
        return '信用卡支付'
    }

    if(pay == "linePay"){

        return 'LINE Pay'

    }
    if(pay == "cash"){

        return '到店支付'

    }
}

function findFollowDataByDealerId(data, targetDealerId) {
    if (!data || !Array.isArray(data)) {
        return [];
    }

    const results = [];
    for (const item of data) {
        if (item.dealerId === targetDealerId) {
            results.push(item);
        }

        const deeperResults = findFollowDataByDealerId(item.children, targetDealerId);
        if (deeperResults.length > 0) {
            results.push(...deeperResults);
        }
    }

    return results;
}

function extractChildrenThreeLevels(data, level = 0) {

    if (level >= 4 || !Array.isArray(data)) {
        return [];
    }

    const extractedChildren = [];

    for (const item of data) {
        const extractedItem = {
            dealerCode: item.dealerCode,
            name      : item.name,
            cut       : dealerType(level).cut,
            title     : dealerType(level).title,
            children  : extractChildrenThreeLevels(item.children, level + 1)
        };
        extractedChildren.push(extractedItem);
    }

    return extractedChildren;
}

//取出付費的下線 詳細內容
const listPaymentCut = async (item) => {
    item.followlineData.forEach(async (val) => {
        await fetchPaymentData(val);
    });
    let total    = 0;
    let totalCut = 0;
    let level1   = [];
    let level2   = [];
    let level3   = [];
    let level4   = [];
    setTimeout(async () => {
        await item.followlineData.filter(val => {
            if (val.paymentList) {
                let sum = 0;
                val.paymentList.filter(val => {
                    sum += val.PeriodAmt
                })
                val.totleAmt = sum;
                total += sum
                totalCut += Math.round(sum * val.cut)
                level1.push(val)
                if (val.children.length > 0) {
                    val.children.filter(val2 => {
                        if (val2.paymentList) {
                            let sum = 0;
                            val2.paymentList.filter(val => {
                                sum += val.PeriodAmt
                            })
                            val2.totleAmt = sum;
                            total += sum
                            totalCut += Math.round(sum * val2.cut)
                            level2.push(val2)
                            if (val2.children.length > 0) {
                                val2.children.filter(val3 => {
                                    if (val3.paymentList) {
                                        let sum = 0;
                                        val3.paymentList.filter(val => {
                                            sum += val.PeriodAmt
                                        })
                                        val3.totleAmt = sum;
                                        total += sum
                                        totalCut += Math.round(sum * val3.cut)
                                        level3.push(val3)
                                        if (val3.children.length > 0) {
                                            val3.children.filter(val4 => {
                                                if (val4.paymentList) {
                                                    let sum = 0;
                                                    val4.paymentList.filter(val => {
                                                        sum += val.PeriodAmt
                                                    })
                                                    val4.totleAmt = sum
                                                    total += sum
                                                    totalCut += Math.round(sum * val4.cut)
                                                    level4.push(val4)
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        })

        let params        = await {
            "dealerProfitId": "",
            "created_at"    : new Date().format("yyyy-MM-dd hh:mm:ss"),
            "amount"        : total,
            "profitAmount"  : totalCut,
            "date"          : new Date().format("yyyy-MM-dd"),
            "dealerId"      : item.dealerId,
            "detail"        : item,
            "month"         : new Date().format("MM"),
            "pay"           : 0,
            "year"          : new Date().format("yyyy"),
        }
        params.created_at = await new Date().format("yyyy-MM-dd hh:mm:ss");
        const docRef      = await db.collection('dealerProfitHistory').add(params);
        const result      = await docRef.update({
            dealerProfitId: docRef.id
        });
    }, 1000)
}
const dealerType     = (number) => {
    const arr = [
        {
            title: '10%',
            cut  : 0.1,
            value: 0
        },
        {
            title: '5%',
            cut  : 0.05,
            value: 1
        },
        {
            title: '3%',
            cut  : 0.03,
            value: 2
        },
        {
            title: '2%',
            cut  : 0.02,
            value: 3
        },
    ]
    return arr[number];
}

async function fetchPaymentData(item) {
    let content     = await []
    const citiesRef = db.collection('payment');
    const snapshot  = await citiesRef.where('userInfo.dealerId', '==', item.dealerCode).where("payStatus", "==", true).get();
    snapshot.forEach(doc => {
        content.push(doc.data());
    });
    item.paymentList = await content;
    if (item.children && item.children.length > 0) {
        await Promise.all(item.children.map(async (child) => {
            await fetchPaymentData(child);
        }));
    }
}


// 每個月算經銷商分潤
// cron.schedule('0 23 28-31 * *' , async () => {
//     try {
//
//         let userArr     = [];
//         const citiesRef = db.collection('dealerData');
//         const snapshot  = await citiesRef.where('lineCode', '==', true).get();
//         snapshot.forEach(doc => {
//             userArr.push(doc.data());
//         });
//
//         let followJson        = null;
//         const citiesRefFollow = db.collection('dealerFollow');
//         const snapshotFollow  = await citiesRefFollow.where('dealerId', '==', "wqtzuAP1MWZyQHKcbtmN").get();
//         snapshotFollow.forEach(doc => {
//             followJson = doc.data();
//         });
//
//         userArr.filter(item => {
//             const extractedData = findFollowDataByDealerId([followJson], item.dealerId);
//             item.followlineData = extractChildrenThreeLevels(extractedData);
//             listPaymentCut(item)
//         })
//         setTimeout(() => {
//             res.status(200).send(userArr)
//         }, 5000)
//     }
//     catch (err) {
//         console.log(err)
//     }
// })


app.post('/getlink', async (req, res) => {
    let formData = req.body;
    console.log('form data888', req.body);
    // console.log('form data777', formData.code)

    //取得member Data資料
    let content = null;
    const citiesRef = db.collection('lineData');
    const snapshot = await citiesRef.where('shopId', '==', formData.state).get();
    snapshot.forEach(doc => {
        content = doc.data();
    });

    var options = {
        'method': 'POST',
        'url': 'https://notify-bot.line.me/oauth/token',
        'headers': {
            'Cookie': 'XSRF-TOKEN=9ab3e848-ba0e-4f3a-9c54-8079be7aa2e6'
        },
        formData: {
            'grant_type': 'authorization_code',
            'redirect_uri': 'https://node.newdate.app/getlink',
            'code': formData.code,
            'client_id': 'HPMbQdIqPKSyqdk7IlVl2b',
            'client_secret': 'GHvc16TDkoR8KXGWZk6fZiwB6mrJXgTe6UAB35uC0vO'
        }
    };

    request(options, function (error, response) {
        if (error) throw new Error(error);
        var getData = JSON.parse(response.body)

        // fireData.ref('adminLine').set({token:'Bearer '+getData.access_token })
        console.log('999 ' + getData);
        console.log('Bearer ' + getData.access_token);

        db.collection('lineData').doc(formData.state).update({
            shopId: formData.state,
            freeLineToken: 'Bearer ' + getData.access_token,
            freeLine: true
        });
        res.status(200).send('設定成功！關閉此頁面')
    });


})


app.post("/getMsg", async (req, res, next) => {
    console.log(req.body);
    try {

        //取得lineData資料
        // let content     = [];
        // const citiesRef = db.collection('lineData');
        // const snapshot  = await citiesRef.where('shopLinkId', '==', req.body.shopLinkId).get();
        // snapshot.forEach(doc => {
        //     content = doc.data();
        // });
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-api.line.me/api/notify',
            'headers': {
                'Authorization': req.body.freeLineToken,
            },
            formData : {
                'message': req.body.message
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
            res.status(200).send({
                result: true,
            });
        });
    }
    catch (err) {
        console.log(err)
    }
})




app.post('/link', async (req, res) => {
    let formData = req.body;
    // console.log('form data888', req.body);
    // console.log('form data777', formData.code)

    //取得lineData資料
    // let content     = [];
    // const citiesRef = db.collection('lineData');
    // const snapshot  = await citiesRef.where('userId', '==', formData.state).get();
    // snapshot.forEach(doc => {
    //     content = doc.data();
    // });

    try {
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-bot.line.me/oauth/token',
            'headers': {
                'Cookie': 'XSRF-TOKEN=9ab3e848-ba0e-4f3a-9c54-8079be7aa2e6'
            },
            formData : {
                'grant_type'   : 'authorization_code',
                'redirect_uri' : 'https://node.newdate.app/link',
                'code'         : formData.code,
                'client_id'    : 'vjXnEotXzAReT8VKxQ8JAa',
                'client_secret': 'paTlqnFKXdIL570lItWeGZKa3pJEFqaBi2VA3thCmBU'
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            var getData = JSON.parse(response.body)

            // fireData.ref('adminLine').set({token:'Bearer '+getData.access_token })
            console.log('Bearer ' + getData.access_token);
            //
            // db.collection('lineData').doc(content.userId).set(content);

            res.status(200).send('設定成功！')
            // res.location(content.siteUrl);
            // res.redirect(content.siteUrl);
        });
    }
    catch (err) {
        console.log(err)
    }
})

app.post("/getMsg2", async (req, res, next) => {
    // console.log(req.body);
    try {

        //取得lineData資料
        // let content     = [];
        // const citiesRef = db.collection('lineData');
        // const snapshot  = await citiesRef.where('shopLinkId', '==', req.body.shopLinkId).get();
        // snapshot.forEach(doc => {
        //     content = doc.data();
        // });
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-api.line.me/api/notify',
            'headers': {
                'Authorization': req.body.token,
            },
            formData : {
                'message': req.body.message
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            // console.log(response.body);
            res.status(200).send({
                result: true,
            });
        });
    }
    catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 token
app.post("/smsToken", async (req, res, next) => {
    // console.log(req.body);
    try {
        //取得lineData資料
        // let tdata    = {
        //   "HandlerType": 3,
        //   "VerifyType" : 1,
        //   "UID"        : "ye0205414225",
        //   "PWD"        : "Aa859230"
        // };
        // var options = {
        //   'method' : 'POST',
        //   'url'    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
        //   'headers': {
        //     'Content-Type': 'application/json'
        //   },
        //   formData : tdata
        // };
        //
        // axios.post('http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',tdata).then(res=>{
        //   res.status(200).send({
        //     result:res.data,
        //   });
        // });

        var data = JSON.stringify({"HandlerType": 3, "VerifyType": 1, "UID": "ye0205414225", "PWD": "Aa859230"});

        var config = {
            method : 'post',
            url    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
            headers: {
                'Content-Type': 'application/json'
            },
            data   : data
        };

        axios(config).then(function (response) {
            res.status(200).send({
                result: response.data,
            });
        }).catch(function (error) {
            console.log(error);
        });


        // request(options, function (error, response) {
        //   if (error) throw new Error(error);
        //   res.status(200).send({
        //     result:response,
        //   });
        // });
    }
    catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 送出內容
app.post("/smsSend", async (req, res, next) => {
    try {
        var options = {
            'method' : 'POST',
            'url'    : 'http://api.every8d.com/API21/HTTP/SendSMS.ashx',
            'headers': {
                'Authorization': req.body.token
            },
            formData : req.body.content
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            res.status(200).send({
                result: true,
            });
        });
    }
    catch (err) {
        console.log(err)
    }
})

// Line Pay 送出訂單
app.post("/linepay", async (req, res, next) => {
    let linePay = new LinePay({
        channelId    : req.body.linePayData.channelId,
        channelSecret: req.body.linePayData.channelSecret,
        uri          : req.body.linePayData.uri
    })
    // console.log(req.body.order.packages[0].products)

    linePay.request(req.body.order).then(response => {
        // console.log( response.info.paymentUrl.web)
        if (response.returnCode == '0000') {
            // console.log(response);
            // res.redirect(response.info.paymentUrl.web)
            // res.location(response.info.paymentUrl.web0);
            res.status(200).send({data: response});
        }
    });
});
// Line Pay 送出訂單
app.get("/linepaytest", async (req, res, next) => {
    let linePay = new LinePay({
        channelId    : "1657022987",//'1657022987',//
        channelSecret: "062dc13e35c24bdae59490c4fd0c20a7", //,'062dc13e35c24bdae59490c4fd0c20a7'
        uri          : "https://sandbox-api-pay.line.me"
    })

    const order = {
        amount      : 5000,
        currency    : 'TWD',
        orderId     : 'Order2019101500001',
        packages    : [
            {
                id      : 'Item20191015001',
                amount  : 5000,
                name    : 'testPackageName',
                products: [
                    {
                        name    : 'testProductName',
                        quantity: 1,
                        price   : 2500
                    },
                    {
                        name    : 'testProductName2',
                        quantity: 1,
                        price   : 2500
                    },
                ]
            }
        ],
        redirectUrls: {
            confirmUrl: 'http://localhost/confirmUrl',
            cancelUrl : 'http://localhost/cancelUrl'
        }
    }

    linePay.request(order).then(response => {
        res.redirect(response.info.paymentUrl.web)
    })
});
app.get("/confirmUrl", async (req, res, next) => {

    var urlObj          = url.parse(req.url, true)
    const transactionId = urlObj.query.transactionId;
    const orderId       = urlObj.query.orderId;

    //取得訂單資料
    let content     = [];
    const citiesRef = db.collection('orderLink');
    const snapshot  = await citiesRef.where('orderLinkId', '==', orderId).get();
    snapshot.forEach(doc => {
        content = doc.data();
    });

    //取得line資料
    let lineData  = [];
    const line    = db.collection('onlinePay');
    const getLine = await line.where('shopId', '==', content.shopId).get();
    getLine.forEach(doc => {
        lineData = doc.data();
    });

    //產品價格
    const confrimData = {
        amount  : content.amountTotal,
        currency: 'TWD',
    }

    let linepayApi = 'https://api-pay.line.me';

    if (lineData.shopId == 'L1BhH0FzxJSlPLMVqsmU') {
        linepayApi = 'https://sandbox-api-pay.line.me';
    }

    let linePay = new LinePay({
        channelId    : lineData.linePay.channelID,
        channelSecret: lineData.linePay.channelSecret,
        uri          : linepayApi
    })
    // console.log(confrimData, transactionId, 444)
    await linePay.confirm(confrimData, transactionId).then(response => {
        // console.log(response)
        if (response.returnCode == '0000') {
            // res.redirect(`https://younee.app/confirm?transactionId=${transactionId}&orderId=${orderId}`)
            res.redirect(`https://newdate.app/member/order-result?orderlinkId=${orderId}&transactionId=${transactionId}`)
        }
    })


})
app.post("/cancel", async (req, res, next) => {
    console.log(req.body)
    let linepayApi = 'https://api-pay.line.me';

    if (req.body.shopId == 'L1BhH0FzxJSlPLMVqsmU') {
        linepayApi = 'https://sandbox-api-pay.line.me';
    }

    let linePay = new LinePay({
        channelId    : req.body.channelId,
        channelSecret: req.body.channelSecret,
        uri          : linepayApi
    })

    linePay.refund({refundAmount: null}, req.body.transactionId).then(response => {
        // console.log(response)
        res.status(200).send(response);
    })
})

app.post("/", async (req, res, next) => {

    console.log(req.body)

    res.status(200).send('newdata');
})

app.post("/calendartest", async (req, res, next) => {

    console.log(req.body)

    res.status(200).send('calendartest');
})

app.post("/lineMsgPush", async (req, res, next) => {
    // console.log(req.body, 999);
    var data = JSON.stringify({"to": req.body.userId, "messages": req.body.message});

    var config = {
        method : 'post',
        url    : 'https://api.line.me/v2/bot/message/push',
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': 'Bearer ' + req.body.token
        },
        data   : data
    };

    axios(config).then(function (response) {
        // console.log(JSON.stringify(response.data));
    }).catch(function (error) {
        // console.log(error);
    });

})
app.post("/webhook", async (req, res, next) => {
    // console.log(req.body, 555)
    // console.log(req.body.events, 666)
    // console.log(req.body.events[0].source.userId, 777) // 查詢資料表lineData 是有符合 有的話撈出token


    var myLinkline = false
    if (req.body.events[0].message.text == '綁定自動回覆') {
        let params      = [];
        const citiesRef = db.collection('lineData');
        const snapshot  = await citiesRef.where('shopLineId', '==', req.body.events[0].source.userId).get();
        await snapshot.forEach(doc => {
            params.push(doc.data());
        });
        if (params.length > 0) {
            params.filter(async (val) => {
                val.destination = await req.body.destination;
                const ref       = db.collection('lineData');
                ref.doc(val.shopId).set(val);
            })
            myLinkline = true;
        }
    }


    let content     = [];
    let pushContent = [];
    const citiesRef = db.collection('lineData');
    const snapshot  = await citiesRef.where('destination', '==', req.body.destination).get();
    snapshot.forEach(doc => {
        pushContent.push(doc.data());
        content = doc.data();
    });


    const client = new line.Client({
        channelAccessToken: content.token
    });


    if (myLinkline) {
        const message = [{
            "type": "text",
            "text": "已綁定，請輸入『預約』以及『登入』",
        }]

        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }

    if (await req.body.events[0].message.text == '看範例後台介面') {
        if(req.body.destination == 'Uecb7d160b6c9c23ed0aedfee7a4f54f6'){
            let　message2 = await [
                {
                    "type"    : "flex",
                    "altText" : '測試範例後台登入',
                    "contents": {
                        "type"  : "bubble",
                        "body"  : {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "contents": [
                                {
                                    "type"  : "text",
                                    "text"  : '測試範例後台登入',
                                    "weight": "bold",
                                    "size"  : "xl"
                                },
                                {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "margin"  : "lg",
                                    "spacing" : "sm",
                                    "contents": [
                                        {
                                            "type"    : "box",
                                            "layout"  : "baseline",
                                            "spacing" : "sm",
                                            "contents": [
                                                {
                                                    "type" : "text",
                                                    "text" : '點擊下方按鈕進入測試範例後台。',
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "sm",
                                                }
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        "footer": {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "spacing" : "sm",
                            "contents": [
                                {
                                    "type"  : "button",
                                    "style" : "primary",
                                    "height": "sm",
                                    "action": {
                                        "type" : "uri",
                                        "label": "登入",
                                        "uri"  : `https://newdate.app/login?demo=true`
                                    },
                                    "color" : "#555555"
                                },
                                {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "contents": [],
                                    "margin"  : "sm"
                                }
                            ],
                            "flex"    : 0
                        }
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message2).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
    }



    const largeAreaList = [
        "不分類",
        "北部(北北基桃竹)",
        "中部(苗中彰投雲)",
        "南部(嘉南高屏)",
        "東部(宜花東)",
        "外島(澎金馬)",
        "其他",
    ];

    if (largeAreaList.includes(req.body.events[0].message.text)) {

        if (pushContent.length > 0) {
            var button = [];

            // 使用 Promise.all 來確保所有非同步操作完成後再進行下一步
            await Promise.all(
                pushContent.map(async (val) => {
                if (val.menuBtn == 4) {
                    // 商店資訊
                    const citiesRef2 = db.collection('shopData');
                    const snapshot2 = await citiesRef2.where('shopId', '==', val.shopId).get();

                    // 遍歷 snapshot2 並將結果推入 button 陣列
                    snapshot2.forEach(doc => {
                        if (doc.data().largeArea == req.body.events[0].message.text) {
                            button.push({
                                "type": "button",
                                "style": "primary",
                                "height": "sm",
                                "action": {
                                    "type": "message",
                                    "label": doc.data().shopName,
                                    "text": '選擇分店'+doc.data().shopName
                                },
                                "color": "#555555"
                            });
                        }
                    });
                }
                })
            );

            await pushContent.map(async (val) => {
                if (val.menuBtn == 4) {
                    if(val.CustomizeBtn){
                        button.push({
                            "type": "button",
                            "style": "primary",
                            "height": "sm",
                            "action": {
                                "type": "message",
                                "label": val.CustomizeBtn,
                                "text": (val.CustomizeText)?val.CustomizeText:'請聯繫客服'
                            },
                            "color": "#555555"
                        });
                    }
                }
            })

            let message = [
                {
                    "type": "flex",
                    "altText": '預約',
                    "contents": {
                        "type": "bubble",
                        "body": {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "text",
                                    "text": req.body.events[0].message.text,
                                    "weight": "bold",
                                    "size": "xl"
                                },
                                {
                                    "type": "box",
                                    "layout": "vertical",
                                    "margin": "lg",
                                    "spacing": "sm",
                                    "contents": [
                                        {
                                            "type": "box",
                                            "layout": "baseline",
                                            "spacing": "sm",
                                            "contents": [
                                                {
                                                    "type": "text",
                                                    "text": '點擊下方分店',
                                                    "wrap": true,
                                                    "color": "#666666",
                                                    "size": "sm",
                                                }
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        "footer": {
                            "type": "box",
                            "layout": "vertical",
                            "spacing": "sm",
                            "contents": button,
                            "flex": 0
                        }
                    }
                }
            ];

            try {
                await client.replyMessage(req.body.events[0].replyToken, message);
                res.status(200).send('OK');
            } catch (err) {
                console.log(err);
            }
        }

    }


    if (req.body.events[0].message.text.includes('選擇分店')) {


        const shopName = req.body.events[0].message.text.replace(/選擇分店/, "") //取得字串

        const shopRef = db.collection('shopData');
        const shopsnapshot = await shopRef.where('shopName', '==', shopName).get();
        const shopId = shopsnapshot.empty ? '' : shopsnapshot.docs[0].data().shopId;

        const citiesRef = db.collection('lineData');
        const snapshot = await citiesRef.where('shopId', '==', shopId).get();
        let liffId = snapshot.empty ? '' : snapshot.docs[0].data().liffId;

        // 取得該服務項目的服務人員
        const getFirebaseStoragePublicUrl = async (shopId, avatarImg ,taffId) => {
            if(taffId){
                console.log(taffId,1)
                console.log(avatarImg,2)
                if(avatarImg){
                    console.log(avatarImg,3)
                    return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservicer%2F${shopId}%2F${avatarImg}?alt=media`;
                }else{
                    const taffDB = db.collection('taffData');
                    const taffData = await taffDB.where('taffId', '==', taffId).get();
                    const taffAvatar = taffData.docs[0].data().avatar
                    console.log(taffAvatar,3)
                    if(taffAvatar){
                        return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/taff%2Favatar%2F${taffId}%2F${taffAvatar}?alt=media`;
                    }else {
                        return '';
                    }
                }
            }else{

                if(avatarImg){
                    return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservice%2F${shopId}%2F${avatarImg}?alt=media`;
                }else{
                    return '';
                }

            }
        };

        const servicerDB = db.collection('servicer');
        const servicerDateSnapshot = await servicerDB.where('shopId', '==', shopId).where('switch', '==', true).get();

        const serviceDB = db.collection('service');
        const serviceDateSnapshot = await serviceDB.where('shopId', '==', shopId).where('switch', '==', true).get();


        const avatarShop = 'https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/notimg.jpg?alt=media';
        let servicerContent = [];

        // 將快照轉換為陣列，並按排序進行排序
        const servicerDateArray = servicerDateSnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
        })).sort((a, b) => a.sort - b.sort);
        const serviceDateArray = serviceDateSnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
        })).sort((a, b) => a.sort - b.sort);
        for (const doc of servicerDateArray) {
            let servicerName = doc.servicerName ? doc.servicerName : '未設定名稱';
            let servicerId = doc.servicerId ? doc.servicerId : '---';
            let taffId = doc.taffId ? doc.taffId : '';

            let infoformation = doc.infoformation ? doc.infoformation : '---';
            let avatarImg = doc.img.length>0 ? doc.img[0] : false;
            let getImageUrl2 = await getFirebaseStoragePublicUrl(shopId, avatarImg,taffId) ;



            servicerContent.push(
                {
                            "type": "bubble",
                            "body": {
                                "type": "box",
                                "layout": "vertical",
                                "contents": [
                                    {
                                        "type": "image",
                                        "url": getImageUrl2 ? getImageUrl2 : avatarShop,
                                        "size": "full",
                                        "aspectMode": "cover",
                                        "aspectRatio": "2:3",
                                        "gravity": "top"
                                    },
                                    {
                                        "type": "box",
                                        "layout": "vertical",
                                        "contents": [
                                            {
                                                "type": "box",
                                                "layout": "vertical",
                                                "contents": [
                                                    {
                                                        "type": "text",
                                                        "text": servicerName,
                                                        "size": "xl",
                                                        "color": "#ffffff",
                                                        "weight": "bold"
                                                    }
                                                ]
                                            },
                                            {
                                                "type": "box",
                                                "layout": "baseline",
                                                "margin": "md",
                                                "contents": [
                                                    {
                                                        "type": "icon",
                                                        "size": "sm",
                                                        "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                                    },
                                                    {
                                                        "type": "icon",
                                                        "size": "sm",
                                                        "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                                    },
                                                    {
                                                        "type": "icon",
                                                        "size": "sm",
                                                        "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                                    },
                                                    {
                                                        "type": "icon",
                                                        "size": "sm",
                                                        "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                                    },
                                                    {
                                                        "type": "icon",
                                                        "size": "sm",
                                                        "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                                    },
                                                    {
                                                        "type": "text",
                                                        "text": "5",
                                                        "size": "sm",
                                                        "color": "#999999",
                                                        "margin": "md",
                                                        "flex": 0
                                                    }
                                                ],
                                                "height": "30px"
                                            },
                                            {
                                                "type": "box",
                                                "layout": "baseline",
                                                "contents": [
                                                    {
                                                        "type": "text",
                                                        "color": "#ebebeb",
                                                        "size": "sm",
                                                        "flex": 0,
                                                        "text": infoformation,
                                                        "wrap": true,
                                                        "position": "absolute",
                                                        "offsetBottom": "0px"
                                                    }
                                                ],
                                                "spacing": "lg",
                                                "height": "290px"
                                            },
                                            {
                                                "type": "box",
                                                "layout": "vertical",
                                                "contents": [
                                                    {
                                                        "type": "filler"
                                                    },
                                                    {
                                                        "type": "box",
                                                        "layout": "baseline",
                                                        "contents": [
                                                            {
                                                                "type": "filler"
                                                            },
                                                            {
                                                                "type": "text",
                                                                "text": "立即預約",
                                                                "color": "#ffffff",
                                                                "flex": 0,
                                                                "offsetTop": "-2px",
                                                                "action": {
                                                                    "type": "uri",
                                                                    "label": "action",
                                                                    "uri": `https://liff.line.me/${liffId}?servicerId${servicerId}`
                                                                }
                                                            },
                                                            {
                                                                "type": "filler"
                                                            }
                                                        ],
                                                        "spacing": "sm"
                                                    },
                                                    {
                                                        "type": "filler"
                                                    }
                                                ],
                                                "borderWidth": "1px",
                                                "cornerRadius": "4px",
                                                "spacing": "sm",
                                                "borderColor": "#ffffff",
                                                "margin": "xxl",
                                                "height": "40px"
                                            }
                                        ],
                                        "position": "absolute",
                                        "offsetBottom": "0px",
                                        "offsetStart": "0px",
                                        "offsetEnd": "0px",
                                        "paddingTop": "18px",
                                        "background": {
                                            "type": "linearGradient",
                                            "angle": "0deg",
                                            "startColor": "#000000D7",
                                            "endColor": "#00000000"
                                        },
                                        "paddingAll": "20px",
                                    }
                                ],
                                "paddingAll": "0px"
                            }
                        }
            )

           /* servicerContent.push({
                "type": "bubble",
                "size": "micro",
                "hero": {
                    "type": "image",
                    "url": getImageUrl2 ? getImageUrl2 : avatarShop,
                    "size": "full",
                    "aspectMode": "cover",
                    "aspectRatio": "1:1"
                },
                "body": {
                    "type": "box",
                    "layout": "vertical",
                    "contents": [
                        {
                            "type": "text",
                            "text": servicerName,
                            "weight": "bold",
                            "size": "sm",
                            "wrap": true
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "box",
                                    "layout": "baseline",
                                    "spacing": "sm",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": infoformation,
                                            "wrap": true,
                                            "color": "#8c8c8c",
                                            "size": "xs",
                                            "flex": 5
                                        }
                                    ],
                                    "height": "65px",
                                    "margin": "5px"
                                }
                            ]
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "button",
                                    "action": {
                                        "type": "uri",
                                        "label": "預約",
                                        "uri": `https://liff.line.me/${liffId}?servicerId${servicerId}`
                                    },
                                    "offsetTop": "-6px",
                                    "color": "#ffffff"
                                }
                            ],
                            "backgroundColor": "#999999",
                            "cornerRadius": "lg",
                            "height": "40px"
                        }
                    ],
                    "spacing": "sm",
                    "paddingAll": "13px"
                }
            });*/
        }
        for (const doc of serviceDateArray) {
            let servicerName = doc.serviceName ? doc.serviceName : '未設定名稱';
            let servicerId = doc.servicerIdarr && doc.servicerIdarr.length>0 ? doc.servicerIdarr[0] : 'null';
            // let taffId = doc.taffId ? doc.taffId : '';

            let infoformation = doc.infoformation ? doc.infoformation : '---';
            let avatarImg = doc.img.length>0 ? doc.img[0] : false;
            let getImageUrl2 = await getFirebaseStoragePublicUrl(shopId, avatarImg) ;



            servicerContent.push(
                {
                    "type": "bubble",
                    "body": {
                        "type": "box",
                        "layout": "vertical",
                        "contents": [
                            {
                                "type": "image",
                                "url": getImageUrl2 ? getImageUrl2 : avatarShop,
                                "size": "full",
                                "aspectMode": "cover",
                                "aspectRatio": "2:3",
                                "gravity": "top"
                            },
                            {
                                "type": "box",
                                "layout": "vertical",
                                "contents": [
                                    {
                                        "type": "box",
                                        "layout": "vertical",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "text": servicerName,
                                                "size": "xl",
                                                "color": "#ffffff",
                                                "weight": "bold"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "box",
                                        "layout": "baseline",
                                        "margin": "md",
                                        "contents": [
                                            {
                                                "type": "icon",
                                                "size": "sm",
                                                "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                            },
                                            {
                                                "type": "icon",
                                                "size": "sm",
                                                "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                            },
                                            {
                                                "type": "icon",
                                                "size": "sm",
                                                "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                            },
                                            {
                                                "type": "icon",
                                                "size": "sm",
                                                "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                            },
                                            {
                                                "type": "icon",
                                                "size": "sm",
                                                "url": "https://developers-resource.landpress.line.me/fx/img/review_gold_star_28.png"
                                            },
                                            {
                                                "type": "text",
                                                "text": "5",
                                                "size": "sm",
                                                "color": "#999999",
                                                "margin": "md",
                                                "flex": 0
                                            }
                                        ],
                                        "height": "30px"
                                    },
                                    {
                                        "type": "box",
                                        "layout": "baseline",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "color": "#ebebeb",
                                                "size": "sm",
                                                "flex": 0,
                                                "text": infoformation,
                                                "wrap": true,
                                                "position": "absolute",
                                                "offsetBottom": "0px"
                                            }
                                        ],
                                        "spacing": "lg",
                                        "height": "290px"
                                    },
                                    {
                                        "type": "box",
                                        "layout": "vertical",
                                        "contents": [
                                            {
                                                "type": "filler"
                                            },
                                            {
                                                "type": "box",
                                                "layout": "baseline",
                                                "contents": [
                                                    {
                                                        "type": "filler"
                                                    },
                                                    {
                                                        "type": "text",
                                                        "text": "立即預約",
                                                        "color": "#ffffff",
                                                        "flex": 0,
                                                        "offsetTop": "-2px",
                                                        "action": {
                                                            "type": "uri",
                                                            "label": "action",
                                                            "uri": `https://liff.line.me/${liffId}?servicerId${servicerId}`
                                                        }
                                                    },
                                                    {
                                                        "type": "filler"
                                                    }
                                                ],
                                                "spacing": "sm"
                                            },
                                            {
                                                "type": "filler"
                                            }
                                        ],
                                        "borderWidth": "1px",
                                        "cornerRadius": "4px",
                                        "spacing": "sm",
                                        "borderColor": "#ffffff",
                                        "margin": "xxl",
                                        "height": "40px"
                                    }
                                ],
                                "position": "absolute",
                                "offsetBottom": "0px",
                                "offsetStart": "0px",
                                "offsetEnd": "0px",
                                "paddingTop": "18px",
                                "background": {
                                    "type": "linearGradient",
                                    "angle": "0deg",
                                    "startColor": "#000000D7",
                                    "endColor": "#00000000"
                                },
                                "paddingAll": "20px",
                            }
                        ],
                        "paddingAll": "0px"
                    }
                }
            )

            /* servicerContent.push({
             "type": "bubble",
             "size": "micro",
             "hero": {
             "type": "image",
             "url": getImageUrl2 ? getImageUrl2 : avatarShop,
             "size": "full",
             "aspectMode": "cover",
             "aspectRatio": "1:1"
             },
             "body": {
             "type": "box",
             "layout": "vertical",
             "contents": [
             {
             "type": "text",
             "text": servicerName,
             "weight": "bold",
             "size": "sm",
             "wrap": true
             },
             {
             "type": "box",
             "layout": "vertical",
             "contents": [
             {
             "type": "box",
             "layout": "baseline",
             "spacing": "sm",
             "contents": [
             {
             "type": "text",
             "text": infoformation,
             "wrap": true,
             "color": "#8c8c8c",
             "size": "xs",
             "flex": 5
             }
             ],
             "height": "65px",
             "margin": "5px"
             }
             ]
             },
             {
             "type": "box",
             "layout": "vertical",
             "contents": [
             {
             "type": "button",
             "action": {
             "type": "uri",
             "label": "預約",
             "uri": `https://liff.line.me/${liffId}?servicerId${servicerId}`
             },
             "offsetTop": "-6px",
             "color": "#ffffff"
             }
             ],
             "backgroundColor": "#999999",
             "cornerRadius": "lg",
             "height": "40px"
             }
             ],
             "spacing": "sm",
             "paddingAll": "13px"
             }
             });*/
        }

        let message = {
            "type": "flex",
            "altText": "預約",
            "contents": {
                "type": "carousel",
                "contents": servicerContent
            }
        };

        client.replyMessage(req.body.events[0].replyToken, message)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send('Error');
        });

    }

    if (req.body.events[0].message.text.includes('預約分店')) {
        const shopId = req.body.events[0].message.text.replace(/^[^\d\w]+/, ''); // 使用正則表達式去除開頭非數字和字母的部分

        const citiesRef = db.collection('lineData');
        const snapshot = await citiesRef.where('shopId', '==', shopId).get();
        let liffId = snapshot.empty ? '' : snapshot.docs[0].data().liffId;

        const getFirebaseStoragePublicUrl = async (shopId, avatarImg) => {
            try {
                return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservicer%2F${shopId}%2F${avatarImg}?alt=media`;
            } catch (error) {
                console.error('Error getting public URL:', error);
                return null; // 或者返回一個默認的圖片 URL
            }
        };

        const servicer = db.collection('servicer');
        const servicerDateSnapshot = await servicer.where('shopId', '==', shopId).get();
        const avatarShop = 'https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/notimg.jpg?alt=media';
        let serviceContent = [];

        // 將快照轉換為陣列，並按排序進行排序
        const servicerDateArray = servicerDateSnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
        })).sort((a, b) => a.sort - b.sort);

        for (const doc of serviceDateArray) {
            let serviceName = doc.serviceName ? doc.serviceName : '未設定名稱';
            let serviceId = doc.serviceId ? doc.serviceId : '---';
            let infoformation = doc.infoformation ? doc.infoformation : '---';
            let avatarImg = doc.img[0] ? doc.img[0] : false;
            let getImageUrl2 = avatarImg ? await getFirebaseStoragePublicUrl(shopId, avatarImg) : null;

            serviceContent.push({
                "type": "bubble",
                "size": "micro",
                "hero": {
                    "type": "image",
                    "url": getImageUrl2 ? getImageUrl2 : avatarShop,
                    "size": "full",
                    "aspectMode": "cover",
                    "aspectRatio": "1:1"
                },
                "body": {
                    "type": "box",
                    "layout": "vertical",
                    "contents": [
                        {
                            "type": "text",
                            "text": serviceName,
                            "weight": "bold",
                            "size": "sm",
                            "wrap": true
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "box",
                                    "layout": "baseline",
                                    "spacing": "sm",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": infoformation,
                                            "wrap": true,
                                            "color": "#8c8c8c",
                                            "size": "xs",
                                            "flex": 5
                                        }
                                    ],
                                    "height": "30px",
                                    "margin": "5px"
                                }
                            ]
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "button",
                                    "action": {
                                        "type": "message",
                                        "label": "選擇",
                                        "text": `預約服務${serviceId}`
                                    },
                                    "offsetTop": "-6px",
                                    "color": "#ffffff"
                                }
                            ],
                            "backgroundColor": "#999999",
                            "cornerRadius": "lg",
                            "height": "40px"
                        }
                    ],
                    "spacing": "sm",
                    "paddingAll": "13px"
                }
            });
        }

        let message = {
            "type": "flex",
            "altText": "預約",
            "contents": {
                "type": "carousel",
                "contents": serviceContent
            }
        };

        client.replyMessage(req.body.events[0].replyToken, message)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send('Error');
        });
    }

    if (req.body.events[0].message.text.includes('預約服務')) {

        const serviceId = req.body.events[0].message.text.replace(/^[^\d\w]+/, ''); // 使用正則表達式去除開頭非數字和字母的部分

        const serviceDB = db.collection('service');
        const serviceData = await serviceDB.where('serviceId', '==', serviceId).where('switch', '==', true).get();
        const shopId = serviceData.docs[0].data().shopId;
        const servicerIdarr = serviceData.docs[0].data().servicerIdarr;

        const citiesRef = db.collection('lineData');
        const snapshot = await citiesRef.where('shopId', '==', shopId).get();
        let liffId = snapshot.empty ? '' : snapshot.docs[0].data().liffId;

        // 取得該服務項目的服務人員
        const getFirebaseStoragePublicUrl = async (shopId, avatarImg ,taffId) => {
            console.log(taffId,1)
            console.log(avatarImg,2)
            if(avatarImg){
                console.log(avatarImg,3)
                return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservicer%2F${shopId}%2F${avatarImg}?alt=media`;
            }else{
                console.log(taffId,2)
                const taffDB = db.collection('taffData');
                const taffData = await taffDB.where('taffId', '==', taffId).get();
                const taffAvatar = taffData.docs[0].data().avatar
                console.log(taffAvatar,3)
                if(taffAvatar){
                    return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/taff%2Favatar%2F${taffId}%2F${taffAvatar}?alt=media`;
                }else {
                    return null;
                }
            }
        };

        const servicerDB = db.collection('servicer');
        const servicerDateSnapshot = await servicerDB.where('servicerId', 'in', servicerIdarr).where('switch', '==', true).get();

        const avatarShop = 'https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/notimg.jpg?alt=media';
        let servicerContent = [];

        // 將快照轉換為陣列，並按排序進行排序
        const servicerDateArray = servicerDateSnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
        })).sort((a, b) => a.sort - b.sort);

        for (const doc of servicerDateArray) {
            let servicerName = doc.servicerName ? doc.servicerName : '未設定名稱';
            let servicerId = doc.servicerId ? doc.servicerId : '---';
            let taffId = doc.taffId ? doc.taffId : '';

            let infoformation = doc.infoformation ? doc.infoformation : '---';
            let avatarImg = doc.img[0] ? doc.img[0] : false;
            let getImageUrl2 = await getFirebaseStoragePublicUrl(shopId, avatarImg,taffId) ;

            servicerContent.push({
                "type": "bubble",
                "size": "micro",
                "hero": {
                    "type": "image",
                    "url": getImageUrl2 ? getImageUrl2 : avatarShop,
                    "size": "full",
                    "aspectMode": "cover",
                    "aspectRatio": "20:13"
                },
                "body": {
                    "type": "box",
                    "layout": "vertical",
                    "contents": [
                        {
                            "type": "text",
                            "text": servicerName,
                            "weight": "bold",
                            "size": "sm",
                            "wrap": true
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "box",
                                    "layout": "baseline",
                                    "spacing": "sm",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": infoformation,
                                            "wrap": true,
                                            "color": "#8c8c8c",
                                            "size": "xs",
                                            "flex": 5
                                        }
                                    ],
                                    "height": "50px",
                                    "margin": "5px"
                                }
                            ]
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": [
                                {
                                    "type": "button",
                                    "action": {
                                        "type": "uri",
                                        "label": "預約",
                                        "uri": `https://liff.line.me/${liffId}?servicerId${servicerId}-serviceId${serviceId}`
                                    },
                                    "offsetTop": "-6px",
                                    "color": "#ffffff"
                                }
                            ],
                            "backgroundColor": "#999999",
                            "cornerRadius": "lg",
                            "height": "40px"
                        }
                    ],
                    "spacing": "sm",
                    "paddingAll": "13px"
                }
            });
        }

        let message = {
            "type": "flex",
            "altText": "預約",
            "contents": {
                "type": "carousel",
                "contents": servicerContent
            }
        };

        client.replyMessage(req.body.events[0].replyToken, message)
        .then(() => {
            res.status(200).send('OK');
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send('Error');
        });

    }



    if (req.body.events[0].message.text == '選擇地區') {
        console.log(pushContent.length,'分店預約')

        if (pushContent.length > 0) {
            var button = await [];

            const uniqueLargeAreas = new Set();
            const arrArea = [];

            await Promise.all(pushContent.map(async (val) => {
                // 商店資訊
                const citiesRef2 = db.collection('shopData');
                const snapshot2 = await citiesRef2.where('shopId', '==', val.shopId).get();
                snapshot2.forEach((doc) => {
                    val.shopName = doc.data().shopName;
                    val.largeArea = doc.data().largeArea;
                    const largeArea = doc.data().largeArea;
                    const sortValue = doc.data().sort; // 假設這裡的排序值是 doc.data().sort

                    if (!uniqueLargeAreas.has(largeArea)) {
                        uniqueLargeAreas.add(largeArea);
                        arrArea.push({ text: largeArea, sort: sortValue });
                    }
                });
            }));

            arrArea.sort((a, b) => a.sort - b.sort);
            
            console.log(arrArea,'分店預約')
            if(arrArea.length>0){
                arrArea.filter(async (val) => {
                    await button.push({
                        "type"  : "button",
                        "style" : "primary",
                        "height": "sm",
                        "action": {
                            "type" : "message",
                            "label": val.text,
                            "text"  : val.text
                        },
                        "color" : "#555555"
                    })
                })
                let message = await [
                    {
                        "type"    : "flex",
                        "altText" : '預約',
                        "contents": {
                            "type"  : "bubble",
                            "body"  : {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [
                                    {
                                        "type"  : "text",
                                        "text"  : '選擇地區',
                                        "weight": "bold",
                                        "size"  : "xl"
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "margin"  : "lg",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type"    : "box",
                                                "layout"  : "baseline",
                                                "spacing" : "sm",
                                                "contents": [
                                                    {
                                                        "type" : "text",
                                                        "text" : '下列為有服務的地區',
                                                        "wrap" : true,
                                                        "color": "#666666",
                                                        "size" : "sm",
                                                    }
                                                ]
                                            },
                                        ]
                                    }
                                ]
                            },
                            "footer": {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "spacing" : "sm",
                                "contents": button,
                                "flex"    : 0
                            }
                        }
                    }
                ]
                client.replyMessage(req.body.events[0].replyToken, message)
                .then(() => {
                    res.status(200).send('OK');
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).send('Error');
                });
            }

        }
    }


    if (await req.body.events[0].message.text == '預約') {
        if (pushContent.length > 0) {
            var button = await [];
            await pushContent.filter(async (val) => {

                // 商店資訊
                const citiesRef2 = await db.collection('shopData');
                const snapshot2  = await citiesRef2.where('shopId', '==', val.shopId).get();

                //傳統按鈕
                if(val.menuBtn == 2){
                    await snapshot2.forEach(async (doc) => {
                        val.shopName = await doc.data().shopName;
                        await button.push({
                            "type"  : "button",
                            "style" : "primary",
                            "height": "sm",
                            "action": {
                                "type" : "uri",
                                "label": doc.data().shopName,
                                "uri"  : `https://liff.line.me/${val.liffId}`
                            },
                            "color" : "#555555"
                        })
                    })
                    if (button.length == pushContent.length) {
                        let message = await [
                            {
                                "type"    : "flex",
                                "altText" : '預約',
                                "contents": {
                                    "type"  : "bubble",
                                    "body"  : {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "contents": [
                                            {
                                                "type"  : "text",
                                                "text"  : '預約說明',
                                                "weight": "bold",
                                                "size"  : "xl"
                                            },
                                            {
                                                "type"    : "box",
                                                "layout"  : "vertical",
                                                "margin"  : "lg",
                                                "spacing" : "sm",
                                                "contents": [
                                                    {
                                                        "type"    : "box",
                                                        "layout"  : "baseline",
                                                        "spacing" : "sm",
                                                        "contents": [
                                                            {
                                                                "type" : "text",
                                                                "text" : '點擊下方選擇店舖，進入預約系統。',
                                                                "wrap" : true,
                                                                "color": "#666666",
                                                                "size" : "sm",
                                                            }
                                                        ]
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                    "footer": {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "spacing" : "sm",
                                        "contents": button,
                                        "flex"    : 0
                                    }
                                }
                            }
                        ]

                        await client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    }
                }

                //服務人員按鈕
                if(val.menuBtn == 3){

                    // 取得該服務項目的服務人員
                    const getFirebaseStoragePublicUrl = async (shopId, avatarImg ,taffId) => {
                        console.log(taffId,1)
                        console.log(avatarImg,2)
                        if(avatarImg){
                            console.log(avatarImg,3)
                            return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservicer%2F${shopId}%2F${avatarImg}?alt=media`;
                        }else{
                            console.log(taffId,22)
                            const taffDB = db.collection('taffData');
                            const taffData = await taffDB.where('taffId', '==', taffId).get();
                            const taffAvatar = taffData.docs[0].data().avatar
                            console.log(taffAvatar,33)
                            if(taffAvatar){
                                return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/taff%2Favatar%2F${taffId}%2F${taffAvatar}?alt=media`;
                            }else {
                                return null;
                            }
                        }
                    };

                    const servicer = await db.collection('servicer');
                    const servicerDate  = await servicer.where('shopId', '==', val.shopId).where('switch', '==', true).get();
                    const avatarShop =  'https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/notimg.jpg?alt=media';
                    let servicerContent = []

                    await servicerDate.forEach(async (doc) => {

                        let servicerName = (doc.data().servicerName)?doc.data().servicerName:'未設定名稱'
                        let servicerId = (doc.data().servicerId)?doc.data().servicerId:'---'
                        let taffId = doc.data().taffId ? doc.data().taffId : '';
                        let infoformation = (doc.data().infoformation)?doc.data().infoformation:'---'
                        let avatarImg = (doc.data().img.length>0)?doc.data().img[0]:false
                        if(avatarImg){
                            var getImageUrl2  = await getFirebaseStoragePublicUrl(val.shopId, avatarImg,taffId)   // 取得圖片
                        }

                        servicerContent.push(
                            {
                                "type": "bubble",
                                "size": "micro",
                                "hero": {
                                    "type": "image",
                                    "url": (getImageUrl2)?getImageUrl2:avatarShop,
                                    "size": "full",
                                    "aspectMode": "cover",
                                    "aspectRatio": "1:1"
                                },
                                "body": {
                                    "type": "box",
                                    "layout": "vertical",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": servicerName,
                                            "weight": "bold",
                                            "size": "sm",
                                            "wrap": true
                                        },
                                        {
                                            "type": "box",
                                            "layout": "vertical",
                                            "contents": [
                                                {
                                                    "type": "box",
                                                    "layout": "baseline",
                                                    "spacing": "sm",
                                                    "contents": [
                                                        {
                                                            "type": "text",
                                                            "text": infoformation,
                                                            "wrap": true,
                                                            "color": "#8c8c8c",
                                                            "size": "xs",
                                                            "flex": 5
                                                        }
                                                    ],
                                                    "height": "30px",
                                                    "margin": "5px"
                                                }
                                            ]
                                        },
                                        {
                                            "type": "box",
                                            "layout": "vertical",
                                            "contents": [
                                                {
                                                    "type": "button",
                                                    "action": {
                                                        "type": "uri",
                                                        "label": "預約",
                                                        "uri": `https://liff.line.me/${val.liffId}`
                                                    },
                                                    "offsetTop": "-6px",
                                                    "color": "#ffffff"
                                                }
                                            ],
                                            "backgroundColor": "#999999",
                                            "cornerRadius": "lg",
                                            "height": "40px"
                                        }
                                    ],
                                    "spacing": "sm",
                                    "paddingAll": "13px"
                                }
                            }
                        )

                    })


                    let message = {
                        "type": "flex",
                        "altText": "預約",
                        "contents": {
                            "type": "carousel",
                            "contents": servicerContent
                        }
                    };

                    client.replyMessage(req.body.events[0].replyToken, message)
                    .then(() => {
                        res.status(200).send('OK');
                    })
                    .catch((err) => {
                        console.log(err);
                        res.status(500).send('Error');
                    });

                }

                //服務項目按鈕
                if(val.menuBtn == 5){

                    const getFirebaseStoragePublicUrl = async (shopId, avatarImg) => {
                        try {

                            return `https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/shop%2Fservice%2F${shopId}%2F${avatarImg}?alt=media`;
                        } catch (error) {
                            console.error('Error getting public URL:', error);
                            return null; // 或者返回一個默認的圖片 URL
                        }
                    };

                    const service = await db.collection('service');
                    const serviceDate  = await service.where('shopId', '==', val.shopId).where('switch', '==', true).get();
                    const avatarShop =  'https://firebasestorage.googleapis.com/v0/b/freer-ba4ea.appspot.com/o/notimg.jpg?alt=media';
                    let serviceContent = []

                    await serviceDate.forEach(async (doc) => {

                        let serviceName = (doc.data().serviceName)?doc.data().serviceName:'未設定名稱'
                        let serviceId = (doc.data().serviceId)?doc.data().serviceId:'---'
                        let infoformation = (doc.data().infoformation)?doc.data().infoformation:'---'
                        let avatarImg = (doc.data().img[0])?doc.data().img[0]:false
                        if(avatarImg){
                            var getImageUrl2  = await getFirebaseStoragePublicUrl(val.shopId, avatarImg)   // 取得圖片
                        }

                        serviceContent.push(
                            {
                                "type": "bubble",
                                "size": "micro",
                                "hero": {
                                    "type": "image",
                                    "url": (getImageUrl2)?getImageUrl2:avatarShop,
                                    "size": "full",
                                    "aspectMode": "cover",
                                    "aspectRatio": "1:1"
                                },
                                "body": {
                                    "type": "box",
                                    "layout": "vertical",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": serviceName,
                                            "weight": "bold",
                                            "size": "sm",
                                            "wrap": true
                                        },
                                        {
                                            "type": "box",
                                            "layout": "vertical",
                                            "contents": [
                                                {
                                                    "type": "box",
                                                    "layout": "baseline",
                                                    "spacing": "sm",
                                                    "contents": [
                                                        {
                                                            "type": "text",
                                                            "text": infoformation,
                                                            "wrap": true,
                                                            "color": "#8c8c8c",
                                                            "size": "xs",
                                                            "flex": 5
                                                        }
                                                    ],
                                                    "height": "30px",
                                                    "margin": "5px"
                                                }
                                            ]
                                        },
                                        {
                                            "type": "box",
                                            "layout": "vertical",
                                            "contents": [
                                                {
                                                    "type": "button",
                                                    "action": {
                                                        "type": "uri",
                                                        "label": "預約",
                                                        "uri": `https://liff.line.me/${val.liffId}`
                                                    },
                                                    "offsetTop": "-6px",
                                                    "color": "#ffffff"
                                                }
                                            ],
                                            "backgroundColor": "#999999",
                                            "cornerRadius": "lg",
                                            "height": "40px"
                                        }
                                    ],
                                    "spacing": "sm",
                                    "paddingAll": "13px"
                                }
                            }
                        )

                    })


                    let message = {
                        "type": "flex",
                        "altText": "預約",
                        "contents": {
                            "type": "carousel",
                            "contents": serviceContent
                        }
                    };

                    client.replyMessage(req.body.events[0].replyToken, message)
                    .then(() => {
                        res.status(200).send('OK');
                    })
                    .catch((err) => {
                        console.log(err);
                        res.status(500).send('Error');
                    });

                }
            })
        }


    }

    if (req.body.events[0].message.text == '登入') {
        {
            const message = [
                {
                    "type"    : "flex",
                    "altText" : '後台登入',
                    "contents": {
                        "type"  : "bubble",
                        "body"  : {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "contents": [
                                {
                                    "type"  : "text",
                                    "text"  : '後台登入',
                                    "weight": "bold",
                                    "size"  : "xl"
                                },
                                {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "margin"  : "lg",
                                    "spacing" : "sm",
                                    "contents": [
                                        {
                                            "type"    : "box",
                                            "layout"  : "baseline",
                                            "spacing" : "sm",
                                            "contents": [
                                                {
                                                    "type" : "text",
                                                    "text" : '點擊下方按鈕進入後台。',
                                                    "wrap" : true,
                                                    "color": "#666666",
                                                    "size" : "sm",
                                                }
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        "footer": {
                            "type"    : "box",
                            "layout"  : "vertical",
                            "spacing" : "sm",
                            "contents": [
                                {
                                    "type"  : "button",
                                    "style" : "primary",
                                    "height": "sm",
                                    "action": {
                                        "type" : "uri",
                                        "label": "登入",
                                        "uri"  : `https://newdate.app/taff/home`
                                    },
                                    "color" : "#555555"
                                },
                                {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "contents": [],
                                    "margin"  : "sm"
                                }
                            ],
                            "flex"    : 0
                        }
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
    }


    if (req.body.events[0].message.text == '登出') {
        const message = [
            {
                "type"    : "flex",
                "altText" : '後台登入',
                "contents": {
                    "type"  : "bubble",
                    "body"  : {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : '後台登入',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "margin"  : "lg",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"    : "box",
                                        "layout"  : "baseline",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type" : "text",
                                                "text" : '點擊按鈕至登出畫面',
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "sm",
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    "footer": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "spacing" : "sm",
                        "contents": [
                            {
                                "type"  : "button",
                                "style" : "primary",
                                "height": "sm",
                                "action": {
                                    "type" : "uri",
                                    "label": "登出",
                                    "uri"  : `https://newdate.app/login`
                                },
                                "color" : "#555555"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [],
                                "margin"  : "sm"
                            }
                        ],
                        "flex"    : 0
                    }
                }
            }
        ]
        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }


    const orderSend  = await req.body.events[0].message.text.includes('派出預約訂單');
    const orderConfirm = await req.body.events[0].message.text.includes('接受預約派單');
    const orderCancel  = await req.body.events[0].message.text.includes('取消預約訂單');
    const orderFinish  = await req.body.events[0].message.text.includes('完成預約訂單');

    if (orderConfirm || orderCancel || orderFinish || orderSend) {


        const member = await req.body.events[0].message.text.includes('會員');
        const admin  = await req.body.events[0].message.text.includes('管理員');
        const taff   = await req.body.events[0].message.text.includes('夥伴');

        let orderId = req.body.events[0].message.text.split(':')[1]


        let contentOrder = [];
        const citiesRef  = db.collection('order');
        const snapshot   = await citiesRef.where('orderId', '==', orderId).get();
        snapshot.forEach(doc => {
            contentOrder = doc.data();
        });
        let changeStatus = null;

        // 開啟服務人員自動接單
        if(admin && contentOrder.status == 0 && contentOrder.orderDetail.auto){




            // 自動接單確認
            if(orderConfirm){
                changeStatus        = 1;
                contentOrder.status = changeStatus;
                const ref           = db.collection('order');
                ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                    let status = MessageType(contentOrder.status);
                    if (contentOrder.spaceAmount) {
                        contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                    }
                    // 推會員
                    const configMember = {
                        method : 'post',
                        url    : 'https://node.newdate.app/lineMsgPush',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        data   : {
                            message: PushMessage({
                                shopName    : contentOrder.orderDetail.shopName,
                                orderStatus : status,
                                status      : contentOrder.orderDetail.status,
                                serviceName : contentOrder.orderDetail.serviceName,
                                servicerName: contentOrder.orderDetail.servicerName,
                                date        : contentOrder.date,
                                time        : contentOrder.time,
                                showTime    : '大約' + contentOrder.orderDetail.showTime,
                                amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                token       : contentOrder.lineData.token,
                                altText     : "接受預約通知",
                                url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                message     : contentOrder.lineData.message,
                                pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                orderId     : contentOrder.orderId,
                                role        : ['membercheck']
                            }),
                            userId : contentOrder.lineData.linkLineMemberId,
                            token  : contentOrder.lineData.token,
                        }
                    };
                    axios(configMember).then(function (response) {
                        // console.log(JSON.stringify(response.data));
                    }).catch(function (error) {
                        // console.log(error);
                    });
                });


                let message = [{
                    "type": "text",
                    "text": `您已接受預約訂單，已通知客人。`,
                }]
                client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                    res.status(200).send('OK');
                }).catch((err) => {
                    console.log(err);
                });
            }

            //自動接單取消
            if(orderCancel){
                changeStatus        = 3;
                contentOrder.status = changeStatus;
                const ref           = db.collection('order');
                ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                    let status = MessageType(contentOrder.status);
                    if (contentOrder.spaceAmount) {
                        contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                    }
                    // 推會員
                    const config = {
                        method : 'post',
                        url    : 'https://node.newdate.app/lineMsgPush',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        data   : {
                            message: PushMessage({
                                shopName    : contentOrder.orderDetail.shopName,
                                orderStatus : status,
                                status      : contentOrder.orderDetail.status,
                                serviceName : contentOrder.orderDetail.serviceName,
                                servicerName: contentOrder.orderDetail.servicerName,
                                date        : contentOrder.date,
                                time        : contentOrder.time,
                                showTime    : '大約' + contentOrder.orderDetail.showTime,
                                amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                token       : contentOrder.lineData.token,
                                altText     : "取消預約通知",
                                url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                message     : contentOrder.lineData.message,
                                pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                orderId     : contentOrder.orderId,
                                role        : []
                            }),
                            userId : contentOrder.lineData.linkLineMemberId,
                            token  : contentOrder.lineData.token,
                        }
                    };
                    axios(config).then(function (response) {
                        // console.log(JSON.stringify(response.data));
                    }).catch(function (error) {
                        // console.log(error);
                    });

                    let message = [{
                        "type": "text",
                        "text": `您已取消預約訂單，已通知客人。`,
                    }]
                    client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                        res.status(200).send('OK');
                    }).catch((err) => {
                        console.log(err);
                    });
                });
            }



        }else{
            //待回覆 0
            if (contentOrder.status == 0) {
                //管理員確認
                if (admin && orderSend) {
                    // 4 確認預約中 -> 給技術者端
                    changeStatus        = 4;
                    contentOrder.status = changeStatus;
                    const ref           = db.collection('order');
                    ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                        let status = MessageType(contentOrder.status);
                        if (contentOrder.spaceAmount) {
                            contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                        }
                        // 推技術員
                        const config = {
                            method : 'post',
                            url    : 'https://node.newdate.app/lineMsgPush',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            data   : {
                                message: PushMessage({
                                    shopName    : contentOrder.orderDetail.shopName,
                                    orderStatus : status,
                                    status      : contentOrder.orderDetail.status,
                                    serviceName : contentOrder.orderDetail.serviceName,
                                    servicerName: contentOrder.orderDetail.servicerName,
                                    date        : contentOrder.date,
                                    time        : contentOrder.time,
                                    showTime    : '大約' + contentOrder.orderDetail.showTime,
                                    amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount : contentOrder.amount,
                                    token       : contentOrder.lineData.token,
                                    altText     : "派單預約通知",
                                    url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                    message     : contentOrder.lineData.message,
                                    orderId     : contentOrder.orderId,
                                    pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                    role        : ['cancel', 'confirm'],
                                    who         : '夥伴',
                                }),
                                userId : contentOrder.lineData.linkLineTaffId,
                                token  : contentOrder.lineData.token,
                            }
                        };
                        axios(config).then(function (response) {
                            // console.log(JSON.stringify(response.data));
                        }).catch(function (error) {
                            // console.log(error);
                        });

                        let message = [{
                            "type": "text",
                            "text": `您已派出預約訂單，等待團隊夥伴接受。`,
                        }]
                        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    });

                }
                //管理員取消
                if (admin && orderCancel) {
                    // 3 管理員取消 -> 給會員端
                    changeStatus        = 3;
                    contentOrder.status = changeStatus;
                    const ref           = db.collection('order');
                    ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                        let status = MessageType(contentOrder.status);
                        if (contentOrder.spaceAmount) {
                            contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                        }
                        // 推會員
                        const config = {
                            method : 'post',
                            url    : 'https://node.newdate.app/lineMsgPush',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            data   : {
                                message: PushMessage({
                                    shopName    : contentOrder.orderDetail.shopName,
                                    orderStatus : status,
                                    status      : contentOrder.orderDetail.status,
                                    serviceName : contentOrder.orderDetail.serviceName,
                                    servicerName: contentOrder.orderDetail.servicerName,
                                    date        : contentOrder.date,
                                    time        : contentOrder.time,
                                    showTime    : '大約' + contentOrder.orderDetail.showTime,
                                    amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount : contentOrder.amount,
                                    token       : contentOrder.lineData.token,
                                    altText     : "取消預約通知",
                                    url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                    message     : contentOrder.lineData.message,
                                    pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                    orderId     : contentOrder.orderId,
                                    role        : []
                                }),
                                userId : contentOrder.lineData.linkLineMemberId,
                                token  : contentOrder.lineData.token,
                            }
                        };
                        axios(config).then(function (response) {
                            // console.log(JSON.stringify(response.data));
                        }).catch(function (error) {
                            // console.log(error);
                        });

                        let message = [{
                            "type": "text",
                            "text": `您已取消預約訂單，已通知客人。`,
                        }]
                        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    });
                }
                //會員取消
                if (member && orderCancel) {
                    // 3 會員取消 -> 給管理端
                    changeStatus        = 3;
                    contentOrder.status = changeStatus;
                    const ref           = db.collection('order');
                    ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                        let status = MessageType(contentOrder.status);
                        // 推管理端
                        if (contentOrder.spaceAmount) {
                            contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                        }
                        if (contentOrder.lineData.linkLineAdminId.length > 0) {
                            contentOrder.lineData.linkLineAdminId.filter(val => {

                                const config = {
                                    method : 'post',
                                    url    : 'https://node.newdate.app/lineMsgPush',
                                    headers: {
                                        'Content-Type': 'application/json',
                                    },
                                    data   : {
                                        message: PushMessage({
                                            shopName    : contentOrder.orderDetail.shopName,
                                            orderStatus : status,
                                            status      : contentOrder.orderDetail.status,
                                            serviceName : contentOrder.orderDetail.serviceName,
                                            servicerName: contentOrder.orderDetail.servicerName,
                                            date        : contentOrder.date,
                                            time        : contentOrder.time,
                                            showTime    : '大約' + contentOrder.orderDetail.showTime,
                                            amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                            token       : contentOrder.lineData.token,
                                            altText     : "取消預約通知",
                                            url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                            message     : contentOrder.lineData.message,
                                            pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                            orderId     : contentOrder.orderId,
                                            role        : []
                                        }),
                                        userId : val.lineId,
                                        token  : contentOrder.lineData.token,
                                    }
                                };
                                axios(config).then(function (response) {
                                    // console.log(JSON.stringify(response.data));
                                }).catch(function (error) {
                                    // console.log(error);
                                });
                            })
                        }


                        let message = [{
                            "type": "text",
                            "text": `您已取消預約訂單，已通知商家。`,
                        }]
                        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    });
                }
            }
            //確認預約中 4
            if (contentOrder.status == 4) {
                if (taff && orderConfirm) {
                    // 1 技術員確認 -> 給管理端 會員端
                    changeStatus        = 1;
                    contentOrder.status = changeStatus;
                    const ref           = db.collection('order');
                    ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                        let status = MessageType(contentOrder.status);
                        // 推管理端
                        if (contentOrder.spaceAmount) {
                            contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                        }
                        if (contentOrder.lineData.linkLineAdminId.length > 0) {
                            contentOrder.lineData.linkLineAdminId.filter(val => {

                                const config = {
                                    method : 'post',
                                    url    : 'https://node.newdate.app/lineMsgPush',
                                    headers: {
                                        'Content-Type': 'application/json',
                                    },
                                    data   : {
                                        message: PushMessage({
                                            shopName    : contentOrder.orderDetail.shopName,
                                            orderStatus : status,
                                            status      : contentOrder.orderDetail.status,
                                            serviceName : contentOrder.orderDetail.serviceName,
                                            servicerName: contentOrder.orderDetail.servicerName,
                                            date        : contentOrder.date,
                                            time        : contentOrder.time,
                                            showTime    : '大約' + contentOrder.orderDetail.showTime,
                                            amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                            token       : contentOrder.lineData.token,
                                            altText     : "接受預約通知",
                                            url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                            message     : contentOrder.lineData.message,
                                            pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                            orderId     : contentOrder.orderId,
                                            role        : ['membercheck']
                                        }),
                                        userId : val.lineId,
                                        token  : contentOrder.lineData.token,
                                    }
                                };
                                axios(config).then(function (response) {
                                    // console.log(JSON.stringify(response.data));
                                }).catch(function (error) {
                                    // console.log(error);
                                });
                            })
                        }

                        // 推會員
                        const configMember = {
                            method : 'post',
                            url    : 'https://node.newdate.app/lineMsgPush',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            data   : {
                                message: PushMessage({
                                    shopName    : contentOrder.orderDetail.shopName,
                                    orderStatus : status,
                                    status      : contentOrder.orderDetail.status,
                                    serviceName : contentOrder.orderDetail.serviceName,
                                    servicerName: contentOrder.orderDetail.servicerName,
                                    date        : contentOrder.date,
                                    time        : contentOrder.time,
                                    showTime    : '大約' + contentOrder.orderDetail.showTime,
                                    amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount : contentOrder.amount,
                                    token       : contentOrder.lineData.token,
                                    altText     : "接受預約通知",
                                    url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                    message     : contentOrder.lineData.message,
                                    pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                    orderId     : contentOrder.orderId,
                                    role        : ['membercheck']
                                }),
                                userId : contentOrder.lineData.linkLineMemberId,
                                token  : contentOrder.lineData.token,
                            }
                        };
                        axios(configMember).then(function (response) {
                            // console.log(JSON.stringify(response.data));
                        }).catch(function (error) {
                            // console.log(error);
                        });

                        let message = [{
                            "type": "text",
                            "text": `您已接受預約訂單，已通知商家及客人。`,
                        }]
                        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    });
                }
                if (taff && orderCancel) {
                    // 3 技術員取消 -> 給管理端 會員端
                    changeStatus        = 3;
                    contentOrder.status = changeStatus;
                    const ref           = db.collection('order');
                    ref.doc(contentOrder.orderId).set(contentOrder).then(function () {
                        let status = MessageType(contentOrder.status);
                        // 推管理端
                        if (contentOrder.spaceAmount) {
                            contentOrder.amount = contentOrder.amount + contentOrder.spaceAmount;
                        }
                        if (contentOrder.lineData.linkLineAdminId.length > 0) {
                            contentOrder.lineData.linkLineAdminId.filter(val => {
                                const config = {
                                    method : 'post',
                                    url    : 'https://node.newdate.app/lineMsgPush',
                                    headers: {
                                        'Content-Type': 'application/json',
                                    },
                                    data   : {
                                        message: PushMessage({
                                            shopName    : contentOrder.orderDetail.shopName,
                                            orderStatus : status,
                                            status      : contentOrder.orderDetail.status,
                                            serviceName : contentOrder.orderDetail.serviceName,
                                            servicerName: contentOrder.orderDetail.servicerName,
                                            date        : contentOrder.date,
                                            time        : contentOrder.time,
                                            showTime    : '大約' + contentOrder.orderDetail.showTime,
                                            amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                            token       : contentOrder.lineData.token,
                                            altText     : "確認預約通知",
                                            url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                            message     : contentOrder.lineData.message,
                                            pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                            orderId     : contentOrder.orderId,
                                            role        : []
                                        }),
                                        userId : val.lineId,
                                        token  : contentOrder.lineData.token,
                                    }
                                };
                                axios(config).then(function (response) {
                                    // console.log(JSON.stringify(response.data));
                                }).catch(function (error) {
                                    // console.log(error);
                                });
                            })
                        }
                        // 推會員
                        const configMember = {
                            method : 'post',
                            url    : 'https://node.newdate.app/lineMsgPush',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            data   : {
                                message: PushMessage({
                                    shopName    : contentOrder.orderDetail.shopName,
                                    orderStatus : status,
                                    status      : contentOrder.orderDetail.status,
                                    serviceName : contentOrder.orderDetail.serviceName,
                                    servicerName: contentOrder.orderDetail.servicerName,
                                    date        : contentOrder.date,
                                    time        : contentOrder.time,
                                    showTime    : '大約' + contentOrder.orderDetail.showTime,
                                    amount      : (contentOrder.depositAmount) ? contentOrder.depositAmount  : contentOrder.amount,
                                    token       : contentOrder.lineData.token,
                                    altText     : "取消預約通知",
                                    url         : `https://newdate.app/common/order?orderId=${contentOrder.orderId}`,
                                    message     : contentOrder.lineData.message,
                                    pay         : (payType(contentOrder.orderPay))+((contentOrder.onlinePay)?"("+contentOrder.onlinePay.account+")":''),
                                    orderId     : contentOrder.orderId,
                                    role        : []
                                }),
                                userId : contentOrder.lineData.linkLineMemberId,
                                token  : contentOrder.lineData.token,
                            }
                        };
                        axios(configMember).then(function (response) {
                            // console.log(JSON.stringify(response.data));
                        }).catch(function (error) {
                            // console.log(error);
                        });
                        let message = [{
                            "type": "text",
                            "text": `您已取消次預約訂單，已通知商家與客人。`,
                        }]
                        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                            res.status(200).send('OK');
                        }).catch((err) => {
                            console.log(err);
                        });
                    });
                }
                if (member && orderCancel) {
                    // 3 會員無法取消
                    let message = [{
                        "type": "text",
                        "text": `預約無法在此取消,請與商家聯繫!`,
                    }]
                    client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                        res.status(200).send('OK');
                    }).catch((err) => {
                        console.log(err);
                    });
                }
            }
        }


    }


    const PushMessage = (data) => {

        const msg = [
            {
                "type"    : "flex",
                "altText" : data.altText,
                "contents": {
                    "type"  : "bubble",
                    "body"  : {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : data.shopName,
                                "weight": "bold",
                                "size"  : "xl",
                                "align" : "center"
                            },
                            {
                                "type"  : "text",
                                "text"  : data.orderStatus.title,
                                "margin": "10px",
                                "size"  : "18px",
                                "color" : data.orderStatus.color,
                                "weight": "bold",
                                "align" : "center"
                            },
                            {
                                "type"      : "box",
                                "layout"    : "vertical",
                                "margin"    : "20px",
                                "spacing"   : "sm",
                                "contents"  : [
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type"  : "text",
                                                "text"  : "服務項目",
                                                "color" : "#aaaaaa",
                                                "size"  : "16px",
                                                "flex"  : 2,
                                                "weight": "regular"
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.serviceName,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type"  : "text",
                                                "text"  : "服務人員",
                                                "color" : "#aaaaaa",
                                                "size"  : "16px",
                                                "flex"  : 2,
                                                "weight": "regular"
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.servicerName,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "預約日期",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.date,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "預約時間",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.time,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "項目工時",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.showTime,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "付款方式",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.pay,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "金額",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.status + data.amount,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },
                                    {
                                        "type"         : "box",
                                        "layout"       : "baseline",
                                        "spacing"      : "sm",
                                        "contents"     : [
                                            {
                                                "type" : "text",
                                                "text" : "預約提醒",
                                                "color": "#aaaaaa",
                                                "size" : "16px",
                                                "flex" : 2,
                                                "wrap" : true
                                            },
                                            {
                                                "type" : "text",
                                                "text" : data.message[data.orderStatus.value],
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "16px",
                                                "flex" : 5
                                            }
                                        ],
                                        "paddingBottom": "10px"
                                    },

                                ],
                                "flex"      : 0,
                                "paddingTop": "10px"
                            }
                        ]
                    },
                    "footer": {
                        "type"      : "box",
                        "layout"    : "vertical",
                        "spacing"   : "sm",
                        "contents"  : [
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"  : "button",
                                        "style" : "link",
                                        "height": "sm",
                                        "action": {
                                            "type" : "uri",
                                            "label": "查看詳細",
                                            "uri"  : data.url
                                        }
                                    }
                                ],
                                "flex"    : 0,
                                "width"   : "100%"
                            }
                        ],
                        "flex"      : 0,
                        "width"     : "100%",
                        "paddingAll": "5px"
                    }
                }
            }
        ];

        if (data.role.length > 0) {
            let addBtn = {
                "type"      : "box",
                "layout"    : "horizontal",
                "spacing"   : "sm",
                "contents"  : [
                    // {
                    //     "type"    : "button",
                    //     "style"   : "secondary",
                    //     "height"  : "sm",
                    //     "action"  : {
                    //         "type" : "message",
                    //         "label": "取消",
                    //         "text" : `取消訂單:${data.orderId}`
                    //     },
                    //     "position": "relative"
                    // },
                    // {
                    //     "type"    : "button",
                    //     "style"   : "primary",
                    //     "height"  : "sm",
                    //     "action"  : {
                    //         "type" : "message",
                    //         "label": "確認",
                    //         "text" : `確認訂單:${data.orderId}`
                    //     },
                    //     "position": "relative",
                    //     "color"   : "#555555"
                    // }
                ],
                "flex"      : 2,
                "width"     : "100%",
                "paddingAll": "5px"
            }

            if (data.role.includes('membercheck')) {
                let cancelBtn = {
                    "type"    : "button",
                    "style"   : "secondary",
                    "height"  : "sm",
                    "action"  : {
                        "type" : "message",
                        "label": "好的，我已確定",
                        "text" : `您已確定！`
                    },
                    "position": "relative"
                }
                addBtn.contents.push(cancelBtn)
            }
            if (data.role.includes('cancel')) {
                let cancelBtn = {
                    "type"    : "button",
                    "style"   : "secondary",
                    "height"  : "sm",
                    "action"  : {
                        "type" : "message",
                        "label": "取消",
                        "text" : `${data.who}取消預約訂單:${data.orderId}`
                    },
                    "position": "relative"
                }
                addBtn.contents.push(cancelBtn)
            }
            if (data.role.includes('confirm')) {
                let confirmBtn = {
                    "type"    : "button",
                    "style"   : "primary",
                    "height"  : "sm",
                    "action"  : {
                        "type" : "message",
                        "label": "接受派單",
                        "text" : `${data.who}接受預約派單:${data.orderId}`
                    },
                    "position": "relative",
                    "color"   : data.orderStatus.color,
                }
                addBtn.contents.push(confirmBtn)
            }
            if (data.role.includes('send')) {
                let confirmBtn = {
                    "type"    : "button",
                    "style"   : "primary",
                    "height"  : "sm",
                    "action"  : {
                        "type" : "message",
                        "label": "確認並派單",
                        "text" : `${data.who}派出預約訂單:${data.orderId}`
                    },
                    "position": "relative",
                    "color"   : data.orderStatus.color,
                }
                addBtn.contents.push(confirmBtn)
            }

            msg[0].contents.footer.contents.push(addBtn)
        }


        return msg;
    }
    const MessageType = (number) => {
        const arr = [
            {
                title: '預約待回覆',
                color: '#8428F6',
                value: 0
            },
            {
                title: '預約已接受',
                color: '#FA4BA2',
                value: 1
            },
            {
                title: '預約已完成',
                color: '#8DD776',
                value: 2
            },
            {
                title: '預約已取消',
                color: '#F5BB55',
                value: 3
            },
            {
                title: '派單確認中',
                color: '#5590f5',
                value: 4
            }
        ]
        return arr[number];
    }


    if (req.body.destination == 'Uecb7d160b6c9c23ed0aedfee7a4f54f6') {
        // 設定app影片  預約流程  操作訂單 設定服務人員 設定服務項目 綁定服務人員 綁定管理人員 綁定LINE通知
        const teach = await req.body.events[0].message.text.includes('系統教學');
        if (teach) {
            const message = [
                {
                    "type"    : "flex",
                    "altText" : '系統使用教學',
                    "contents": {
                        "type"    : "carousel",
                        "contents": [
                            {
                                "type"  : "bubble",
                                "footer": {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "spacing" : "sm",
                                    "contents": [
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "如何設定到手機APP",
                                                "text" : "教學影片 https://www.youtube.com/shorts/M0zSxVDU4PU"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "預約下單流程含通知",
                                                "text" : "教學影片 https://youtu.be/qHY1mY23O9Q"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "邀請服務人員入團隊",
                                                "text" : "教學影片 https://youtube.com/shorts/b8bn0BEIK4k?feature=share"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "服務人員連動LINE通知",
                                                "text" : "https://youtube.com/shorts/FI4ZlujShdc?feature=share"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "設定服務項目",
                                                "text" : "教學影片 https://www.youtube.com/shorts/Yg6tY5wHVis"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "邀請加入店鋪管理員",
                                                "text" : "教學影片 https://youtu.be/MoZXDlXpbb0"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "管理員連動LINE通知",
                                                "text" : "教學影片 https://youtube.com/shorts/f9z_SaCKLk4?feature=share"
                                            },
                                            "color" : "#555555"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        // 系統特色 免費與付費 line@設定費 如何訂閱付款 開通線上支付
        const plan = await req.body.events[0].message.text.includes('方案費用');
        if (plan) {
            const message = [
                {
                    "type"    : "flex",
                    "altText" : '費用與方案介紹',
                    "contents": {
                        "type"    : "carousel",
                        "contents": [
                            {
                                "type"  : "bubble",
                                "footer": {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "spacing" : "sm",
                                    "contents": [
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "優惠方案說明",
                                                "text" : "一間店1位 師傅/老師 永久免費\n" +
                                                    "\n" +
                                                    "方案1-預約系統自己來就好=免費\n" +
                                                    "\n" +
                                                    "方案2-客服協助串接系統會產生\n" +
                                                    "一次性費用\n" +
                                                    "1.首次協助開店設定費 \n" +
                                                    "2.系統上架含綁定Line@通知\n"+
                                                    "售後服務：\n" +
                                                    "1.有問題皆可問客服諮詢\n" +
                                                    "2.14日內可全額退費\n" +
                                                    "3.30日內可五成退費\n" +
                                                    "\n" +
                                                    "如果有擴張營業規模\n" +
                                                    "每增加1位技術者=299\n" +
                                                    "每增加一間分店=450元一個月\n" +
                                                    "\n" +
                                                    "以上都需要跟客服索取系統開通碼，謝謝您。"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "免費與付費內容",
                                                "text" : "免費方案:\n 1人 1間店鋪。\n" +
                                                    "免費試用需索取開通驗證碼。\n\n" +
                                                    "付費方案:系統內可升級付費\n"+
                                                    "新增1人$299\n"+"新增1分店$450"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "取消付款",
                                                "text" : "在系統內我的店舖點選「付費升級」，點選「取消付款」。"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "信用卡支付需求",
                                                "text" : "信用卡線上付款客製化串接，費用另計。"
                                            },
                                            "color" : "#555555"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        //後台登入 開通流程 驗證碼 設定LINE@ 綁定自動回覆
        const sop = await req.body.events[0].message.text.includes('開通流程');
        if (sop) {
            const message = [
                {
                    "type"    : "flex",
                    "altText" : '開通流程',
                    "contents": {
                        "type"    : "carousel",
                        "contents": [
                            {
                                "type"  : "bubble",
                                "footer": {
                                    "type"    : "box",
                                    "layout"  : "vertical",
                                    "spacing" : "sm",
                                    "contents": [
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "進入後台開店",
                                                "text" : "在LINE輸入「登入」並觀看教學影片 https://youtu.be/NT9ilTN_fkQ"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "驗證碼",
                                                "text" : "開通驗證碼wqtzuAP1"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "設定LINE@",
                                                "text" : "請諮詢客服人員協助"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "綁定自動回覆",
                                                "text" : "開店完成以及初次LINE通知設定完成後，請輸入「綁定自動回覆」確認連動通知!"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "網站上架",
                                                "text" : "連動LINE通知以及開店完成，請告知客服人員上架，需要1~3工作天"
                                            },
                                            "color" : "#555555"
                                        },
                                        {
                                            "type"  : "button",
                                            "style" : "primary",
                                            "action": {
                                                "type" : "message",
                                                "label": "設定成手機桌面APP",
                                                "text" : "教學影片 https://www.youtube.com/shorts/M0zSxVDU4PU"
                                            },
                                            "color" : "#555555"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        //應用領域介紹 官方網站
        const website = await req.body.events[0].message.text.includes('應用介紹');
        if (website) {
            const message = [
                {
                    "type"    : "flex",
                    "altText" : '應用介紹',
                    "contents": {
                        "type"    : "carousel",
                        "contents": [
                            {
                                "type": "bubble",
                                "body": {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "contents"  : [
                                        {
                                            "type"       : "image",
                                            "url"        : "https://firebasestorage.googleapis.com/v0/b/groupbuy-10352.appspot.com/o/website%2F0004.jpg?alt=media&token=c9f54835-6f6d-4606-980d-e49faa82cee9",
                                            "size"       : "full",
                                            "aspectMode" : "cover",
                                            "aspectRatio": "2:3",
                                            "gravity"    : "top"
                                        },
                                        {
                                            "type"           : "box",
                                            "layout"         : "vertical",
                                            "contents"       : [
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "vertical",
                                                    "contents": [
                                                        {
                                                            "type"  : "text",
                                                            "text"  : "健身按摩",
                                                            "size"  : "xl",
                                                            "color" : "#ffffff",
                                                            "weight": "bold"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "baseline",
                                                    "contents": [
                                                        {
                                                            "type" : "text",
                                                            "color": "#ebebeb",
                                                            "size" : "sm",
                                                            "flex" : 0,
                                                            "text" : "健身教練、SPA按摩、推拿整復預約使用"
                                                        }
                                                    ],
                                                    "spacing" : "lg"
                                                },
                                                {
                                                    "type"        : "box",
                                                    "layout"      : "vertical",
                                                    "contents"    : [
                                                        {
                                                            "type": "filler"
                                                        },
                                                        {
                                                            "type"    : "box",
                                                            "layout"  : "baseline",
                                                            "contents": [
                                                                {
                                                                    "type": "filler"
                                                                },
                                                                {
                                                                    "type"     : "text",
                                                                    "text"     : "前往介紹",
                                                                    "color"    : "#ffffff",
                                                                    "flex"     : 0,
                                                                    "offsetTop": "-2px"
                                                                },
                                                                {
                                                                    "type": "filler"
                                                                }
                                                            ],
                                                            "spacing" : "sm"
                                                        },
                                                        {
                                                            "type": "filler"
                                                        }
                                                    ],
                                                    "borderWidth" : "1px",
                                                    "cornerRadius": "4px",
                                                    "spacing"     : "sm",
                                                    "borderColor" : "#ffffff",
                                                    "margin"      : "xxl",
                                                    "height"      : "40px"
                                                }
                                            ],
                                            "position"       : "absolute",
                                            "offsetBottom"   : "0px",
                                            "offsetStart"    : "0px",
                                            "offsetEnd"      : "0px",
                                            "backgroundColor": "#fa4ba2a8",
                                            "paddingAll"     : "20px",
                                            "paddingTop"     : "18px"
                                        }
                                    ],
                                    "paddingAll": "0px"
                                }
                            },
                            {
                                "type": "bubble",
                                "body": {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "contents"  : [
                                        {
                                            "type"       : "image",
                                            "url"        : "https://firebasestorage.googleapis.com/v0/b/groupbuy-10352.appspot.com/o/website%2F0001.jpg?alt=media&token=48ab07be-aa31-4fe6-99ab-a18e20d0eea7",
                                            "size"       : "full",
                                            "aspectMode" : "cover",
                                            "aspectRatio": "2:3",
                                            "gravity"    : "top"
                                        },
                                        {
                                            "type"           : "box",
                                            "layout"         : "vertical",
                                            "contents"       : [
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "vertical",
                                                    "contents": [
                                                        {
                                                            "type"  : "text",
                                                            "text"  : "美業服務",
                                                            "size"  : "xl",
                                                            "color" : "#ffffff",
                                                            "weight": "bold"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "baseline",
                                                    "contents": [
                                                        {
                                                            "type" : "text",
                                                            "color": "#ebebeb",
                                                            "size" : "sm",
                                                            "flex" : 0,
                                                            "text" : "美髮、美甲、美睫、美容預約使用"
                                                        }
                                                    ],
                                                    "spacing" : "lg"
                                                },
                                                {
                                                    "type"        : "box",
                                                    "layout"      : "vertical",
                                                    "contents"    : [
                                                        {
                                                            "type": "filler"
                                                        },
                                                        {
                                                            "type"    : "box",
                                                            "layout"  : "baseline",
                                                            "contents": [
                                                                {
                                                                    "type": "filler"
                                                                },
                                                                {
                                                                    "type"     : "text",
                                                                    "text"     : "前往介紹",
                                                                    "color"    : "#ffffff",
                                                                    "flex"     : 0,
                                                                    "offsetTop": "-2px"
                                                                },
                                                                {
                                                                    "type": "filler"
                                                                }
                                                            ],
                                                            "spacing" : "sm"
                                                        },
                                                        {
                                                            "type": "filler"
                                                        }
                                                    ],
                                                    "borderWidth" : "1px",
                                                    "cornerRadius": "4px",
                                                    "spacing"     : "sm",
                                                    "borderColor" : "#ffffff",
                                                    "margin"      : "xxl",
                                                    "height"      : "40px"
                                                }
                                            ],
                                            "position"       : "absolute",
                                            "offsetBottom"   : "0px",
                                            "offsetStart"    : "0px",
                                            "offsetEnd"      : "0px",
                                            "backgroundColor": "#8428f69e",
                                            "paddingAll"     : "20px",
                                            "paddingTop"     : "18px"
                                        }
                                    ],
                                    "paddingAll": "0px"
                                }
                            },
                            {
                                "type": "bubble",
                                "body": {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "contents"  : [
                                        {
                                            "type"       : "image",
                                            "url"        : "https://firebasestorage.googleapis.com/v0/b/groupbuy-10352.appspot.com/o/website%2F0009.png?alt=media&token=656e3a06-2707-4f23-8297-91af4679d9fe",
                                            "size"       : "full",
                                            "aspectMode" : "cover",
                                            "aspectRatio": "2:3",
                                            "gravity"    : "top"
                                        },
                                        {
                                            "type"           : "box",
                                            "layout"         : "vertical",
                                            "contents"       : [
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "vertical",
                                                    "contents": [
                                                        {
                                                            "type"  : "text",
                                                            "text"  : "長照機構",
                                                            "size"  : "xl",
                                                            "color" : "#ffffff",
                                                            "weight": "bold"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "baseline",
                                                    "contents": [
                                                        {
                                                            "type" : "text",
                                                            "color": "#ebebeb",
                                                            "size" : "sm",
                                                            "flex" : 0,
                                                            "text" : "居家照顧員與長照機構預約使用"
                                                        }
                                                    ],
                                                    "spacing" : "lg"
                                                },
                                                {
                                                    "type"        : "box",
                                                    "layout"      : "vertical",
                                                    "contents"    : [
                                                        {
                                                            "type": "filler"
                                                        },
                                                        {
                                                            "type"    : "box",
                                                            "layout"  : "baseline",
                                                            "contents": [
                                                                {
                                                                    "type": "filler"
                                                                },
                                                                {
                                                                    "type"     : "text",
                                                                    "text"     : "前往介紹",
                                                                    "color"    : "#ffffff",
                                                                    "flex"     : 0,
                                                                    "offsetTop": "-2px"
                                                                },
                                                                {
                                                                    "type": "filler"
                                                                }
                                                            ],
                                                            "spacing" : "sm"
                                                        },
                                                        {
                                                            "type": "filler"
                                                        }
                                                    ],
                                                    "borderWidth" : "1px",
                                                    "cornerRadius": "4px",
                                                    "spacing"     : "sm",
                                                    "borderColor" : "#ffffff",
                                                    "margin"      : "xxl",
                                                    "height"      : "40px"
                                                }
                                            ],
                                            "position"       : "absolute",
                                            "offsetBottom"   : "0px",
                                            "offsetStart"    : "0px",
                                            "offsetEnd"      : "0px",
                                            "backgroundColor": "#fa4ba2a8",
                                            "paddingAll"     : "20px",
                                            "paddingTop"     : "18px"
                                        }
                                    ],
                                    "paddingAll": "0px"
                                }
                            },
                            {
                                "type": "bubble",
                                "body": {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "contents"  : [
                                        {
                                            "type"       : "image",
                                            "url"        : "https://firebasestorage.googleapis.com/v0/b/groupbuy-10352.appspot.com/o/website%2F0007.jpg?alt=media&token=407c6c4d-e202-462c-a9ee-8c772b5c330e",
                                            "size"       : "full",
                                            "aspectMode" : "cover",
                                            "aspectRatio": "2:3",
                                            "gravity"    : "top"
                                        },
                                        {
                                            "type"           : "box",
                                            "layout"         : "vertical",
                                            "contents"       : [
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "vertical",
                                                    "contents": [
                                                        {
                                                            "type"  : "text",
                                                            "text"  : "寵物相關領域",
                                                            "size"  : "xl",
                                                            "color" : "#ffffff",
                                                            "weight": "bold"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "baseline",
                                                    "contents": [
                                                        {
                                                            "type" : "text",
                                                            "color": "#ebebeb",
                                                            "size" : "sm",
                                                            "flex" : 0,
                                                            "text" : "寵物照顧、美容、旅宿預約使用"
                                                        }
                                                    ],
                                                    "spacing" : "lg"
                                                },
                                                {
                                                    "type"        : "box",
                                                    "layout"      : "vertical",
                                                    "contents"    : [
                                                        {
                                                            "type": "filler"
                                                        },
                                                        {
                                                            "type"    : "box",
                                                            "layout"  : "baseline",
                                                            "contents": [
                                                                {
                                                                    "type": "filler"
                                                                },
                                                                {
                                                                    "type"     : "text",
                                                                    "text"     : "前往介紹",
                                                                    "color"    : "#ffffff",
                                                                    "flex"     : 0,
                                                                    "offsetTop": "-2px"
                                                                },
                                                                {
                                                                    "type": "filler"
                                                                }
                                                            ],
                                                            "spacing" : "sm"
                                                        },
                                                        {
                                                            "type": "filler"
                                                        }
                                                    ],
                                                    "borderWidth" : "1px",
                                                    "cornerRadius": "4px",
                                                    "spacing"     : "sm",
                                                    "borderColor" : "#ffffff",
                                                    "margin"      : "xxl",
                                                    "height"      : "40px"
                                                }
                                            ],
                                            "position"       : "absolute",
                                            "offsetBottom"   : "0px",
                                            "offsetStart"    : "0px",
                                            "offsetEnd"      : "0px",
                                            "backgroundColor": "#8428f69e",
                                            "paddingAll"     : "20px",
                                            "paddingTop"     : "18px"
                                        }
                                    ],
                                    "paddingAll": "0px"
                                }
                            },
                            {
                                "type": "bubble",
                                "body": {
                                    "type"      : "box",
                                    "layout"    : "vertical",
                                    "contents"  : [
                                        {
                                            "type"       : "image",
                                            "url"        : "https://firebasestorage.googleapis.com/v0/b/groupbuy-10352.appspot.com/o/website%2F0010.png?alt=media&token=7df2a655-e9a5-434c-8114-0b0a36051328",
                                            "size"       : "full",
                                            "aspectMode" : "cover",
                                            "aspectRatio": "2:3",
                                            "gravity"    : "top"
                                        },
                                        {
                                            "type"           : "box",
                                            "layout"         : "vertical",
                                            "contents"       : [
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "vertical",
                                                    "contents": [
                                                        {
                                                            "type"  : "text",
                                                            "text"  : "電器修繕",
                                                            "size"  : "xl",
                                                            "color" : "#ffffff",
                                                            "weight": "bold"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "type"    : "box",
                                                    "layout"  : "baseline",
                                                    "contents": [
                                                        {
                                                            "type" : "text",
                                                            "color": "#ebebeb",
                                                            "size" : "sm",
                                                            "flex" : 0,
                                                            "text" : "冷氣清理、洗衣機清理、水電等相關預約使用"
                                                        }
                                                    ],
                                                    "spacing" : "lg"
                                                },
                                                {
                                                    "type"        : "box",
                                                    "layout"      : "vertical",
                                                    "contents"    : [
                                                        {
                                                            "type": "filler"
                                                        },
                                                        {
                                                            "type"    : "box",
                                                            "layout"  : "baseline",
                                                            "contents": [
                                                                {
                                                                    "type": "filler"
                                                                },
                                                                {
                                                                    "type"     : "text",
                                                                    "text"     : "前往介紹",
                                                                    "color"    : "#ffffff",
                                                                    "flex"     : 0,
                                                                    "offsetTop": "-2px"
                                                                },
                                                                {
                                                                    "type": "filler"
                                                                }
                                                            ],
                                                            "spacing" : "sm"
                                                        },
                                                        {
                                                            "type": "filler"
                                                        }
                                                    ],
                                                    "borderWidth" : "1px",
                                                    "cornerRadius": "4px",
                                                    "spacing"     : "sm",
                                                    "borderColor" : "#ffffff",
                                                    "margin"      : "xxl",
                                                    "height"      : "40px"
                                                }
                                            ],
                                            "position"       : "absolute",
                                            "offsetBottom"   : "0px",
                                            "offsetStart"    : "0px",
                                            "offsetEnd"      : "0px",
                                            "backgroundColor": "#fa4ba2a8",
                                            "paddingAll"     : "20px",
                                            "paddingTop"     : "18px"
                                        }
                                    ],
                                    "paddingAll": "0px"
                                }
                            }
                        ]
                    }
                }
            ]
            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        const test = await req.body.events[0].message.text.includes('test');
        if (test) {
            const message = [
                {
                    "type": "text",
                    "text": `https://youtu.be/Mdj5qEby-HA`,
                }
                // {
                //     "type"              : "image",
                //     "originalContentUrl": "https://youtu.be/Mdj5qEby-HA",
                //     "previewImageUrl"   : "https://www.pcschool.com.tw/updimg/act/Blog/content/C00059/interior-classb-certified.jpg"
                // }
                // {
                //     "type": "video",
                //     "originalContentUrl": "https://drive.google.com/file/d/1beh_-NUkWMzGvCWLkTFjQyDtBFeZ4uj5/view?usp=sharing",
                //     "previewImageUrl": "https://example.com/preview.jpg",
                // }
            ]

            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        const push = await req.body.events[0].message.text.includes('推廣合作');
        if (push) {
            const message = [
                {
                    "type": "text",
                    "text": `\n加入經銷商請先告知基本資訊\n
                     \n1.介紹你的產業領域\n
                     \n2.有什麼推廣的策略\n 
                     \n對於系統的合作經銷夥伴，我們會嚴格把關品質。
                     \n該系統的操作使用，需熟悉功能，要能自己先會用。
                     \n再來是和我們的理念相符，對於未來的發展有共識。
                     \n如果您覺得以上都有符合，可以跟我們聊聊。
                    `,
                }
            ]

            client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                res.status(200).send('OK');
            }).catch((err) => {
                console.log(err);
            });
        }
        //經銷商登入
        if (req.body.events[0].message.text == '經銷商登入') {
            {
                const message = [
                    {
                        "type"    : "flex",
                        "altText" : '經銷商後台',
                        "contents": {
                            "type"  : "bubble",
                            "body"  : {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [
                                    {
                                        "type"  : "text",
                                        "text"  : '經銷商後台',
                                        "weight": "bold",
                                        "size"  : "xl"
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "margin"  : "lg",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type"    : "box",
                                                "layout"  : "baseline",
                                                "spacing" : "sm",
                                                "contents": [
                                                    {
                                                        "type" : "text",
                                                        "text" : '點擊下方按鈕進入後台。',
                                                        "wrap" : true,
                                                        "color": "#666666",
                                                        "size" : "sm",
                                                    }
                                                ]
                                            },
                                        ]
                                    }
                                ]
                            },
                            "footer": {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"  : "button",
                                        "style" : "primary",
                                        "height": "sm",
                                        "action": {
                                            "type" : "uri",
                                            "label": "登入",
                                            "uri"  : `https://newdate.app/dealer/home`
                                        },
                                        "color" : "#555555"
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "contents": [],
                                        "margin"  : "sm"
                                    }
                                ],
                                "flex"    : 0
                            }
                        }
                    }
                ]
                client.replyMessage(req.body.events[0].replyToken, message).then(() => {
                    res.status(200).send('OK');
                }).catch((err) => {
                    console.log(err);
                });
            }
        }

    }
})


app.post("/sendEmail", async (req, res, next) => {
    console.log(req)
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        auth: {
            user: 'ye0205414225@gmail.com',
            pass: 'ctfafrputwyzezf',
        },
    });
    transporter.sendMail({
        from   : 'service@newdate.app',
        to     : req.body.sendEmail,
        subject: req.body.sendTitle,
        html   : req.body.sendContent,
    }).then(info => {
        console.log({info});
    }).catch(console.error);
})


//金流－支付訂閱
app.post("/payment", async (req, res, next) => {
    try {

        function genDataChain(TradeInfo) {
            let results = [];
            for (let kv of Object.entries(TradeInfo)) {
                results.push(`${kv[0]}=${kv[1]}`);
            }
            return results.join("&");
        }


        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
        let HashIV  = 'PTJeARVkLqvgtSLC';

        // unix 時間戳
        const dateTime  = new Date().getTime();
        const timestamp = Math.floor(dateTime / 1000);

        const today = new Date();

        let TradeInfo = {
            respondType: 'JSON',
            TimeStamp  : timestamp,
            Version    : '1.5',
            MerOrderNo : req.body.MerOrderNo,              //訂單編號
            ProdDesc   : req.body.ProdDesc,                //商品名稱
            PeriodAmt  : req.body.PeriodAmt,              //訂單金額
            PeriodType : req.body.PeriodType,             //週期類別 M Y
            PeriodPoint: req.body.PeriodPoint,            //交易授權時間 每月的話幾號 年的話 MMDD  0315 月日
            PeriodTimes: req.body.PeriodTimes,            //授權週期數
            // Extday              : '0521',                        //信用卡到期日
            PeriodStartType: 2,                            //檢查信用卡10元驗證
            ReturnURL      : req.body.ReturnURL,            //1.當付款人首次執行信用卡授權交易完成後，以 Form Post 方式導回商店頁面。
            PaymentInfo    : 'N',
            OrderInfo      : 'N',
            PayerEmail     : req.body.PayerEmail,              //付款人電子信箱
            NotifyURL      : req.body.NotifyURL,              // 每期授權結果通知
            BackURL        : req.body.BackURL,                // 取消交易時返回商店的網址
        }

        console.log(TradeInfo)


        let cipher        = crypto.createCipheriv("aes256", HashKey, HashIV);
        let encryptedData = cipher.update(genDataChain(TradeInfo), "utf-8", "hex");
        encryptedData += cipher.final("hex");


        // console.log("Encrypted message: " + encryptedData);
        //
        // res.status(200).send({
        //     result: true,
        // });
        // 新增
        let params       = {
            'paymentId' : '',
            'created_at': FieldValue.serverTimestamp(),
            'MerOrderNo': req.body.MerOrderNo,    // 訂單編號
            'ProdDesc'  : req.body.ProdDesc,      // 商品名稱
            'PeriodAmt' : req.body.PeriodAmt,     // 訂單金額
            'PeriodType': req.body.PeriodType,    // 週期類別 M Y
            'userInfo'  : req.body.userInfo,     // 付費者資訊
            'startTime' : req.body.start_time,
            'endTime'   : req.body.end_time,
            'PeriodNo'  : '',                      //委託單號
            'payStatus' : false,                   // 付款狀態
        }
        const addPayment = async (params) => {
            try {
                params.created_at = new Date().format("yyyy-MM-dd hh:mm:ss");
                const docRef      = await db.collection('payment_t').add(params);
                const result      = await docRef.update({
                    paymentId: docRef.id
                });
                return result;
            }
            catch (error) {
                throw error;
            }
        }
        // 建立一筆訂單 到payment
        addPayment(params)
        res.status(200).send(encryptedData);

    }
    catch (err) {

    }
})

//金流－刷卡回傳結果
app.post("/payment/result", async (req, res, next) => {
    try {

        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
            let HashIV  = 'PTJeARVkLqvgtSLC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        let getData = await JSON.parse(create_mpg_aes_decrypt(req.body.Period));

        console.log(getData);

        if (getData.Status == 'SUCCESS') {

            // 查詢付款項目編號
            var setData     = [];
            const citiesRef = db.collection('payment_t');
            const snapshot  = await citiesRef.where('MerOrderNo', '==', getData.Result.MerchantOrderNo).get();
            snapshot.forEach((doc) => {
                console.log(doc.id, '=>', doc.data());
                setData = doc.data();
            });
            setData.payStatus = true;
            setData.PeriodNo  = await getData.Result.PeriodNo;

            const docRef = await db.collection('payment').add(setData);
            const result = await docRef.update({
                paymentId: docRef.id
            });


            res.redirect(`https://newdate.app/taff/payresult?status=success&paymentId=${docRef.id}`)
        } else {
            res.redirect(`https://newdate.app/taff/payresult?status=error}`)
        }


    }
    catch (err) {

    }
})

//金流 - 修改訂閱
app.post("/payment/edit", async (req, res, next) => {
    try {
        // console.log(req.body)
        function genDataChain(TradeInfo) {
            let results = [];
            for (let kv of Object.entries(TradeInfo)) {
                results.push(`${kv[0]}=${kv[1]}`);
            }
            return results.join("&");
        }

        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
        let HashIV  = 'PTJeARVkLqvgtSLC';

        // unix 時間戳
        const dateTime  = new Date().getTime();
        const timestamp = Math.floor(dateTime / 1000);
        const today     = new Date();

        let TradeInfo = {
            RespondType: 'JSON',
            Version    : '1.2',
            MerOrderNo : req.body.MerOrderNo,          //訂單編號
            PeriodNo   : req.body.PeriodNo,            //委託單號
            AlterAmt   : req.body.AlterAmt,                //委託金額
            AlterType  : 'terminate',                  //委託狀態 terminate = 終止委託 suspend = 暫停委託
            TimeStamp  : timestamp.toString(),
        }

        console.log(TradeInfo,999)

        let cipher        = crypto.createCipheriv("aes256", HashKey, HashIV);
        let encryptedData = cipher.update(genDataChain(TradeInfo), "utf-8", "hex");
        encryptedData += cipher.final("hex");

        // let upData = {
        //     'MerchantID_':'MS139521836',
        //     'PostData_':encryptedData,
        // }

        var data = new FormData();
        data.append('MerchantID_', 'MS3703922690');
        data.append('PostData_', encryptedData);

        var config     = {
            method: 'post',
            // url: 'https://ccore.newebpay.com/MPG/period/AlterStatus',
            url    : 'https://core.newebpay.com/MPG/period/AlterAmt',
            headers: {
                ...data.getHeaders()
            },
            data   : data
        };
        const response = await axios(config)
        console.log(response.data, 123);

        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
            let HashIV  = 'PTJeARVkLqvgtSLC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        // let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.period));
        let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.period));
        console.log(getData, 456);

        // 新增
        // let params = {
        //     'paymentId'          : '',
        //     'userId'             : req.body.userId,
        //     'userLevel'          : 2,
        //     'payment_enabled'    : false,
        //     'created_at'         : FieldValue.serverTimestamp(),
        //     'payment_orderNo'    : req.body.MerOrderNo,    // 訂單編號
        //     'payment_prdName'    : req.body.ProdDesc,      // 商品名稱
        //     'payment_amt'        : req.body.PeriodAmt,     // 訂單金額
        //     'payment_type'       : req.body.PeriodType,    // 週期類別 M Y
        //     'payment_priodNo'    : '',
        //     'start_time'        : req.body.start_time,
        //     'end_time'          : req.body.end_time,
        // }
        // const addPayment = async(params) => {
        //     try {
        //         params.created_at = new Date().format("yyyy-MM-dd hh:mm:ss");
        //         const docRef = await db.collection('payment').add(params);
        //         const result = await docRef.update({
        //             paymentId: docRef.id
        //         });
        //         return result;
        //     }
        //     catch (error) {
        //         throw error;
        //     }
        // }

        // 建立一筆訂單 到payment
        // addPayment(params)

        res.status(200).send(getData);
    }
    catch (err) {

    }
})

//金流 - 取消訂閱
app.post("/payment/calendar", async (req, res, next) => {
    try {
        // console.log(req.body)
        function genDataChain(TradeInfo) {
            let results = [];
            for (let kv of Object.entries(TradeInfo)) {
                results.push(`${kv[0]}=${kv[1]}`);
            }
            return results.join("&");
        }

        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
        let HashIV  = 'PTJeARVkLqvgtSLC';

        // unix 時間戳
        const dateTime  = new Date().getTime();
        const timestamp = Math.floor(dateTime / 1000);
        const today     = new Date();

        let TradeInfo = {
            RespondType: 'JSON',
            Version    : '1.0',
            MerOrderNo : req.body.MerOrderNo,          //訂單編號
            PeriodNo   : req.body.PeriodNo,            //委託單號
            AlterType  : 'terminate',                  //委託狀態 terminate = 終止委託 suspend = 暫停委託
            TimeStamp  : timestamp.toString(),
        }

        let cipher        = crypto.createCipheriv("aes256", HashKey, HashIV);
        let encryptedData = cipher.update(genDataChain(TradeInfo), "utf-8", "hex");
        encryptedData += cipher.final("hex");

        // let upData = {
        //     'MerchantID_':'MS139521836',
        //     'PostData_':encryptedData,
        // }

        var data = new FormData();
        data.append('MerchantID_', 'MS3703922690');
        data.append('PostData_', encryptedData);

        var config     = {
            method: 'post',
            // url: 'https://ccore.newebpay.com/MPG/period/AlterStatus',
            url    : 'https://core.newebpay.com/MPG/period/AlterStatus',
            headers: {
                ...data.getHeaders()
            },
            data   : data
        };
        const response = await axios(config)
        console.log(response.data, 123);

        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
            let HashIV  = 'PTJeARVkLqvgtSLC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        // let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.period));
        let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.period));
        console.log(getData, 456);

        // 新增
        // let params = {
        //     'paymentId'          : '',
        //     'userId'             : req.body.userId,
        //     'userLevel'          : 2,
        //     'payment_enabled'    : false,
        //     'created_at'         : FieldValue.serverTimestamp(),
        //     'payment_orderNo'    : req.body.MerOrderNo,    // 訂單編號
        //     'payment_prdName'    : req.body.ProdDesc,      // 商品名稱
        //     'payment_amt'        : req.body.PeriodAmt,     // 訂單金額
        //     'payment_type'       : req.body.PeriodType,    // 週期類別 M Y
        //     'payment_priodNo'    : '',
        //     'start_time'        : req.body.start_time,
        //     'end_time'          : req.body.end_time,
        // }
        // const addPayment = async(params) => {
        //     try {
        //         params.created_at = new Date().format("yyyy-MM-dd hh:mm:ss");
        //         const docRef = await db.collection('payment').add(params);
        //         const result = await docRef.update({
        //             paymentId: docRef.id
        //         });
        //         return result;
        //     }
        //     catch (error) {
        //         throw error;
        //     }
        // }

        // 建立一筆訂單 到payment
        // addPayment(params)

        res.status(200).send(getData);
    }
    catch (err) {
        console.log(err)
    }
})

// 續訂
app.post("/payment/keep", async (req, res, next) => {

    // console.log(req.body.Period)
    //  const docRef = await db.collection('paymentData').add({'test':req.body.Period});
    // res.status(200).send('is 200。);
    //解密
    function create_mpg_aes_decrypt(TradeInfo) {
        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'TaysQMSj9snEiVVOpGPmPr7NYCQyYjUL';
        let HashIV  = 'PTJeARVkLqvgtSLC';

        let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
        decrypt.setAutoPadding(false);
        let text      = decrypt.update(TradeInfo, "hex", "utf8");
        let plainText = text + decrypt.final("utf8");
        let result    = plainText.replace(/[\x00-\x20]+/g, "");
        return result;
    }

    let getData = await JSON.parse(create_mpg_aes_decrypt(req.body.Period));

    // if (getData.Status == 'SUCCESS') {
    // getData.Result.MerchantOrderNo//商店自訂的定期定額訂單編號。
    // getData.Result.OrderNo //商店訂單編號_期數
    // getData.Result.TradeNo //藍新金流交易序號。
    // getData.Result.AlreadyTimes
    // getData.Result.PeriodNo //定期定額委託單號
    // getData.Result.NextAuthDate//下期委託授權日期(Y-m-d)。
    // getData.Result 存全部且更新新日期

    // // 查詢付款項目編號
    let setData     = null;
    const citiesRef = db.collection('payment');
    const snapshot  = await citiesRef.where('PeriodNo', '==', getData.Result.PeriodNo).get();
    snapshot.forEach((doc) => {
        setData = doc.data();
    });

    if (setData) {
        getData.Result.userInfo = setData.userInfo;
        const docRef = await db.collection('paymentData').add(getData.Result);
    }

    //
    // // 訂閱成功 延長方案日期
    // let addDayCount = 0;
    // if (setData.payment_type == 'M') {
    //     addDayCount = 31;
    // }
    // if (setData.payment_type == 'Y') {
    //     addDayCount = 365;
    // }
    //
    // // 加入天數後轉為字串
    // let end_time   = ((new Date(setData.end_time)).addDays(addDayCount)).format("yyyy-MM-dd")
    // let start_time = ((new Date(setData.start_time)).addDays(addDayCount)).format("yyyy-MM-dd")

    // //更新付款成功 將續訂結果成功資料存入
    // const updateResult = await db.collection('payment').doc(setData.paymentId).update({
    //     'payment_oldData': setData.payment_oldData.push(getData.Result),
    //     'end_time'       : end_time,
    //     'start_time'     : start_time,
    // });
    //
    // //更新pro日期
    // await db.collection('userData').doc(setData.userId).update({
    //     'paymentDate_end'  : end_time,
    //     'paymentDate_start': start_time,
    //     'userLevel'        : 2,
    // });


    // } else {
    //     res.status(500).send('is 500');
    // }
})
// 銀行信用卡金流
app.post("/credit", async (req, res, next) => {
    try {

        function CreateToken(secret, msg) {


            // const utf8 = unescape(encodeURIComponent(secret));
            // let keyByte = [];
            //
            // for (let i = 0; i < utf8.length; i++) {
            //     keyByte.push(utf8.charCodeAt(i));
            // }
            //
            // const utf82 = unescape(encodeURIComponent(msg));
            // let messageBytes = [];
            // for (let i = 0; i < utf82.length; i++) {
            //     messageBytes.push(utf82.charCodeAt(i));
            // }
            //  crypto.createHmac('sha256', keyByte).update("json").digest("base64");
            // const secret2 = utf8.encode(secret);
            // const msg2 = utf8.encode(msg);
            //
            //
            // var sha256 = crypto.createHash("sha256");
            // sha256.update(msg);//utf8 here
            // var result = sha256.digest("base64");


            // let aa = utf8.encode(secret)
            // let bb  = utf8.encode(msg)
            //
            //  console.log(aa,11)
            //  console.log(bb,22)

            let result = crypto.createHmac('sha256', secret).update(msg).digest("base64");
            // Signature = Base64(HMAC-SHA256(ShaKey, (ShaKey + URI + JSON + nonce)))

            console.log(result)


            // Base64(HMAC-SHA256(ShaKey, (ShaKey + URI + JSON + nonce)))


            return result;

            // secret = secret ?? "";
            // var encoding = new UTF8Encoding();
            //
            // byte[] keyByte = encoding.GetBytes(secret);
            // byte[] messageBytes = encoding.GetBytes(msg);
            //
            // using (HMACSHA256 hmacsha256 = new HMACSHA256(keyByte)){
            //     byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            //     return Convert.ToBase64String(hashmessage);
            // }
            //
            //
            // var crypto = require("crypto");
            // var sha256 = crypto.createHash("sha256");
            // sha256.update("ThisPassword", "utf8");//utf8 here
            // var result = sha256.digest("base64");
            // console.log(result); //d7I986+YD1zS6Wz2XAcDv2K8yw3xIVUp7u/OZiDzhSY=
            //
            // SHA256 sha256 = SHA256Managed.Create(); //utf8 here as well
            // byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes("ThisPassword"));
            // string result = Convert.ToBase64String(bytes);
            // Console.WriteLine(result); //d7I986+YD1zS6Wz2XAcDv2K8yw3xIVUp7u/OZiDzhSY=

        }

        const testApi = 'http://61.219.193.145/api/v1/paypages';
        const bankApi = 'https://api.ubpg.com.tw/api/v1/paypages';
        const bankuri = '/v1/paypages';

        const data = {
            "MerOrderNo"       : req.body.MerOrderNo,
            "Amount"           : req.body.Amount,
            "Currency"         : "TWD",
            "Paytype"          : req.body.PayType,
            "ThreeDomainSecure": true,
            "ReturnUrl"        : req.body.NotifyUrl,
            "NotifyUrl"        : req.body.NotifyUrl
        };

        let resData  = CreateToken(req.body.bankData.hashKey, req.body.bankData.hashKey + '/v1/paypages' + JSON.stringify(data) + req.body.MerOrderNo)
        const config = {
            method : 'post',
            url    : bankApi,
            headers: {
                'X-UB-StoreID': req.body.bankData.storeId, //商城代號
                'X-UB-TaxID'  : req.body.bankData.taxId, //統一編號
                'X-UB-Nonce'  : req.body.MerOrderNo,//隨機瑪
                'X-UB-Auth'   : resData,//簽章
                'Content-Type': 'application/json'
            },
            data   : data
        };

        axios(config).then(function (response) {
            console.log(JSON.stringify(response.data), 999);
            res.status(200).send(response.data);
        }).catch(function (error) {
            console.log(error);
        });

    }
    catch (err) {

    }
})


// getRichmenu
app.post("/getRichmenu", async (req, res, next) => {
    const config = {
        method : 'get',
        url    : 'https://api.line.me/v2/bot/richmenu/list',
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        // data   : {
        //     message: lneMsg,
        //     userId : item.orderData.lineUserId,
        //     token  : item.linaAccessToken,
        // }
    };
    let result   = await axios(config)
    res.json(result.data);
})

function createFileFromBuffer(buffer, filename, mimeType) {
    const {Readable} = require('stream');

    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);

    const file = {
        buffer  : buffer,
        size    : buffer.length,
        mimeType: mimeType,
        name    : filename,
        stream  : () => stream,
    };

    return file;
}

// addRichmenu
app.post("/addRichmenu", async (req, res, next) => {


    // 讀取本地端的 JPG 圖片
    const filePath  = '../app_node/uploads/line-menu-action.jpg';
    const imageData = fs.readFileSync(filePath);

    console.log(imageData);

    const config = {
        method : 'post',
        url    : `https://api.line.me/v2/bot/richmenu`,
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        data   : req.body.data
    };
    await axios(config).then(async (result) => {


        // 上傳圖片
        const configImg = {
            method : 'post',
            url    : `https://api-data.line.me/v2/bot/richmenu/${result.data.richMenuId}/content`,
            headers: {
                'Content-Type' : 'image/jpeg',
                'Authorization': `Bearer ${req.body.token}`,
            },
            data   : imageData
        };
        axios(configImg).then(r => {
            // 設定顯示
            const configShow = {
                method : 'post',
                url    : `https://api.line.me/v2/bot/user/all/richmenu/${result.data.richMenuId}`,
                headers: {
                    'Authorization': `Bearer ${req.body.token}`,
                },
            };
            axios(configShow)
        })
    })


})
// delRichmenu
app.post("/delRichmenu", async (req, res, next) => {

    const config = {
        method : 'delete',
        url    : `https://api.line.me/v2/bot/richmenu/${req.body.richmenuId}`,
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${req.body.token}`,
        },
        // data   : {
        //     message: lneMsg,
        //     userId : item.orderData.lineUserId,
        //     token  : item.linaAccessToken,
        // }
    };
    let result   = await axios(config)
    res.json(result.data);
})


// tg
app.post("/tgtest", async (req, res, next) => {

    console.log(req.body)

    res.status(200).send('calendartest');
})

const server = app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})