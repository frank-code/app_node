const serviceAccount = {
  "type"                       : process.env.TYPE,
  "project_id"                 : process.env.PROJECT_ID,
  "private_key_id"             : process.env.PRIVATE_KEY_ID,
  "private_key"                : process.env.PRIVATE_KEY,
  "client_id"                  : process.env.CLIENT_EMAIL,
  "auth_uri"                   : process.env.CLIENT_ID,
  "token_uri"                  : process.env.AUTH_URL,
  "auth_provider_x509_cert_url": process.env.TOKEN_URL,
  "client_x509_cert_url"       : process.env.AUTH_PROVIDER_X509_CERT_URL,
};

export {
  serviceAccount,
}

