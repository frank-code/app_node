const port                                      = process.env.PORT || 80;
const httpProxy = require('http-proxy');
const express                                   = require('express')
const bodyParser                                = require('body-parser')
const app                                       = express()
const axios                                     = require('axios');
const FormData                                  = require('form-data');
const router                                    = express.Router();
const cors                                      = require('cors')
const url                                       = require('url');
// const qs             = require('qs');
const request                                   = require('request');
const LinePay                                   = require('line-pay-v3')
const admin                                     = require("firebase-admin");

const serviceAccount                            = require("./key/groupbuy-10352-firebase-adminsdk-b9rxy-92e7662016.json");

// import serviceAccount from './key.js';
const {initializeApp, applicationDefault, cert} = require('firebase-admin/app');

const {getFirestore, Timestamp, FieldValue} = require('firebase-admin/firestore');

const line       = require('@line/bot-sdk');
const cron       = require('node-cron');
const nodemailer = require('nodemailer');
const utf8       = require('nodejs-utf8');
var crypto       = require('crypto');

const { createProxyMiddleware } = require('http-proxy-middleware');
const proxy = httpProxy.createProxyServer({});

const proxyOptions = {
    target: 'http://52.41.98.11:8000/', // 內部伺服器的地址和端口
    changeOrigin: true,
    // 其他代理配置...
};
// 中介軟體處理轉發
app.use((req, res, next) => {
    // 檢查請求的網域是否符合指定條件
    if (req.hostname === 'newdata.younee.app') {
        // 將網址轉換至指定的 port (8000)
        // proxy.web(req, res, { target: `http://localhost:8000` });

        const proxyMiddleware = createProxyMiddleware('/test', proxyOptions);

        app.use(proxyMiddleware);
    }

    // 若不符合轉發條件，繼續處理其他請求
    next();
});

admin.initializeApp({
    credential : admin.credential.cert(serviceAccount),
    databaseURL: "https://groupbuy-10352-default-rtdb.asia-southeast1.firebasedatabase.app"
});

const db = getFirestore();


app.use(cors())
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

Date.prototype.format  = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        // "h+": this.getHours() - (this.getHours() >= 12 ? 12 : 0) + '',                   //小时
        "h+": this.getHours(),
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S" : this.getMilliseconds(),             //毫秒
        "t" : this.getHours() >= 12 ? 'PM' : 'AM'
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return this;
}

// 工作排程
cron.schedule('0 0 1 * * *', async () => {
    const checkDay1 = new Date();
    const checkDay2 = new Date(checkDay1.getTime() + 24 * 60 * 60 * 1000);
    console.log(checkDay1, checkDay2);
    let content     = [];
    const citiesRef = db.collection('reserveData');
    const snapshot  = await citiesRef.where('reserveGetWorkDay', '>=', checkDay1).where('reserveGetWorkDay', '<', checkDay2).where('reserveStatus', '==', 1).get();
    snapshot.forEach(doc => {
        content.push(doc.data());
    });
    console.log(content);

    content.forEach((item) => {
        const lneMsg     = [
            {
                "type"    : "flex",
                "altText" : '預約提醒通知',
                "contents": {
                    "type"  : "bubble",
                    "body"  : {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : item.reserveShop + '-預約提醒',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "margin"  : "lg",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"    : "box",
                                        "layout"  : "baseline",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type" : "text",
                                                "text" : "預約項目",
                                                "color": "#aaaaaa",
                                                "size" : "sm",
                                                "flex" : 2
                                            },
                                            {
                                                "type" : "text",
                                                "text" : item.orderData.serviceName,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "sm",
                                                "flex" : 5
                                            }
                                        ]
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "baseline",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type" : "text",
                                                "text" : "預約時間",
                                                "color": "#aaaaaa",
                                                "size" : "sm",
                                                "flex" : 2
                                            },
                                            {
                                                "type" : "text",
                                                "text" : item.orderData.reserveGetWorkTime,
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "sm",
                                                "flex" : 5
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    "footer": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "spacing" : "sm",
                        "contents": [
                            {
                                "type"  : "button",
                                "style" : "primary",
                                "height": "sm",
                                "action": {
                                    "type" : "uri",
                                    "label": "查看詳細",
                                    "uri"  : `https://liff.line.me/1656769636-1nZBq8KG/member/order-detail/?orderId=${item.reserveId}`
                                },
                                "color" : "#555555"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [],
                                "margin"  : "sm"
                            }
                        ],
                        "flex"    : 0
                    }
                }
            }
        ]
        const configUser = {
            method: 'post',
            url   : 'https://node.younee.app/lineMsgPush',
            // url    : 'http://localhost:80/lineMsgPush',
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                message: lneMsg,
                userId : item.orderData.lineUserId,
                token  : item.linaAccessToken,
            }
        };

        console.log(configUser)

        axios(configUser).then(function (response) {
            // console.log(JSON.stringify(response.data));
        }).catch(function (error) {
            // console.log(error);
        });

        const configShop = {
            method: 'post',
            url   : 'https://node.younee.app/lineMsgPush',
            // url    : 'http://localhost:80/lineMsgPush',
            headers: {
                'Content-Type': 'application/json',
            },
            data   : {
                message: lneMsg,
                userId : item.lineShopId,
                token  : item.linaAccessToken,
            }
        };

        console.log(configShop)

        axios(configShop).then(function (response) {
            // console.log(JSON.stringify(response.data));
        }).catch(function (error) {
            // console.log(error);
        });
    })
})

app.get("/firebase", async (req, res, next) => {
    try {
        const checkDay1 = new Date();
        const checkDay2 = new Date(checkDay1.getTime() + 24 * 60 * 60 * 1000);
        console.log(checkDay1, checkDay2);
        let content     = [];
        const citiesRef = db.collection('reserveData');
        const snapshot  = await citiesRef.where('reserveGetWorkDay', '>=', checkDay1).where('reserveGetWorkDay', '<', checkDay2).where('reserveStatus', '==', 1).get();
        snapshot.forEach(doc => {
            content.push(doc.data());
        });
        console.log(content);

        content.forEach((item) => {
            if (item.reserveShopId === '7fr4OXh3BcSliqY0Q8DP8aHqG3K3') {
                const lneMsg     = [
                    {
                        "type"    : "flex",
                        "altText" : '預約提醒通知',
                        "contents": {
                            "type"  : "bubble",
                            "body"  : {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [
                                    {
                                        "type"  : "text",
                                        "text"  : item.reserveShop + '-預約提醒',
                                        "weight": "bold",
                                        "size"  : "xl"
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "margin"  : "lg",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type"    : "box",
                                                "layout"  : "baseline",
                                                "spacing" : "sm",
                                                "contents": [
                                                    {
                                                        "type" : "text",
                                                        "text" : "預約項目",
                                                        "color": "#aaaaaa",
                                                        "size" : "sm",
                                                        "flex" : 2
                                                    },
                                                    {
                                                        "type" : "text",
                                                        "text" : item.orderData.serviceName,
                                                        "wrap" : true,
                                                        "color": "#666666",
                                                        "size" : "sm",
                                                        "flex" : 5
                                                    }
                                                ]
                                            },
                                            {
                                                "type"    : "box",
                                                "layout"  : "baseline",
                                                "spacing" : "sm",
                                                "contents": [
                                                    {
                                                        "type" : "text",
                                                        "text" : "預約時間",
                                                        "color": "#aaaaaa",
                                                        "size" : "sm",
                                                        "flex" : 2
                                                    },
                                                    {
                                                        "type" : "text",
                                                        "text" : item.orderData.reserveGetWorkTime,
                                                        "wrap" : true,
                                                        "color": "#666666",
                                                        "size" : "sm",
                                                        "flex" : 5
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            "footer": {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"  : "button",
                                        "style" : "primary",
                                        "height": "sm",
                                        "action": {
                                            "type" : "uri",
                                            "label": "詳細",
                                            "uri"  : `https://liff.line.me/1656769636-1nZBq8KG/member/order-detail/?orderId=${item.reserveId}&status=9`
                                        },
                                        "color" : "#555555"
                                    },
                                    {
                                        "type"    : "box",
                                        "layout"  : "vertical",
                                        "contents": [],
                                        "margin"  : "sm"
                                    }
                                ],
                                "flex"    : 0
                            }
                        }
                    }
                ]
                const config     = {
                    method: 'post',
                    // url    : 'https://node.younee.app/lineMsgPush',
                    url    : 'http://localhost:80/lineMsgPush',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data   : {
                        message: lneMsg,
                        userId : item.orderData.lineUserId,
                        token  : item.linaAccessToken,
                    }
                };
                const configshop = {
                    method: 'post',
                    // url    : 'https://node.younee.app/lineMsgPush',
                    url    : 'http://localhost:80/lineMsgPush',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data   : {
                        message: lneMsg,
                        userId : item.lineShopId,
                        token  : item.linaAccessToken,
                    }
                };


                axios(config).then(function (response) {
                    // console.log(JSON.stringify(response.data));
                }).catch(function (error) {
                    // console.log(error);
                });
                axios(configshop).then(function (response) {
                    // console.log(JSON.stringify(response.data));
                }).catch(function (error) {
                    // console.log(error);
                });
            }

        })
    }
    catch (err) {
        console.log(err)
    }
})

app.post('/link', async (req, res) => {
    let formData = req.body;
    // console.log('form data888', req.body);
    // console.log('form data777', formData.code)

    //取得lineData資料
    let content     = [];
    const citiesRef = db.collection('lineData');
    const snapshot  = await citiesRef.where('userId', '==', formData.state).get();
    snapshot.forEach(doc => {
        content = doc.data();
    });


    try {
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-bot.line.me/oauth/token',
            'headers': {
                'Cookie': 'XSRF-TOKEN=9ab3e848-ba0e-4f3a-9c54-8079be7aa2e6'
            },
            formData : {
                'grant_type'   : 'authorization_code',
                'redirect_uri' : 'https://node.younee.app/link',
                'code'         : formData.code,
                'client_id'    : content.client_id,
                'client_secret': content.client_secret
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            var getData = JSON.parse(response.body)

            // fireData.ref('adminLine').set({token:'Bearer '+getData.access_token })
            content.access_token = 'Bearer ' + getData.access_token;

            db.collection('lineData').doc(content.userId).set(content);

            // res.status(200).send('設定成功！')
            // res.location(content.siteUrl);
            res.redirect(content.siteUrl);
        });
    }
    catch (err) {
        console.log(err)
    }
})

app.post("/getMsg", async (req, res, next) => {
    // console.log(req.body);
    try {

        //取得lineData資料
        let content     = [];
        const citiesRef = db.collection('lineData');
        const snapshot  = await citiesRef.where('userId', '==', req.body.userId).get();
        snapshot.forEach(doc => {
            content = doc.data();
        });
        var options = {
            'method' : 'POST',
            'url'    : 'https://notify-api.line.me/api/notify',
            'headers': {
                'Authorization': content.access_token
            },
            formData : {
                'message': req.body.message
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            // console.log(response.body);
            res.status(200).send({
                result: true,
            });
        });
    }
    catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 token
app.post("/smsToken", async (req, res, next) => {
    // console.log(req.body);
    try {
        //取得lineData資料
        // let tdata    = {
        //   "HandlerType": 3,
        //   "VerifyType" : 1,
        //   "UID"        : "ye0205414225",
        //   "PWD"        : "Aa859230"
        // };
        // var options = {
        //   'method' : 'POST',
        //   'url'    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
        //   'headers': {
        //     'Content-Type': 'application/json'
        //   },
        //   formData : tdata
        // };
        //
        // axios.post('http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',tdata).then(res=>{
        //   res.status(200).send({
        //     result:res.data,
        //   });
        // });

        var data = JSON.stringify({"HandlerType": 3, "VerifyType": 1, "UID": "ye0205414225", "PWD": "Aa859230"});

        var config = {
            method : 'post',
            url    : 'http://api.every8d.com/API21/HTTP/ConnectionHandler.ashx',
            headers: {
                'Content-Type': 'application/json'
            },
            data   : data
        };

        axios(config).then(function (response) {
            res.status(200).send({
                result: response.data,
            });
        }).catch(function (error) {
            console.log(error);
        });


        // request(options, function (error, response) {
        //   if (error) throw new Error(error);
        //   res.status(200).send({
        //     result:response,
        //   });
        // });
    }
    catch (err) {
        console.log(err)
    }
})

// 簡訊寄送 送出內容
app.post("/smsSend", async (req, res, next) => {
    try {
        var options = {
            'method' : 'POST',
            'url'    : 'http://api.every8d.com/API21/HTTP/SendSMS.ashx',
            'headers': {
                'Authorization': req.body.token
            },
            formData : req.body.content
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            res.status(200).send({
                result: true,
            });
        });
    }
    catch (err) {
        console.log(err)
    }
})

// Line Pay 送出訂單
app.post("/linepay", async (req, res, next) => {
    let linePay = new LinePay({
        channelId    : req.body.linePayData.channelId,
        channelSecret: req.body.linePayData.channelSecret,
        uri          : req.body.linePayData.uri
    })
    // console.log(req.body.order.packages[0].products)

    linePay.request(req.body.order).then(response => {
        // console.log( response.info.paymentUrl.web)
        if (response.returnCode == '0000') {
            // console.log(response);
            // res.redirect(response.info.paymentUrl.web)
            // res.location(response.info.paymentUrl.web0);
            res.status(200).send({data: response});
        }
    });
});
// Line Pay 送出訂單
app.get("/linepaytest", async (req, res, next) => {
    let linePay = new LinePay({
        channelId    : "1657022987",//'1657022987',//
        channelSecret: "062dc13e35c24bdae59490c4fd0c20a7", //,'062dc13e35c24bdae59490c4fd0c20a7'
        uri          : "https://sandbox-api-pay.line.me"
    })

    const order = {
        amount      : 5000,
        currency    : 'TWD',
        orderId     : 'Order2019101500001',
        packages    : [
            {
                id      : 'Item20191015001',
                amount  : 5000,
                name    : 'testPackageName',
                products: [
                    {
                        name    : 'testProductName',
                        quantity: 1,
                        price   : 2500
                    },
                    {
                        name    : 'testProductName2',
                        quantity: 1,
                        price   : 2500
                    },
                ]
            }
        ],
        redirectUrls: {
            confirmUrl: 'http://localhost/confirmUrl',
            cancelUrl : 'http://localhost/cancelUrl'
        }
    }

    linePay.request(order).then(response => {
        res.redirect(response.info.paymentUrl.web)
    });
});
app.get("/confirmUrl", async (req, res, next) => {

    var urlObj          = url.parse(req.url, true)
    const transactionId = urlObj.query.transactionId;
    const orderId       = urlObj.query.orderId;

    //取得訂單資料
    let content     = [];
    const citiesRef = db.collection('order');
    const snapshot  = await citiesRef.where('orderId', '==', orderId).get();
    snapshot.forEach(doc => {
        content = doc.data();
    });



    //取得line資料
    let lineData  = [];
    const line    = db.collection('lineData');
    const getLine = await line.where('userId', '==', content.shopId).get();
    getLine.forEach(doc => {
        lineData = doc.data();
    });

    //產品價格
    const confrimData = {
        amount  : content.orderPrice,
        currency: 'TWD',
    }

    let linepayApi = 'https://api-pay.line.me';

    if (lineData.userId == '7fr4OXh3BcSliqY0Q8DP8aHqG3K3') {
        linepayApi = 'https://sandbox-api-pay.line.me';
    }

    let linePay = new LinePay({
        channelId    : lineData.channel_id,
        channelSecret: lineData.channel_secret,
        uri          : linepayApi
    })
    // console.log(confrimData, transactionId, 444)
    await linePay.confirm(confrimData, transactionId).then(response => {
        // console.log(response)
        if (response.returnCode == '0000') {
            res.redirect(`https://younee.app/confirm?transactionId=${transactionId}&orderId=${orderId}`)
            // res.redirect(`http://localhost:3000/confirm?transactionId=${transactionId}&orderId=${orderId}`)
        }
    })


})
app.post("/cancel", async (req, res, next) => {

    let linepayApi = 'https://api-pay.line.me';

    if (req.body.userId == '7fr4OXh3BcSliqY0Q8DP8aHqG3K3') {
        linepayApi = 'https://sandbox-api-pay.line.me';
    }

    let linePay = new LinePay({
        channelId    : req.body.channelId,
        channelSecret: req.body.channelSecret,
        uri          : linepayApi
    })

    linePay.refund({refundAmount: null}, req.body.transactionId).then(response => {
        // console.log(response)
        res.status(200).send(response);
    })
})

app.get("/test", async (req, res, next) => {
    res.status(200).send('OK1');
})

app.post("/lineMsgPush", async (req, res, next) => {
    console.log(req.body.userId, req.body.message);
    var data = JSON.stringify({"to": req.body.userId, "messages": req.body.message});

    var config = {
        method : 'post',
        url    : 'https://api.line.me/v2/bot/message/push',
        headers: {
            'Content-Type' : 'application/json',
            'Authorization': 'Bearer ' + req.body.token
        },
        data   : data
    };

    axios(config).then(function (response) {
        // console.log(JSON.stringify(response.data));
    }).catch(function (error) {
        console.log(error);
    });

})

app.post("/webhook", async (req, res, next) => {
    const client = new line.Client({
        channelAccessToken: 'ftWsq1o/NOQZ25a0DFEYnrJLZh6EPKUFpuY/1YqmBIzDoAxYtHQgwfGkMWiAH0ABARtWYuN1kh9w+Lk4OqnmeiknTyIzowfZJ9YNB1yRKzM53NBaDo42FyGff4vM/yyMnfc3gjm6OOTIx7j6tlqlTwdB04t89/1O/w1cDnyilFU='
    });
    if (req.body.events[0].message.text == '我的預約') {
        const message = [
            {
                "type"    : "flex",
                "altText" : '我的預約',
                "contents": {
                    "type"  : "bubble",
                    "body"  : {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : '我的預約記錄',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "margin"  : "lg",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"    : "box",
                                        "layout"  : "baseline",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type" : "text",
                                                "text" : '若商家已確認，預約無法取消，請聯繫商家做更改。',
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "sm",
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    "footer": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "spacing" : "sm",
                        "contents": [
                            {
                                "type"  : "button",
                                "style" : "primary",
                                "height": "sm",
                                "action": {
                                    "type" : "uri",
                                    "label": "查看預約記錄",
                                    "uri"  : `https://liff.line.me/1656769636-1nZBq8KG/member/order-list`
                                },
                                "color" : "#555555"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [],
                                "margin"  : "sm"
                            }
                        ],
                        "flex"    : 0
                    }
                }
            }
        ]
        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }
    if (req.body.events[0].message.text == '優惠券') {
        const message = [
            {
                "type"    : "flex",
                "altText" : '優惠券',
                "contents": {
                    "type": "bubble",
                    "body": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : '優惠券尚未開放使用',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            // {
                            //     "type": "box",
                            //     "layout": "vertical",
                            //     "margin": "lg",
                            //     "spacing": "sm",
                            //     "contents": [
                            //         {
                            //             "type": "box",
                            //             "layout": "baseline",
                            //             "spacing": "sm",
                            //             "contents": [
                            //                 {
                            //                     "type": "text",
                            //                     "text": '若商家已確認，預約無法取消，請聯繫商家做更改。',
                            //                     "wrap": true,
                            //                     "color": "#666666",
                            //                     "size": "sm",
                            //                 }
                            //             ]
                            //         },
                            //     ]
                            // }
                        ]
                    },
                    // "footer": {
                    //     "type": "box",
                    //     "layout": "vertical",
                    //     "spacing": "sm",
                    //     "contents": [
                    //         {
                    //             "type": "button",
                    //             "style": "primary",
                    //             "height": "sm",
                    //             "action": {
                    //                 "type": "uri",
                    //                 "label": "查看預約記錄",
                    //                 "uri": `https://liff.line.me/1656769636-1nZBq8KG/member/order-list`
                    //             },
                    //             "color": "#555555"
                    //         },
                    //         {
                    //             "type": "box",
                    //             "layout": "vertical",
                    //             "contents": [],
                    //             "margin": "sm"
                    //         }
                    //     ],
                    //     "flex": 0
                    // }
                }
            }
        ]
        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }
    if (req.body.events[0].message.text == '紅利點數') {
        const message = [
            {
                "type"    : "flex",
                "altText" : '紅利點數',
                "contents": {
                    "type": "bubble",
                    "body": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : '紅利點數尚未開放使用',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            // {
                            //     "type": "box",
                            //     "layout": "vertical",
                            //     "margin": "lg",
                            //     "spacing": "sm",
                            //     "contents": [
                            //         {
                            //             "type": "box",
                            //             "layout": "baseline",
                            //             "spacing": "sm",
                            //             "contents": [
                            //                 {
                            //                     "type": "text",
                            //                     "text": '若商家已確認，預約無法取消，請聯繫商家做更改。',
                            //                     "wrap": true,
                            //                     "color": "#666666",
                            //                     "size": "sm",
                            //                 }
                            //             ]
                            //         },
                            //     ]
                            // }
                        ]
                    },
                    // "footer": {
                    //     "type": "box",
                    //     "layout": "vertical",
                    //     "spacing": "sm",
                    //     "contents": [
                    //         {
                    //             "type": "button",
                    //             "style": "primary",
                    //             "height": "sm",
                    //             "action": {
                    //                 "type": "uri",
                    //                 "label": "查看預約記錄",
                    //                 "uri": `https://liff.line.me/1656769636-1nZBq8KG/member/order-list`
                    //             },
                    //             "color": "#555555"
                    //         },
                    //         {
                    //             "type": "box",
                    //             "layout": "vertical",
                    //             "contents": [],
                    //             "margin": "sm"
                    //         }
                    //     ],
                    //     "flex": 0
                    // }
                }
            }
        ]
        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }
})


app.post("/sendEmail", async (req, res, next) => {
    console.log(req)
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        auth: {
            user: 'ye0205414225@gmail.com',
            pass: 'vmuzzpmnpjjbreka',
        },
    });
    transporter.sendMail({
        from   : 'service@younee.app',
        to     : req.body.sendEmail,
        subject: req.body.sendTitle,
        html   : req.body.sendContent,
    }).then(info => {
        console.log({info});
    }).catch(console.error);
})


//金流－支付訂閱
app.post("/payment", async (req, res, next) => {
    try {

        function genDataChain(TradeInfo) {
            let results = [];
            for (let kv of Object.entries(TradeInfo)) {
                results.push(`${kv[0]}=${kv[1]}`);
            }
            return results.join("&");
        }


        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'ELut09PgCyRPrlqVJrRItjy439YG82VU';
        let HashIV  = 'PBeUMsIvqpa0LizC';

        // unix 時間戳
        const dateTime  = new Date().getTime();
        const timestamp = Math.floor(dateTime / 1000);

        const today = new Date();

        let TradeInfo = {
            respondType: 'JSON',
            TimeStamp  : timestamp,
            Version    : '1.0',
            MerOrderNo : req.body.MerOrderNo,              //訂單編號
            ProdDesc   : req.body.ProdDesc,                //商品名稱
            PeriodAmt  : req.body.PeriodAmt,              //訂單金額
            PeriodType : req.body.PeriodType,             //週期類別 M Y
            PeriodPoint: req.body.PeriodPoint,            //交易授權時間 每月的話幾號 年的話 MMDD  0315 月日
            PeriodTimes: req.body.PeriodTimes,            //授權週期數
            // Extday              : '0521',                        //信用卡到期日
            PeriodStartType: 2,                            //檢查信用卡10元驗證
            ReturnURL      : req.body.ReturnURL,            //1.當付款人首次執行信用卡授權交易完成後，以 Form Post 方式導回商店頁面。
            PaymentInfo    : 'N',
            OrderInfo      : 'N',
            NotifyURL      : req.body.NotifyURL,              // 每期授權結果通知
            BackURL        : req.body.BackURL,                // 取消交易時返回商店的網址
        }

        console.log(TradeInfo)


        let cipher        = crypto.createCipheriv("aes256", HashKey, HashIV);
        let encryptedData = cipher.update(genDataChain(TradeInfo), "utf-8", "hex");
        encryptedData += cipher.final("hex");
        // console.log("Encrypted message: " + encryptedData);
        //
        // res.status(200).send({
        //     result: true,
        // });


        // 新增
        let params = {
            'paymentId'      : '',
            'userId'         : req.body.userId,
            'userLevel'      : 2,
            'payment_enabled': false,
            'created_at'     : FieldValue.serverTimestamp(),
            'payment_orderNo': req.body.MerOrderNo,    // 訂單編號
            'payment_prdName': req.body.ProdDesc,      // 商品名稱
            'payment_amt'    : req.body.PeriodAmt,     // 訂單金額
            'payment_type'   : req.body.PeriodType,    // 週期類別 M Y
            'payment_priodNo': '',
            'start_time'     : req.body.start_time,
            'end_time'       : req.body.end_time,
        }

        const addPayment = async (params) => {
            try {
                params.created_at = new Date().format("yyyy-MM-dd hh:mm:ss");
                const docRef      = await db.collection('payment').add(params);
                const result      = await docRef.update({
                    paymentId: docRef.id
                });
                return result;
            }
            catch (error) {
                throw error;
            }
        }
        // 建立一筆訂單 到payment
        addPayment(params)
        res.status(200).send(encryptedData);

    }
    catch (err) {

    }
})

//金流－刷卡回傳結果
app.post("/payment/result", async (req, res, next) => {
    try {

        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'ELut09PgCyRPrlqVJrRItjy439YG82VU';
            let HashIV  = 'PBeUMsIvqpa0LizC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        let getData = await JSON.parse(create_mpg_aes_decrypt(req.body.Period));

        console.log(getData);

        if (getData.Status == 'SUCCESS') {

            // 查詢付款項目編號
            var setData     = [];
            const citiesRef = db.collection('payment');
            const snapshot  = await citiesRef.where('payment_orderNo', '==', getData.Result.MerchantOrderNo).get();
            snapshot.forEach((doc) => {
                console.log(doc.id, '=>', doc.data());
                setData = doc.data();
            });

            let payment_oldData = []; //陣列初始化
            //更新付款成功 啟用
            const updateResult = await db.collection('payment').doc(setData.paymentId).update({
                'payment_enabled': true,
                'payment_priodNo': getData.Result.PeriodNo,
                'payment_oldData': payment_oldData,
            });

            //更新pro日期
            await db.collection('userData').doc(setData.userId).update({
                'paymentDate_end'  : setData.end_time,
                'paymentDate_start': setData.start_time,
                'userLevel'        : 2,
            });

            res.redirect(`https://younee.app/payresult?status=success&type=${getData.Result.PeriodType}&amt=${getData.Result.PeriodAmt}`)
        } else {
            res.redirect(`https://younee.app/payresult?status=error&type=${getData.Result.PeriodType}&amt=${getData.Result.PeriodAmt}`)
        }


    }
    catch (err) {

    }
})


//金流 - 取消訂閱
app.post("/payment/calendar", async (req, res, next) => {
    try {
        // console.log(req.body)
        function genDataChain(TradeInfo) {
            let results = [];
            for (let kv of Object.entries(TradeInfo)) {
                results.push(`${kv[0]}=${kv[1]}`);
            }
            return results.join("&");
        }

        //測試
        // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
        // let HashIV = 'C9Pt75HPAMSkKEQP';
        //正式
        let HashKey = 'ELut09PgCyRPrlqVJrRItjy439YG82VU';
        let HashIV  = 'PBeUMsIvqpa0LizC';

        // unix 時間戳
        const dateTime  = new Date().getTime();
        const timestamp = Math.floor(dateTime / 1000);
        const today     = new Date();

        let TradeInfo = {
            RespondType: 'JSON',
            Version    : '1.0',
            MerOrderNo : req.body.MerOrderNo,          //訂單編號
            PeriodNo   : req.body.PeriodNo,            //委託單號
            AlterType  : 'terminate',                  //委託狀態 terminate = 終止委託 suspend = 暫停委託
            TimeStamp  : timestamp.toString(),
        }

        let cipher        = crypto.createCipheriv("aes256", HashKey, HashIV);
        let encryptedData = cipher.update(genDataChain(TradeInfo), "utf-8", "hex");
        encryptedData += cipher.final("hex");

        // let upData = {
        //     'MerchantID_':'MS139521836',
        //     'PostData_':encryptedData,
        // }

        var data = new FormData();
        data.append('MerchantID_', 'MS139521836');
        data.append('PostData_', encryptedData);

        var config     = {
            method: 'post',
            // url: 'https://ccore.newebpay.com/MPG/period/AlterStatus',
            url    : 'https://core.spgateway.com/MPG/period/AlterStatus',
            headers: {
                ...data.getHeaders()
            },
            data   : data
        };
        const response = await axios(config)
        console.log(response.data, 123);

        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'ELut09PgCyRPrlqVJrRItjy439YG82VU';
            let HashIV  = 'PBeUMsIvqpa0LizC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.period));

        console.log(getData, 456);

        // 新增
        // let params = {
        //     'paymentId'          : '',
        //     'userId'             : req.body.userId,
        //     'userLevel'          : 2,
        //     'payment_enabled'    : false,
        //     'created_at'         : FieldValue.serverTimestamp(),
        //     'payment_orderNo'    : req.body.MerOrderNo,    // 訂單編號
        //     'payment_prdName'    : req.body.ProdDesc,      // 商品名稱
        //     'payment_amt'        : req.body.PeriodAmt,     // 訂單金額
        //     'payment_type'       : req.body.PeriodType,    // 週期類別 M Y
        //     'payment_priodNo'    : '',
        //     'start_time'        : req.body.start_time,
        //     'end_time'          : req.body.end_time,
        // }
        // const addPayment = async(params) => {
        //     try {
        //         params.created_at = new Date().format("yyyy-MM-dd hh:mm:ss");
        //         const docRef = await db.collection('payment').add(params);
        //         const result = await docRef.update({
        //             paymentId: docRef.id
        //         });
        //         return result;
        //     }
        //     catch (error) {
        //         throw error;
        //     }
        // }

        // 建立一筆訂單 到payment
        // addPayment(params)

        res.status(200).send(getData);
    }
    catch (err) {

    }
})

// 續訂
app.post("/payment/keep", async (req, res, next) => {
    try {
        console.log(req.body.Period)
        let TradeInfo = req.body.Period;

        //解密
        function create_mpg_aes_decrypt(TradeInfo) {
            //測試
            // let HashKey = 'vxXfasst7tFBazlzU03qcDEVlvW2272O';
            // let HashIV = 'C9Pt75HPAMSkKEQP';
            //正式
            let HashKey = 'ELut09PgCyRPrlqVJrRItjy439YG82VU';
            let HashIV  = 'PBeUMsIvqpa0LizC';

            let decrypt = crypto.createDecipheriv("aes256", HashKey, HashIV);
            decrypt.setAutoPadding(false);
            let text      = decrypt.update(TradeInfo, "hex", "utf8");
            let plainText = text + decrypt.final("utf8");
            let result    = plainText.replace(/[\x00-\x20]+/g, "");
            return result;
        }

        let getData = await JSON.parse(create_mpg_aes_decrypt(response.data.Period));
        console.log(getData)
        if (getData.Status == 'SUCCESS') {
            // getData.Result.MerchantOrderNo//商店自訂的定期定額訂單編號。
            // getData.Result.OrderNo //商店訂單編號_期數
            // getData.Result.TradeNo //藍新金流交易序號。
            // getData.Result.AlreadyTimes
            // getData.Result.PeriodNo //定期定額委託單號
            // getData.Result.NextAuthDate//下期委託授權日期(Y-m-d)。
            // getData.Result 存全部且更新新日期

            // 查詢付款項目編號
            var setData     = [];
            const citiesRef = db.collection('payment');
            const snapshot  = await citiesRef.where('payment_orderNo', '==', getData.Result.MerchantOrderNo).get();
            snapshot.forEach((doc) => {
                console.log(doc.id, '=>', doc.data());
                setData = doc.data();
            });

            // 訂閱成功 延長方案日期
            let addDayCount = 0;
            if (setData.payment_type == 'M') {
                addDayCount = 31;
            }
            if (setData.payment_type == 'Y') {
                addDayCount = 365;
            }

            // 加入天數後轉為字串
            let end_time   = ((new Date(setData.end_time)).addDays(addDayCount)).format("yyyy-MM-dd")
            let start_time = ((new Date(setData.start_time)).addDays(addDayCount)).format("yyyy-MM-dd")

            //更新付款成功 將續訂結果成功資料存入
            const updateResult = await db.collection('payment').doc(setData.paymentId).update({
                'payment_oldData': setData.payment_oldData.push(getData.Result),
                'end_time'       : end_time,
                'start_time'     : start_time,
            });

            //更新pro日期
            await db.collection('userData').doc(setData.userId).update({
                'paymentDate_end'  : end_time,
                'paymentDate_start': start_time,
                'userLevel'        : 2,
            });
        }

    }
    catch (err) {

    }
})
// 銀行信用卡金流
app.post("/credit", async (req, res, next) => {
    try {

        function CreateToken(secret, msg) {


            // const utf8 = unescape(encodeURIComponent(secret));
            // let keyByte = [];
            //
            // for (let i = 0; i < utf8.length; i++) {
            //     keyByte.push(utf8.charCodeAt(i));
            // }
            //
            // const utf82 = unescape(encodeURIComponent(msg));
            // let messageBytes = [];
            // for (let i = 0; i < utf82.length; i++) {
            //     messageBytes.push(utf82.charCodeAt(i));
            // }
            //  crypto.createHmac('sha256', keyByte).update("json").digest("base64");
            // const secret2 = utf8.encode(secret);
            // const msg2 = utf8.encode(msg);
            //
            //
            // var sha256 = crypto.createHash("sha256");
            // sha256.update(msg);//utf8 here
            // var result = sha256.digest("base64");


           // let aa = utf8.encode(secret)
           // let bb  = utf8.encode(msg)
           //
           //  console.log(aa,11)
           //  console.log(bb,22)

            let result = crypto.createHmac('sha256', secret).update(msg).digest("base64");
            // Signature = Base64(HMAC-SHA256(ShaKey, (ShaKey + URI + JSON + nonce)))

            console.log(result)


            // Base64(HMAC-SHA256(ShaKey, (ShaKey + URI + JSON + nonce)))


            return result;

            // secret = secret ?? "";
            // var encoding = new UTF8Encoding();
            //
            // byte[] keyByte = encoding.GetBytes(secret);
            // byte[] messageBytes = encoding.GetBytes(msg);
            //
            // using (HMACSHA256 hmacsha256 = new HMACSHA256(keyByte)){
            //     byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            //     return Convert.ToBase64String(hashmessage);
            // }
            //
            //
            // var crypto = require("crypto");
            // var sha256 = crypto.createHash("sha256");
            // sha256.update("ThisPassword", "utf8");//utf8 here
            // var result = sha256.digest("base64");
            // console.log(result); //d7I986+YD1zS6Wz2XAcDv2K8yw3xIVUp7u/OZiDzhSY=
            //
            // SHA256 sha256 = SHA256Managed.Create(); //utf8 here as well
            // byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes("ThisPassword"));
            // string result = Convert.ToBase64String(bytes);
            // Console.WriteLine(result); //d7I986+YD1zS6Wz2XAcDv2K8yw3xIVUp7u/OZiDzhSY=

        }

        const testApi = 'http://61.219.193.145/api/v1/paypages';
        const bankApi = 'https://api.ubpg.com.tw/api/v1/paypages';
        const bankuri = '/v1/paypages';

        const data = {
            "MerOrderNo"       : req.body.MerOrderNo,
            "Amount"           : req.body.Amount,
            "Currency"         : "TWD",
            "Paytype"          : req.body.PayType,
            "ThreeDomainSecure": true,
            "ReturnUrl"        : req.body.NotifyUrl,
            "NotifyUrl"        : req.body.NotifyUrl
        };

        let resData  = CreateToken(req.body.bankData.hashKey, req.body.bankData.hashKey + '/v1/paypages' + JSON.stringify(data) + req.body.MerOrderNo)
        const config = {
            method : 'post',
            url    : bankApi,
            headers: {
                'X-UB-StoreID': req.body.bankData.storeId, //商城代號
                'X-UB-TaxID'  : req.body.bankData.taxId, //統一編號
                'X-UB-Nonce'  : req.body.MerOrderNo,//隨機瑪
                'X-UB-Auth'   : resData,//簽章
                'Content-Type': 'application/json'
            },
            data   : data
        };

        axios(config).then(function (response) {
            console.log(JSON.stringify(response.data), 999);
            res.status(200).send(response.data);
        }).catch(function (error) {
            console.log(error);
        });

    }
    catch (err) {

    }
})


app.post("/newhook", async (req, res, next) => {

    console.log(req.body.events,666)
    console.log(req.body.events[0].source.userId,777) // 查詢資料表lineData 是有符合 有的話撈出token

    let content     = [];
    const citiesRef = db.collection('lineData');
    const snapshot  = await citiesRef.where('shopLineId', '==', req.body.events[0].source.userId).get();
    snapshot.forEach(doc => {
        content = doc.data();
        // content.push(doc.data());
    });

    const client = new line.Client({
        channelAccessToken: content.token
    });
    if (req.body.events[0].message.text == '預約') {
        const message = [
            {
                "type"    : "flex",
                "altText" : '預約流程',
                "contents": {
                    "type"  : "bubble",
                    "body"  : {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "contents": [
                            {
                                "type"  : "text",
                                "text"  : '進入預約',
                                "weight": "bold",
                                "size"  : "xl"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "margin"  : "lg",
                                "spacing" : "sm",
                                "contents": [
                                    {
                                        "type"    : "box",
                                        "layout"  : "baseline",
                                        "spacing" : "sm",
                                        "contents": [
                                            {
                                                "type" : "text",
                                                "text" : '點擊下方預約按鈕進入預約系統。',
                                                "wrap" : true,
                                                "color": "#666666",
                                                "size" : "sm",
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    "footer": {
                        "type"    : "box",
                        "layout"  : "vertical",
                        "spacing" : "sm",
                        "contents": [
                            {
                                "type"  : "button",
                                "style" : "primary",
                                "height": "sm",
                                "action": {
                                    "type" : "uri",
                                    "label": "我要預約",
                                    "uri"  : `https://liff.line.me/1656769636-1nZBq8KG/member/order-list`
                                },
                                "color" : "#555555"
                            },
                            {
                                "type"    : "box",
                                "layout"  : "vertical",
                                "contents": [],
                                "margin"  : "sm"
                            }
                        ],
                        "flex"    : 0
                    }
                }
            }
        ]
        client.replyMessage(req.body.events[0].replyToken, message).then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            console.log(err);
        });
    }
})


const server = app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

